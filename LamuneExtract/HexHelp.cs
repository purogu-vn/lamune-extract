﻿using System;
using System.Text;

namespace LamuneExtract {
	/// <summary>
	/// Provides helper methods associated with hexadecimal
	/// </summary>
	public static class HexHelp {
		/// <summary>
		/// Is the given byte in range to be a printable ASCII character?
		/// </summary>
		public static bool IsPrintableASCII(byte b) {
			return b >= 0x20 && b <= 0x7E;
		}

		public static bool IsASCIILetter(byte b) {
			return b >= 97 && b <= 122;
		}

		/// <summary>
		/// Is the given byte in range to be an unprintable ASCII character?
		/// </summary>
		public static bool IsUnprintableASCII(byte b) {
			return b <= 0x1F || b == 0x7F;
		}
		/// <summary>
		/// Is the given byte in range to be considered a half width katakana character?
		/// </summary>
		public static bool IsHalfWidthKatakana(byte b) {
			return b >= 0xA1 && b <= 0xDF;
		}
		/// <summary>
		/// Is the given byte in range to be considered the
		/// first byte of a Shift JIS encoded character?
		/// </summary>
		public static bool IsJisByte1(byte b) {
			return (b >= 0x81 && b <= 0x9F) || (b >= 0xE0 && b <= 0xEF);
		}
		/// <summary>
		/// Is the given byte in range to be considered the
		/// second byte of a Shift JIS encoded character?
		/// </summary>
		public static bool IsJisByte2(byte b) {
			return (b >= 0x40 && b <= 0xFC) && b != 0x7F;
		}
		public static string GetHexView(byte[] data) {
			return GetHexView(data, 0);
		}
		public static string GetHexView(byte[] data, int start) {
			return GetHexView(data, start, data.Length);
		}
		/// <summary>
		/// Given a byte array, will display the bytes neatly with corresponding string representations
		/// encoded in Shift JIS shown on the right
		/// </summary>
		/// <param name="data">the data to display</param>
		/// <param name="start">the inclusive starting index of what to show in the data array</param>
		/// <param name="end">the exclusive ending index of what to show in the data array</param>
		/// <param name="bytesPerColumn">how many bytes should be printed per column</param>
		/// <param name="columnsPerLine">how many columns should be printed per line</param>
		/// <returns></returns>
		public static string GetHexView(byte[] data, int start, int numPrint, bool isAscii = false, int bytesPerColumn = 4, int columnsPerLine = 4) {
			//wanting to print more bytes than you have
			if (numPrint + start > data.Length) {
				//just print the maximal number
				numPrint = data.Length - start;
			}
			//what index should be ended at
			int end = start + numPrint;
			//stores all string data
			StringBuilder main = new StringBuilder();
			//stores string data temporarily that will be displayed on the right
			StringBuilder buff = new StringBuilder();

			int currCol = 1;
			bool lastWasJis1 = false;
			//number of total lines needed
			int numLines = (int)Math.Ceiling(1.0 * numPrint / (bytesPerColumn * columnsPerLine));
			//can calculate the total number of byte spaces to be filled
			int numSpaces = numLines * columnsPerLine * bytesPerColumn;
			//for every space
			for (int s = 0; s < numSpaces; s++) {
				//index of data to use
				int i = start + s;
				//if no more data to display
				if (i >= end) {
					//put some space
					main.Append("   ");
				}
				else {
					//add 2 digit hex string and space
					main.Append(data[i].ToString("X2") + " ");

					//last read data was byte 1 of shift jis
					if (lastWasJis1) {
						//skip this byte, already read with the first
						lastWasJis1 = false;
					}
					else {
						//append to the text buffer either ascii or shift jis encoding
						buff.Append(isAscii ? EncodeStringAscii(data, i)
							: EncodeStringShiftJIS(data, i, end, out lastWasJis1));
						////a visible, 1 byte byte
						//if (isPrintableASCII(data[i]) || isHalfWidthKatakana(data[i])) {
						//	//encode and store the singular byte
						//	buff.Append(jis.GetString(data, i, 1));
						//}
						////start of a 2 byte sequence (and has next byte)
						//else if (isJisByte1(data[i]) && i + 1 < end) {
						//	//append both bytes encoded into one character
						//	buff.Append(jis.GetString(data, i, 2));
						//	lastWasJis1 = true;
						//}
						////unprintable character
						//else {
						//	buff.Append(".");
						//}
					}
				}
				//time for new column
				if ((s + 1) % bytesPerColumn == 0) {
					main.Append(" ");
					currCol++;
				}
				//time for new row
				if (currCol % (columnsPerLine + 1) == 0) {
					//add translation to the right
					main.AppendLine("| " + buff.ToString());
					buff.Clear();
					//reset column
					currCol = 1;
				}
			}
			return main.ToString();
		}
		private static string EncodeStringAscii(byte[] data, int i) {
			if (IsPrintableASCII(data[i])) {
				return Encoding.ASCII.GetString(data, i, 1);
			}

			return ".";

		}
		private static string EncodeStringShiftJIS(byte[] data, int i, int end, out bool lastWasJis1) {
			//a visible, 1 byte byte
			if (IsPrintableASCII(data[i]) || IsHalfWidthKatakana(data[i])) {
				//encode and store the singular byte
				lastWasJis1 = false;
				return Config.SHIFT_JIS.GetString(data, i, 1);
			}
			//start of a 2 byte sequence (and has next byte)

			if (IsJisByte1(data[i]) && i + 1 < end) {
				//append both bytes encoded into one character
				lastWasJis1 = true;
				return Config.SHIFT_JIS.GetString(data, i, 2);

			}
			//unprintable character
			lastWasJis1 = false;
			return ".";
		}
	}
}

﻿namespace LamuneExtract.Raw {
	internal class DecryptedASBFile : ASBFile {

		// load already decrypted file
		internal DecryptedASBFile(string fileName, byte[] data) : base(fileName) {
			FileData = data;
		}

		// load and decrypt file data
		internal DecryptedASBFile(string fileName, uint compressedLength, uint uncompressedLength, 
			byte[] encryptedData) : base(fileName) {
			CompressedLength = compressedLength;
			UncompressedLength = uncompressedLength;
			byte[] decrData = DecryptData(encryptedData);
			FileData = DecompressData(decrData);
		}

		internal EncryptedASBFile Encrypt() {
			return new EncryptedASBFile(FileName, FileData);
		}

		private byte[] DecryptData(byte[] data) {
			return CipherData(data, (key, dword) => dword - key);
		}

		private byte[] DecompressData(byte[] checksumAndData) {
			return ZLib.Decompress(checksumAndData, UncompressedLength);
		}
	}
}

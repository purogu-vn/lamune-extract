﻿using System.Collections.Generic;
using System.IO;

namespace LamuneExtract.Raw {
	internal class DecryptedMemoryARCFile : DecryptedLazyARCFile {
		internal IList<DecryptedASBFile> ASBFiles { get; }
		internal IList<DecryptedCPBFile> CPBFiles { get; }
		internal IList<DecryptedTBLFile> TblFiles { get; }

		internal DecryptedMemoryARCFile(string path) : base(path, true) {
			ASBFiles = new List<DecryptedASBFile>();
			CPBFiles = new List<DecryptedCPBFile>();
			TblFiles = new List<DecryptedTBLFile>();
			// Store all the files we need into ASBFiles, CPBFiles, TblFiles by running
			// through the enumerable
			foreach (LamuneFile dummy in AllFiles) {}
		}

		protected override DecryptedASBFile ParseASB(Stream str, BinaryReader bRead, string fileName) {
			DecryptedASBFile asbFile = base.ParseASB(str, bRead, fileName);
			ASBFiles.Add(asbFile);
			return asbFile;
		}

		protected override DecryptedCPBFile ParseCPB(Stream str, BinaryReader bRead, string fileName, int fileSize, int entryStart) {
			DecryptedCPBFile cpbFile = base.ParseCPB(str, bRead, fileName, fileSize, entryStart);
			if (cpbFile != null) {
				CPBFiles.Add(cpbFile);
			}

			return cpbFile;
		}

		protected override DecryptedTBLFile ParseTbl(Stream str, BinaryReader bRead, string fileName) {
			DecryptedTBLFile tblFile = base.ParseTbl(str, bRead, fileName);
			TblFiles.Add(tblFile);
			return tblFile;
		}
	}
}

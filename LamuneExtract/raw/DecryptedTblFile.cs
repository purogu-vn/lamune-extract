﻿using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;

namespace LamuneExtract.Raw {
	internal class DecryptedTBLFile : LamuneFile {
		internal IDictionary<int, string> Table { get; }
		internal DecryptedTBLFile(string fileName, byte[] tblData) : base(Path.ChangeExtension(fileName, ".tbl.json")) {
			FileData = tblData;
			Table = new Dictionary<int, string>();
			int i = 0;
			for(int pos = 0; pos < tblData.Length; pos += 32) {
				string mapping = Config.SHIFT_JIS.GetString(tblData, pos, 32).TrimEnd('\0');
				if(mapping.Length > 0) {
					Table[i] = mapping;
				}
				i++;
			}
		}

		internal override void Save(string folder) {
			string path = Path.Combine(folder, FileName);
			JObject jTbl = new JObject();
			foreach (var pair in Table) {
				jTbl.Add(pair.Key.ToString(), pair.Value);
			}
			path.SaveJson(jTbl);
		}

		internal DecryptedTBLFile(string path) : this(Path.GetFileName(path), File.ReadAllBytes(path)) {
		}
	}
}

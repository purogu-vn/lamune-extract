﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace LamuneExtract.Raw {
	internal class EncryptedCPBFile : LamuneFile {
		internal int Width { private set; get; }
		internal int Height { private set; get; }
		internal int[] ColorSizes { private set; get; }

		internal EncryptedCPBFile(string path) : base(Path.GetFileName(Path.ChangeExtension(path, ".cpb"))) {
			FileData = EncodeCPB(path);
		}

		private byte[] EncodeCPB(string path) {
			byte[] bitmapData = BitmapData(path);
			using (MemoryStream stream = new MemoryStream()) {
				using (BinaryWriter bWrite = new BinaryWriter(stream)) {
					bWrite.Write(0x1A425043); // CPB 0x1a
					bWrite.Write((byte)1); // type 1 (zlib compression)
										   // bits per pixel
					bWrite.Write((byte)32);
					// 1 short of space
					bWrite.Write((short)0);
					short width = (short)Width;
					short height = (short)Height;
					bWrite.Write(width);
					bWrite.Write(height);
					// write the max color size
					bWrite.Write(ColorSizes.Max());
					foreach (int colorSize in ColorSizes) {
						bWrite.Write(colorSize);
					}
					bWrite.Write(bitmapData);
				}
				return stream.ToArray();
			}
		}

		private byte[] BitmapData(string path) {
			using (Bitmap bit = new Bitmap(Image.FromFile(path, true))) {
				Width = bit.Width;
				Height = bit.Height;
				byte[][] colorData = BitmapToColors(bit);
				byte[][] compColorData = new byte[4][];
				for (int i = 0; i < 4; i++) {
					compColorData[i] = ZLib.Compress(colorData[i]);
				}
				int combinedSize = compColorData.Sum(arr => arr.Length);
				byte[] bitmapData = new byte[combinedSize];
				int pos = 0;
				ColorSizes = new int[4];
				for (int i = 0; i < 4; i++) {
					int len = compColorData[i].Length;
					ColorSizes[i] = len;
					Buffer.BlockCopy(compColorData[i], 0, bitmapData, pos, len);
					pos += len;
				}
				return bitmapData;
			}
		}

		private byte[][] BitmapToColors(Bitmap bit) {
			int imageSize = Width * Height;
			int dataSize = imageSize * 4;
			BitmapData data = bit.LockBits(new Rectangle(0, 0, bit.Width, bit.Height), ImageLockMode.ReadOnly, bit.PixelFormat);
			byte[] bitmapData = new byte[dataSize];
			IntPtr pRaw = data.Scan0;
			Marshal.Copy(pRaw, bitmapData, 0, dataSize);
			bit.UnlockBits(data);
			byte[][] colorData = new byte[4][];
			for(int i = 0; i < 4; i++) {
				colorData[i] = new byte[imageSize];
			}
			for(int i = 0; i < imageSize; i++) {
				// red
				colorData[0][i] = bitmapData[4 * i + 2];
				// green
				colorData[1][i] = bitmapData[4 * i + 1];
				// blue
				colorData[2][i] = bitmapData[4 * i + 0];
				// alpha
				colorData[3][i] = bitmapData[4 * i + 3];
			}
			return colorData;
		}
	}
}

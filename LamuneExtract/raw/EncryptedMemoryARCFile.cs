﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamuneExtract.Raw {
	internal class EncryptedMemoryARCFile : EncryptedARCFile {
		
		private readonly IList<LamuneFile> files;

		internal EncryptedMemoryARCFile(string fileName) : base(fileName) {
			files = new List<LamuneFile>();
		}

		internal void AddFile(LamuneFile otherFile) {
			RegisterExtension(otherFile.GetExtension());
			files.Add(otherFile);
		}
		internal void Pack() {
			using (MemoryStream str = new MemoryStream()) {
				using (BinaryWriter writer = new BinaryWriter(str)) {
					WriteArc(writer);
				}
				FileData = str.ToArray();
			}
		}

		protected override int FileCount() {
			return files.Count;
		}

		protected override IEnumerable<KeyValuePair<string, int>> GenerateHeaderEntries() {
			foreach (LamuneFile file in files) {
				yield return new KeyValuePair<string, int>(file.FileName, file.FileData.Length);
			}
		}

		protected override IEnumerable<byte[]> GenerateFiles() {
			foreach(LamuneFile file in files) {
				yield return file.FileData;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LamuneExtract.Raw {
	internal class DecryptedLazyARCFile : LamuneFile {

		private string Path { get; }
		private bool OnlyTranslatable { get; }

		internal IEnumerable<LamuneFile> AllFiles { get; }

		internal DecryptedLazyARCFile(string path, bool onlyTranslatable=false) : base(System.IO.Path.GetFileName(path)) {
			Path = path;
			OnlyTranslatable = onlyTranslatable;
			AllFiles = UnpackArc();
		}

		private IEnumerable<LamuneFile> UnpackArc() {
			using (FileStream str = new FileStream(Path, FileMode.Open)) {
				// skip over file signature (ARC0x1A)
				str.Position += 4; // 4
				// skip over num of file extensions in archive
				str.Position += 4; // 8
				using (BinaryReader bRead = new BinaryReader(str)) {
					// number of files in archive
					uint numFiles = bRead.ReadUInt32(); // 12
					// the size of the encrypted header table
					uint encHeaderTableSize = bRead.ReadUInt32(); // 16
					//the space for 8 file extensions found in the archive
					bRead.ReadBytes(8 * 4); // 48 + 0
					// 48 is where the encrypted header table starts, and goes on for encHeaderTableSize bytes
					byte[] encHeaderTable = bRead.ReadBytes((int)encHeaderTableSize);
					// go back to encrypted header table start
					str.Position = 48;
					uint storedHash = bRead.ReadUInt32();
					// skip to size of decrypted header table inside header table
					str.Position += 12; // 48 + 16
					// size of the decrypted header table
					uint headerTableSize = bRead.ReadUInt32();
					// houses decrypted header table
					byte[] headerTable = new byte[headerTableSize];

					DecryptAlcot(encHeaderTable, headerTable, headerTableSize);
					byte[] encHeaderTableContents = new byte[encHeaderTableSize - 20];
					Array.Copy(encHeaderTable, 20, encHeaderTableContents, 0, encHeaderTableSize - 20);
					uint calcHash = Hash.GainaxHash(encHeaderTableContents);
					if (storedHash != calcHash) {
						throw new Exception("Invalid hash");
					}
					// data starts after encrypted header table
					uint dataStart = 48 + encHeaderTableSize;
					// size of each entry in the decrypted header table
					uint entrySize = headerTableSize / numFiles;
					// length allotted to file name in a header
					uint fileNameLength = entrySize - 16;

					for (uint i = 0; i < numFiles; i++) {
						// offset in header table of current entry
						uint offset = i * entrySize;
						// file name located at entry + 16 and goes until end of entry
						string fileName = Config.SHIFT_JIS.GetString(headerTable, (int)offset + 16, (int)fileNameLength).TrimEnd('\0');
						// file size located at entry + 4
						uint fileSize = BitConverter.ToUInt32(headerTable, (int)offset + 4);
						// where this entry's file data begins located at entry + 0
						uint entryDataStart = BitConverter.ToUInt32(headerTable, (int)offset + 0) + dataStart;

						// seek to entry data start
						str.Position = entryDataStart; // 0
						
						if (fileName.EndsWith(".asb")) {
							yield return ParseASB(str, bRead, fileName);
						}
						else if (fileName.EndsWith(".tbl")) {
							yield return ParseTbl(str, bRead, fileName);
						}
						else if(fileName.EndsWith(".cpb")) {
							var cpbFile = ParseCPB(str, bRead, fileName, (int) fileSize, (int) entryDataStart);
							if (cpbFile != null) {
								yield return cpbFile;
							}
						}
						else if(!OnlyTranslatable) {
							byte[] data = bRead.ReadBytes((int)fileSize);
							yield return new LamuneFile(fileName, data);
						}

						//Console.WriteLine(fileName + ": size=" + fileSize + " start=" + entryDataStart);
					}
				}
			}
		}

		protected virtual DecryptedASBFile ParseASB(Stream str, BinaryReader bRead, string fileName) {
			// skip over magic (ASB0x1a)
			str.Position += 4;  // 4
			// read zipped length from start of entry
			uint entryCompressedLength = bRead.ReadUInt32(); // 8
			// read unzipped length
			uint entryUncompressedLength = bRead.ReadUInt32(); // 12
			// read rest of the entry as the encrypted data
			byte[] entryEncryptedData = bRead.ReadBytes((int)entryCompressedLength);
			return new DecryptedASBFile(fileName, entryCompressedLength, entryUncompressedLength, entryEncryptedData);
		}

		protected virtual DecryptedCPBFile ParseCPB(Stream str, BinaryReader bRead, string fileName, int fileSize, int entryStart) {
			if(OnlyTranslatable && Config.GetList(Config.SKIP_IMAGE_PREFIX).Any(fileName.StartsWith)) {
				Console.WriteLine("Skipping " + fileName);
				return null;
			}
			// skip over magic (CPB0x1a) + 0
			str.Position += 4;
			// + 4
			byte type = bRead.ReadByte();
			// usually 24 or 32 + 5
			byte bitsPerPixel = bRead.ReadByte();
			// usually 3 or 4 (rgb or argb)
			byte numColors = (byte)(bitsPerPixel / 8);

			// skip over 2 byte space + 6
			str.Position += 2;
			// read both dimensions + 8
			short width = bRead.ReadInt16();
			short height = bRead.ReadInt16();
			// skip over something? + 12
			str.Position += 4;
			// read the sizes of each color in the encrypted format + 16
			int[] colorSizes = new int[numColors];
			for(int i = 0; i < colorSizes.Length; i++) {
				colorSizes[i] = bRead.ReadInt32();
			}

			// skip to encoded data start
			str.Position = entryStart + 0x20;
			// amount of data for 1 color is the entire image decrypted
			int colorSize = width * height;
			// order is rgb(a)
			byte[][] decData = new byte[numColors][];
			for(int i = 0; i < numColors; i++) {
				// read in the bytes for just this color
				byte[] src = bRead.ReadBytes(colorSizes[i]);
				if (type == 3) {
					// all colors decrypt to the same size
					decData[i] = new byte[colorSize];
					DecryptAlcot(src, decData[i], (uint)colorSize);
				}
				else if (type == 1) {
					decData[i] = ZLib.Decompress(src, (uint)colorSize);
				}
				else {
					Console.WriteLine($"Unsupported type: {type}");
					return null;
				}
			}

			Console.WriteLine($"{fileName}: type={type} bpp={bitsPerPixel} ({width}x{height})");
			return new DecryptedCPBFile(fileName, width, height, decData);
		}

		protected virtual DecryptedTBLFile ParseTbl(Stream str, BinaryReader bRead, string fileName) {
			// Skip over magic (TBL0x1A)
			str.Position += 4;
			// read zipped length from start of entry
			uint entryCompressedLength = bRead.ReadUInt32(); // 8
			// read unzipped length
			uint entryUncompressedLength = bRead.ReadUInt32(); // 12
			byte[] compressedData = bRead.ReadBytes((int) entryCompressedLength);
			byte[] decompressedData = ZLib.Decompress(compressedData, entryUncompressedLength);
			return new DecryptedTBLFile(fileName, decompressedData);
		}
	}
}

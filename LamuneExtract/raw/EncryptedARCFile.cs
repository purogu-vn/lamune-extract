﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LamuneExtract.Raw {
	internal abstract class EncryptedARCFile : LamuneFile {
		private const int TABLE_ENTRY_SIZE = 48;

		private readonly ISet<string> extensions;

		internal EncryptedARCFile(string fileName) : base(fileName) {
			extensions = new SortedSet<string>();
			FileData = null;
		}

		protected void RegisterExtension(string ext) {
			extensions.Add(ext);
			if (extensions.Count > 8) {
				Console.Error.AndExit("Too many extensions added, max is 8.");
			}
		}

		protected abstract int FileCount();
		protected abstract IEnumerable<KeyValuePair<string, int>> GenerateHeaderEntries();
		protected abstract IEnumerable<byte[]> GenerateFiles();

		private byte[] HeaderTableContents() {
			using (MemoryStream str = new MemoryStream()) {
				using (BinaryWriter writer = new BinaryWriter(str)) {
					int dataPosition = 0;
					foreach (var pair in GenerateHeaderEntries()) {
						writer.Write(dataPosition);
						writer.Write(pair.Value);
						writer.Write(0); // dummy
						writer.Write(0); // dummy
						byte[] encodedFileName = Config.SHIFT_JIS.GetBytes(pair.Key);
						writer.Write(encodedFileName);
						for (int i = encodedFileName.Length; i < 32; i++) {
							writer.Write((byte)0);
						}
						dataPosition += pair.Value;
					}
					return str.ToArray();
				}
			}
		}

		private byte[] EncryptedHeaderContents(int branchFixtures) {
			using (MemoryStream str = new MemoryStream()) {
				using (BinaryWriter writer = new BinaryWriter(str)) {
					for (int i = 0; i < branchFixtures; i++) {
						writer.Write((byte)0); // 0 to force psrc3 reading branch
					}
					byte[] headerContents = HeaderTableContents();
					int contentsOffset = 0;
					while (contentsOffset < headerContents.Length) {
						int length = Math.Min(headerContents.Length - contentsOffset, 256);
						writer.Write((byte)(length - 1)); // -1 (we add 1 later)
						writer.Write(headerContents, contentsOffset, length);
						contentsOffset += length;
					}
				}
				return str.ToArray();
			}
		}

		private byte[] EncryptedHeaderTable() {
			using (MemoryStream str = new MemoryStream()) {
				using (BinaryWriter writer = new BinaryWriter(str)) {
					int decryptedSize = FileCount() * TABLE_ENTRY_SIZE;
					// how many pieces the table data will be split into
					int segments = decryptedSize / 256;
					if(decryptedSize % 256 != 0) {
						segments++;
					}
					int branchFixtures = segments / 8;
					if(segments % 8 != 0) {
						branchFixtures += 1;
					}
					byte[] encHeaderContents = EncryptedHeaderContents(branchFixtures);
					uint hash = Hash.GainaxHash(encHeaderContents);
					//header
					writer.Write(hash); // TODO hash
					writer.Write(branchFixtures); // size1
					writer.Write(0); // size2
					writer.Write(decryptedSize + segments); // size3
					writer.Write(decryptedSize); // decryptedSize
					writer.Write(encHeaderContents);
					return str.ToArray();
				}
			}
		}

		protected void WriteArc(BinaryWriter writer) {
			byte[] encryptedHeaderTable = EncryptedHeaderTable();
			writer.Write(0x1a435241); // 0x1aARC
			writer.Write(extensions.Count);
			writer.Write(FileCount());
			writer.Write(encryptedHeaderTable.Length);
			foreach (string ext in extensions) {
				writer.Write(Encoding.ASCII.GetBytes(ext));
			}
			for (int i = extensions.Count; i < 8; i++) {
				writer.Write(0);
			}
			writer.Write(encryptedHeaderTable);
			foreach (byte[] data in GenerateFiles()) {
				writer.Write(data);
			}
		}
	}
}

﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamuneExtract.Raw {
    internal class EncryptedTBLFile : LamuneFile {
        internal EncryptedTBLFile(string path) : base(Path.GetFileNameWithoutExtension(path)) {
			FileData = EncodeTbl(path);
        }

		private byte[] EncodeTbl(string path) {
			byte[] uncompressedData = PrepareTableData(path);
			byte[] compressedData = ZLib.Compress(uncompressedData);
			using (MemoryStream stream = new MemoryStream()) {
				using (BinaryWriter bWrite = new BinaryWriter(stream)) {
					bWrite.Write(0x1A4c4254); // TBL 0x1a
					bWrite.Write(compressedData.Length);
					bWrite.Write(uncompressedData.Length);
					bWrite.Write(compressedData);
				}
				return stream.ToArray();
			}
		}

		private byte[] PrepareTableData(string path) {
			int maxId = 0;
			JObject jTbl = path.ReadAndParseJson();
			IDictionary<int, string> table = new Dictionary<int, string>();
			foreach (JProperty jLine in jTbl.Properties()) {
				int scenarioId = int.Parse(jLine.Name);
				table[scenarioId] = jLine.Value.ToString();
				if (scenarioId > maxId) {
					maxId = scenarioId;
				}
			}
			int tblSize = 32 * maxId;
			using (MemoryStream stream = new MemoryStream()) {
				using (BinaryWriter bWrite = new BinaryWriter(stream)) {
					bWrite.Write(new byte[tblSize]);
					foreach (var pair in table) {
						stream.Position = pair.Key * 32;
						bWrite.Write(Config.SHIFT_JIS.GetBytes(pair.Value));
					}
				}
				return stream.ToArray();
			}
		}
    }
}

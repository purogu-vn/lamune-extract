﻿using System;
using System.Text;

namespace LamuneExtract.Raw {
	internal abstract class ASBFile : LamuneFile {
		// クロハ, クロハDVD, TOYつめ, トリプ体験版, トリプ, ラムネ
		protected static readonly uint[] KEYS = { 0xF44387F3, 0xE1B2097A, 0xD153D863, 0xF389842D, 0x1DE71CB9, 0x99E15CB4 };
		protected static readonly uint LAMUNE_KEY = KEYS[5];

		protected delegate uint CipherFunc(uint key, uint dword);

		protected uint CompressedLength { set; get; }
		protected uint UncompressedLength { set; get; }

		internal ASBFile(string fileName) : base(fileName) {

		}
		
		internal ASBFile(string fileName, byte[] fileData) : base(fileName, fileData) {

		}

		/// <summary>
		/// Either encrypts or decrypts data based on given function
		/// </summary>
		protected byte[] CipherData(byte[] data, CipherFunc cipherFunc) {
			// affect the standard key by the uncompressed length
			uint key = UncompressedLength ^ LAMUNE_KEY;
			// vodoo magic
			key ^= ((key << 0x0C) | key) << 0x0B;

			byte[] decipheredData = new byte[data.Length];
			// cipher all sets of 4 bytes (skip any left over at the end not divisible by 4)
			int toDecryptLength = data.Length - (data.Length % 4);
			for (int i = 0; i < toDecryptLength; i += 4) {
				// read the ciphered dword
				uint encDword = BitConverter.ToUInt32(data, i);
				//apply the key to cipher it
				uint decDword = cipherFunc(key, encDword);
				//convert back to bytes
				byte[] decBytes = BitConverter.GetBytes(decDword);
				//store ciphered bytes back in new data array
				for (int j = 0; j < 4; j++) {
					decipheredData[i + j] = decBytes[j];
				}
			}
			// copy over unciphered bytes at the end
			for(int i = toDecryptLength; i < data.Length; i++) {
				decipheredData[i] = data[i];
			}
			return decipheredData;
		}
	}
}

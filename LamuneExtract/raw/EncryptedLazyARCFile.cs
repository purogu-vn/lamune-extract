﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamuneExtract.Raw {
	internal class EncryptedLazyARCFile : EncryptedARCFile {
		private readonly IDictionary<string, string> files;

		internal EncryptedLazyARCFile(string fileName) : base(fileName) {
			files = new SortedList<string, string>();
		}

		internal void AddFile(string path) {
			RegisterExtension(Path.GetExtension(path));
			files.Add(Path.GetFileName(path), path);
		}

		internal override void Save(string path) {
			string outFile = Path.Combine(path, FileName);
			using(FileStream fs = File.Create(outFile))
			using(BinaryWriter writer = new BinaryWriter(fs)) {
				WriteArc(writer);
			}
		}

		protected override int FileCount() {
			return files.Count;
		}

		protected override IEnumerable<KeyValuePair<string, int>> GenerateHeaderEntries() {
			foreach (var pair in files) {
				yield return new KeyValuePair<string, int>(pair.Key, (int)new FileInfo(pair.Value).Length);
			}
		}

		protected override IEnumerable<byte[]> GenerateFiles() {
			foreach (var pair in files) {
				yield return File.ReadAllBytes(pair.Value);
			}
		}
	}
}

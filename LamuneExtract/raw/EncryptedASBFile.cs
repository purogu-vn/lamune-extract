﻿using System.IO;

namespace LamuneExtract.Raw {
	internal class EncryptedASBFile : ASBFile {

		internal EncryptedASBFile(string asbPath) 
			: base(Path.GetFileName(Path.ChangeExtension(asbPath, ".asb")), File.ReadAllBytes(asbPath)) {
		}

		internal EncryptedASBFile(string fileName, byte[] rawData) : base(fileName) {
			UncompressedLength = (uint)rawData.Length;
			byte[] compressedData = ZLib.Compress(rawData);
			CompressedLength = (uint)compressedData.Length;
			byte[] encData = EncryptData(compressedData);
			using(MemoryStream stream = new MemoryStream()) {
				using (BinaryWriter writer = new BinaryWriter(stream)) {
					writer.Write(0x1a425341); // ASB 0x1a
					writer.Write(CompressedLength);
					writer.Write(UncompressedLength);
					writer.Write(encData);
					FileData = stream.ToArray();
				}
			}
			
		}

		private byte[] EncryptData(byte[] data) {
			return CipherData(data, (key, dword) => key + dword);
		}
	}
}

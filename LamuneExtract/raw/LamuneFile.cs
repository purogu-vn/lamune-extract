﻿using System;
using System.IO;

namespace LamuneExtract.Raw {
	internal class LamuneFile {
		internal string FileName { get; }
		public byte[] FileData { protected set; get; }

		internal LamuneFile(string fileName) : this(fileName, null) {
		}

		internal LamuneFile(string fileName, byte[] fileData) {
			FileName = fileName;
			FileData = fileData;
		}

		internal byte[] DataCopy() {
			byte[] copy = new byte[FileData.Length];
			Array.Copy(FileData, copy, copy.Length);
			return copy;
		}

		internal string GetExtension() {
			return Path.GetExtension(FileName);
		}

		internal virtual void Save(string folder) {
			File.WriteAllBytes(Path.Combine(folder, FileName), FileData);
		}

		internal void SaveHex(string path) {
			File.WriteAllText(Path.Combine(path, Path.ChangeExtension(FileName, ".txt")), this.ToString());
		}

		public override string ToString() {
			return HexHelp.GetHexView(FileData);
		}

		/// <summary>
		/// Decrypts some data into a target array using alcot's custom decryption method
		/// </summary>
		protected void DecryptAlcot(byte[] src, byte[] dest, uint destSize) {
			uint psrc1 = 20; // 20 into the encoded table
			uint psrc2 = psrc1 + BitConverter.ToUInt32(src, 4);
			uint psrc3 = psrc2 + BitConverter.ToUInt32(src, 8);
			uint pdest = 0;
			//ranges from 0x80 to 0x01, shifting the single bit over to the right (runs 8 times)
			byte code = 0x80;
			// while still more to decrypt
			while (pdest < dest.Length) {
				// executed 8 times, now reset
				if (code == 0) {
					psrc1 += 1;
					code = 0x80;
				}
				// source data at the pointer has a 1 bit at the current shifting bit
				if ((src[psrc1] & code) != 0) {
					// tmp: first 3 bits are the length, last 13 are offset
					// get the ushort at psrc2
					ushort tmp = BitConverter.ToUInt16(src, (int)psrc2);
					// skip over the tmp value
					psrc2 += 2;
					// truncate to first 3 bits and add 3
					ushort length = (ushort)((tmp >> 0x0D) + 3);
					if (length > 0) {
						// get last 13 bits to use as offset
						ushort offset = (ushort)((tmp & 0x00001FFF) + 1);
						// point to location offset bytes back in dest
						uint pdest2 = pdest - offset;
						// copy pdest2 data overwriting some data in dest
						for (int i = 0; i < length; i++) {
							dest[pdest] = dest[pdest2];
							pdest += 1;
							pdest2 += 1;
						}
					}
				}
				else {
					// read length and skip over byte
					short length = (short)(src[psrc3] + 1);
					psrc3 += 1;
					// copy psrc3 data over to dest
					for (int i = 0; i < length; i++) {
						dest[pdest] = src[psrc3];
						pdest += 1;
						psrc3 += 1;
					}
				}
				code >>= 1;
			}

		}

		internal static LamuneFile FromFile(string path) {
			return new LamuneFile(Path.GetFileName(path), File.ReadAllBytes(path));
		}
	}
}

﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace LamuneExtract.Raw {
	internal class DecryptedCPBFile : LamuneFile {
		private int Width { get; }
		private int Height { get; }

		internal DecryptedCPBFile(string fileName, int width, int height, byte[][] rawData) : base(Path.ChangeExtension(fileName, ".bmp")) {
			Width = width;
			Height = height;
			FileData = GetBitmapData(rawData);
		}

		private byte[] GetBitmapData(byte[][] rgbaData) {
			int numColors = rgbaData.Length;
			int imageSize = Width * Height;
			// numColors * width * height (always 4 for output image)
			int totalSize = 4 * imageSize;
			byte[] bitmapData = new byte[totalSize];
			for(int i = 0; i < imageSize; i++) {
				// blue
				bitmapData[4 * i + 0] = rgbaData[2][i];
				// green
				bitmapData[4 * i + 1] = rgbaData[1][i];
				// red
				bitmapData[4 * i + 2] = rgbaData[0][i];
				// alpha
				bitmapData[4 * i + 3] = numColors < 4 ? (byte)255 : rgbaData[3][i];
			}
			
			using (MemoryStream output = new MemoryStream()) {
				using (Bitmap bit = new Bitmap(Width, Height, PixelFormat.Format32bppArgb)) {
					BitmapData data = bit.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.WriteOnly, bit.PixelFormat);
					IntPtr pRaw = data.Scan0;
					Marshal.Copy(bitmapData, 0, pRaw, bitmapData.Length);
					bit.UnlockBits(data);
					bit.Save(output, ImageFormat.Bmp);
				}
				return output.ToArray();
			}
		}
	}
}

﻿using System;
using System.IO;
using LamuneExtract.Human;
using LamuneExtract.Raw;
using LamuneExtract.Human.Transformer;
using System.Diagnostics;

namespace LamuneExtract {
	internal static class ProgramInsert {
		internal static readonly string[] Commands = { "full", "quick", "watch", "exe", "custom" };
		private const string DEFAULT_ARC_NAME = "data02.arc";

		internal static void Insert(string action) {
			if(action.Equals("exe")) {
				InsertExe(true);
			}
			else if(action.Equals("watch")) {
				InsertWatch();
			}
			else if (action.Equals("custom")) {
				InsertCustom();
			}
			else {
				InsertAssets(action);
			}
		}

		private static void WatcherPerform() {
			Stopwatch w = new Stopwatch();
			w.Start();
			InsertAssets("watch");
			w.Stop();
			if (Config.GetBoolean(Config.SHOW_TIME)) {
				Console.WriteLine($"Took {w.ElapsedMilliseconds / 1000.0} seconds");
			}
		}

		private static void InsertWatch() {
			string tlFolder = Config.GetAndPrepareOptionalDirectory(Config.TL_FOLDER);
			string imageTlPath = Config.GetAndPrepareOptionalDirectory(Config.IMAGE_TL_FOLDER);
			string watchSubjects = "";
			if(tlFolder != null) {
				DirectoryWatcher.RegisterWatcher(tlFolder, WatcherPerform);
				watchSubjects += "scripts";
			}
			if(imageTlPath != null) {
				DirectoryWatcher.RegisterWatcher(imageTlPath, WatcherPerform);
				watchSubjects = watchSubjects.Length > 0 ? watchSubjects + " and images" : "images";
			}
			if(watchSubjects.Length <= 0) {
				Console.Error.AndExit("Nothing to watch!");
			}
			Console.WriteLine("Watching for changes with: " + watchSubjects);
		}

		private static void InsertCustom() {
			string gameFolder = Config.GetAndPrepareRequiredDirectory(Config.GAME_FOLDER);
			string insertionFolder = Config.GetAndPrepareRequiredDirectory(Config.CUSTOM_INSERT_FOLDER);
			string cacheFolder = Config.GetAndPrepareRequiredDirectory(Config.CACHE_FOLDER);
			string arcName = Config.GetOptionalSetting(Config.CUSTOM_ARC_NAME) ?? DEFAULT_ARC_NAME;
			bool loadedNames = NameTransformer.LoadNames(insertionFolder);
			EncryptedLazyARCFile arc = new EncryptedLazyARCFile(arcName);
			foreach (string filePath in Directory.GetFiles(insertionFolder)) {
				string fileName = Path.GetFileName(filePath);
				FileInfo fileInfo = new FileInfo(filePath);
				string output = null;
				string compiledPath = null;

				if (filePath.EndsWith(NameTransformer.OUTPUT_FILE)) {
					continue;
				}
				if (filePath.EndsWith(".bmp") || filePath.EndsWith(".png")) {
					output = $"Packing {fileName} as CPB file";
					compiledPath = GetCachedFile(cacheFolder, ".cpb", fileInfo);
					if(compiledPath == null) {
						LamuneFile file = new EncryptedCPBFile(filePath);
						compiledPath = CacheFile(cacheFolder, file);
					}
				}
				else if (IsInsertableScript(fileName)) {
					string warningMessage = loadedNames ? "" : $" (no {NameTransformer.OUTPUT_FILE} file found, will use japanese names)";
					output = $"Packing script file{warningMessage}: {fileName}";
					compiledPath = GetCachedFile(cacheFolder, ".asb", fileInfo);
					if (compiledPath == null) {
						LamuneFile file = ScriptFile.FromFile(filePath).PackASB().Encrypt();
						compiledPath = CacheFile(cacheFolder, file);
					}
				}
				else if(filePath.EndsWith(".tbl.json")){
					output = $"Packing table file: {fileName}";
					compiledPath = GetCachedFile(cacheFolder, ".tbl", fileInfo);
					if (compiledPath == null) {
						LamuneFile file = new EncryptedTBLFile(filePath);
						compiledPath = CacheFile(cacheFolder, file);
					}
				}
				else if (filePath.EndsWith(".asb")) {
					output = $"Encrypting ASB file: {fileName}";
					compiledPath = GetCachedFile(cacheFolder, ".asb", fileInfo);
					if (compiledPath == null) {
						LamuneFile file = new EncryptedASBFile(filePath);
						compiledPath = CacheFile(cacheFolder, file);
					}
				}
				else {
					output = $"Packing: {fileName}";
					compiledPath = filePath;
				}
				if (!Config.GetBoolean(Config.CUSTOM_SUPRESS_OUTPUT)) {
					Console.WriteLine(output);
				}
				arc.AddFile(compiledPath);
			}
			Console.WriteLine($"Outputting final {arcName}");
			arc.Save(gameFolder);
			Console.WriteLine("Insertion completed successfully");
		}

		private static void InsertAssets(string action) {
			string gameFolder = Config.GetAndPrepareRequiredDirectory(Config.GAME_FOLDER);
			string cacheFolder = action.Equals("full") ? null
														: Config.GetAndPrepareRequiredDirectory(Config.CACHE_FOLDER);
			string arcName = Config.GetOptionalSetting(Config.CUSTOM_ARC_NAME) ?? DEFAULT_ARC_NAME;

			EncryptedMemoryARCFile finalArc = new EncryptedMemoryARCFile(arcName);
			InsertScript(finalArc, cacheFolder);
			InsertImage(finalArc, cacheFolder);
			InsertExe(false);
			Console.WriteLine($"Outputing final {arcName}");
			finalArc.Pack();
			finalArc.Save(gameFolder);
			Console.WriteLine("Insertion completed successfully");
		}

		private static void InsertScript(EncryptedMemoryARCFile finalArc, string cacheFolder) {
			string tlFolder = Config.GetAndPrepareOptionalDirectory(Config.TL_FOLDER);
			if(tlFolder == null) {
				return;
			}
			Console.WriteLine("Loading translated names");
			bool loaded = NameTransformer.LoadNames(tlFolder);
			if (!loaded) {
				Console.Error.AndExit($"Missing name declarations file {NameTransformer.OUTPUT_FILE}");
			}
			Console.WriteLine("Beginning encrypting translated .asb script files");
			FileInfo cachedNameFileInfo = new FileInfo(Path.Combine(tlFolder, NameTransformer.OUTPUT_FILE));
			string cachedNameFile = GetCachedFile(cacheFolder, ".asb.json", cachedNameFileInfo);
			if(cachedNameFile == null && cacheFolder != null) {
				Console.WriteLine("Names have changed, invalidated cache");
				File.WriteAllBytes(Path.Combine(cacheFolder, NameTransformer.OUTPUT_FILE), 
				File.ReadAllBytes(Path.Combine(tlFolder, NameTransformer.OUTPUT_FILE)));
			}
			foreach (FileInfo file in new DirectoryInfo(tlFolder).GetFiles()) {
				if (IsInsertableScript(file.Name)) {
					// only use cached file if names haven't been updated
					string cachedFile = cachedNameFile == null ? null : GetCachedFile(cacheFolder, ".asb", file);
					if (cachedFile != null) {
						finalArc.AddFile(LamuneFile.FromFile(cachedFile));
					}
					else {
						ScriptFile script = ScriptFile.FromFile(file.FullName);
						EncryptedASBFile packed = script.PackASB().Encrypt();
						finalArc.AddFile(packed);
						CacheFile(cacheFolder, packed);
					}
				}
			}
		}

		private static void InsertImage(EncryptedMemoryARCFile finalArc, string cacheFolder) {
			string imageTlPath = Config.GetAndPrepareOptionalDirectory(Config.IMAGE_TL_FOLDER);
			if(imageTlPath == null) {
				return;
			}
			Console.WriteLine("Beginning encrypting translated .cpb image files");
			foreach (FileInfo file in new DirectoryInfo(imageTlPath).GetFiles()) {
				if(file.Extension.Equals(".bmp")) {
					string cachedFile = GetCachedFile(cacheFolder, ".cpb", file);
					if (cachedFile != null) {
						finalArc.AddFile(LamuneFile.FromFile(cachedFile));
					}
					else {
						EncryptedCPBFile packed = new EncryptedCPBFile(file.FullName);
						finalArc.AddFile(packed);
						CacheFile(cacheFolder, packed);
					}
				}
			}
		}

		private static void InsertExe(bool required) {
			string tlFolder = required ? Config.GetAndPrepareRequiredDirectory(Config.EXE_TL_FOLDER) : Config.GetAndPrepareOptionalDirectory(Config.EXE_TL_FOLDER);
			if (tlFolder == null) {
				return;
			}

			string exeFile = Config.GetRequiredFile(Config.LAMUNE_EXE);
			string gameFolder = Config.GetAndPrepareRequiredDirectory(Config.GAME_FOLDER);

			Console.WriteLine("Beginning translation of the exe");

			FileInfo translationFile = new FileInfo(Path.Combine(tlFolder, ExeStrings.JSON_FILE));
			if (!translationFile.Exists) {
				Console.Error.AndExit($"Missing required {ExeStrings.JSON_FILE} translation file");
			}
			FileInfo tlExeFile = new FileInfo(Path.Combine(gameFolder, "lamune-english.exe"));
			if (ShouldUseCache(translationFile, tlExeFile)) {
				Console.WriteLine("No exe translation changes");
			}
			else {
				ExeStrings.Insert(exeFile, tlFolder, tlExeFile.FullName);
				Console.WriteLine("Patched exe successfully");
			}
		}

		private static string ChangeExt(string p, string to) {
			return p.Substring(0, p.IndexOf('.')) + to;
		}

		private static string GetCachedFile(string cacheFolder, string ext, FileInfo original) {
			if (cacheFolder == null) return null;
			FileInfo cachedFile = new FileInfo(Path.Combine(cacheFolder, ChangeExt(original.Name, ext)));
			if(ShouldUseCache(original, cachedFile)) {
				return cachedFile.FullName;
			}
			Console.WriteLine("Refreshing " + original.Name);
			return null;
		}

		private static string CacheFile(string cacheFolder, LamuneFile file) {
			if (cacheFolder != null) {
				string cachedFile = Path.Combine(cacheFolder, file.FileName);
				File.WriteAllBytes(cachedFile, file.FileData);
				return cachedFile;
			}
			return null;
		}

		private static bool ShouldUseCache(FileInfo original, FileInfo product) {
			if (!product.Exists) return false;
			if (product.LastWriteTime < original.LastWriteTime) return false;
			return true;
		}

		private static bool IsInsertableScript(string fileName) {
			return !fileName.Equals(NameTransformer.OUTPUT_FILE) && fileName.EndsWith(".asb.json");
		}
	}
}

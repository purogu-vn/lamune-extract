﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LamuneExtract {
	internal static class Program {

		private const string LAMUNE_CONFIG = "lamune.cfg";

		public static void Main(string[] args) {
			Config.InitConfig(LAMUNE_CONFIG);
			if ((args.Length == 1 || args.Length == 2) && (args[0].Equals("extract") || args[0].Equals("insert"))) {
				string action = args[0];
				string subaction = action.Equals("extract") ? "tl" : "quick";
				if(args.Length == 2) {
					subaction = args[1];
					if(action.Equals("extract") && !ProgramExtract.Commands.Contains(subaction)) {
						ErrorUsage();
					}
					else if(action.Equals("insert") && !ProgramInsert.Commands.Contains(subaction)) {
						ErrorUsage();
					}
					
				}
				Console.WriteLine($"Starting {action} ({subaction})");
				Stopwatch w = new Stopwatch();
				w.Start();
				if(action.Equals("extract")) {
					ProgramExtract.Extract(subaction);
				}
				else {
					ProgramInsert.Insert(subaction);
				}
				w.Stop();
				if(Config.GetBoolean(Config.SHOW_TIME) && !subaction.Equals("watch")) {
					Console.WriteLine($"Took {w.ElapsedMilliseconds / 1000.0} seconds\n");
				}
			}
			else {
				ErrorUsage();
			}
		}

		private static void ErrorUsage() {
			Console.WriteLine($"Extraction Usage: LamuneExtract.exe extract <{string.Join("|", ProgramExtract.Commands)}>");
			Console.WriteLine($"Insertion Usage: LamuneExtract.exe insert <{string.Join("|", ProgramInsert.Commands)}>");
			Console.WriteLine("The settings for the application can be found in " + LAMUNE_CONFIG + "located next to the exe");
			Environment.Exit(10);
		}
	}
}

## Run
### lamune.cfg
Running `LamuneExtract.exe` requires having a `lamune.cfg` file in the same directory with properties filled out pertaining to what you're doing (more information below).

If `LamuneExtract.exe` is run without a configuration file, a blank one with descriptions of each property will be generated.

If running a debug build, an example `lamune.cfg` has been filled out for reference next to the generated exe to use/customize if preferred to filling out a blank file from scratch.

#### General use properties
These properties are in effect no matter what command is run.

`ShowTime`: List the time taken to perform the command.

### Extract
Run the application in extract mode, with the desired resource type to extract (defaults to tl if none specified).

    LamuneExtract.exe extract <tl|script|script-upgrade|image|exe|custom>
#### tl
Extracts all assets that require translation, specifically scripts, images, and exe strings. This resource is a combination of the script, image, and exe resource types, more information can be found in their respective sections below.

Refer to the script, image, and exe resource type `lamune.cfg` required/optional properties below.

#### script
Extracts ASB scripts from the game archive files and parses them into an intermediary JSON format which can be used for translation (extension `.asb.json`). This involves extracting the scripts from the `scenario.arc` and `data01.arc` archives, where `data01.arc` is the latest update patch released by nekonekosoft. It's not required to have `data01.arc` to extract the scripts, but only the original, non-updated scripts will be extracted.

All the latest scripts (taking first from `data01.arc` if updated, otherwise `scenario.arc`) will be extracted into the output script folder. During extraction, all unique names found will be recorded into a special file `_names.json`, which is exported in the same file structure as the scripts. Table data found in `data01.arc` is referenced for converting index numbers for the commands `load_script` and `music` into their readable string names. If `data01.arc` isn't specified, the outdated tables from `system.arc` will be used.

If specified in the config, a consolidation summary can be printed listing which script files were used from `data01.arc` and `scenario.arc` in the final output.
\n

_Required properties:_ `GameFolder, ScriptFolder`

_Optional properties:_ `OutputConsolidationSummary`

#### script-upgrade
Upgrades a folder of already extracted scripts to have the latest properties for each command in the `.asb.json` files. This allows an existing translation of scripts to be upgraded persisting the translation through the upgrade. Shouldn't need to run this one unless the script format changes (rare).
\n

_Required properties:_ `ScriptFolder`

#### image
Extracts CPB images from the game archive files and converts them into bitmaps that can be translated. This involves extracting the images from the `sysgraph.arc` and `data01.arc` archives, where `data01.arc` is the latest update patch released by nekonekosoft. It's not required to have `data01.arc` to extract the scripts, but only the original, non-updated images will be extracted.

All the latest images (taking first from `data01.arc` if updated, otherwise `sysgraph.arc`) will be extracted into the output image folder. Any image with a prefix in the list provided in the config file will not be extracted, useful for excluding some images that can't be translated.
\n

_Required properties:_ `GameFolder, ImageExtractFolder`

_Optional properties:_ `ImageSkipStartsWith`

#### exe
Extracts all strings from the game executable that can be translated. The strings are output in a simple object format (key is the string to translate, value is the translated string) to an `_exe.json` file in the given folder. It's key that the exe provided to extract from is the correct version, it can be any version of the exe but the same exe should be used for both insertion and extraction. If the `lamune-hack.exe` is not used for extraction, keep in mind that translating some items will break the game (the exe has some patches to get around this).

An important note that this method also extracts some strings that can't, and should not, be translated. This is due to the complexity of finding all these strings. The key/value pair of those entries should be removed from the file before translation.
\n

_Required properties:_ `LamuneExe, ExeFolder`

#### custom
Extracts from a list of game archive files any and all resources found inside. Any images or scripts will be automatically converted into `.bmp` and `.asb.json` files respectively, other files will be directly output with their given extensions. Each archive extracted will put its resources in a separate subfolder in the given output directory.
\n

_Required properties:_ `GameFolder, CustomExtractionFolder, CustomExtractionArcs`

### Insert
Run the application in insert mode, with the desired insertion type (defaults to quick if none specified).

    LamuneExtract.exe insert <quick|full|watch|exe|custom>
#### quick
Inserts all available translated assets (scripts, images, exe) into one game archive, utilizing a caching mechanism to speed up further insertion operations.

If the scripts folder has been provided, all script files will be converted into `.asb` files using the `_names.json` file in the process. A change in the `_names.json` file requires all names to be updated, and invalidates the cache for all script files. Otherwise, only the updated scripts since the last insertion will be converted, the rest will utilise the cache.

If the images folder has been provided, all image files will be converted into `.cpb` files, utilizing the cache if the image hasn't been changed since the last insertion.

Refer to the exe section below for specifics on the exe translation insertion.
\n

_Required properties_: `GameFolder`

_Optional properties:_ `OutputArcName, ScriptFolder, ImageInsertFolder, LamuneExe, ExeFolder`

#### full
Inserts all available translated assets (scripts, images, exe) into one game archive, generating each file from scratch. Refer to the quick section above for details on what this entails, the only difference is the cache is never used.
\n

_Required properties_: `GameFolder`

_Optional properties:_ `OutputArcName, ScriptFolder, ImageInsertFolder, LamuneExe, ExeFolder`

#### watch
Watches for changes in script and/or image files and builds a game archive on the fly. This command will have to be stopped manually once desired. Refer to the quick section above for details on the script/image insertion process, this command utilizes a cache like quick.
\n

_Optional properties:_ `OutputArcName, ScriptFolder, ImageInsertFolder`
#### exe
Takes a filled out `_exe.json` file with string translations and attempts to convert them along with a base exe into a translated exe placed in the game folder. Given the nature of the patching, the translated text is limited to the amount of space the original japanese text took up. In practice, this means for every Japanese character in the original string, two English translated ASCII characters are permitted. If any translated string is found to go over this limit, it will not be translated and an error will be printed to the console.
\n

_Required properties_: `LamuneExe, ExeFolder, GameFolder`

#### custom
Creates a game archive file by encoding all files inside a folder. Currently, a cache folder to speed up subsequent inserts is required to be provided for this operation. Certain types of file will be encoded into a different format before inserting. To summarize, image files (`.bmp, .png`) will be converted into `.cpb` files, script files in `.asb.json` format will be converted into `.asb` files (using the corresponding `_names.json` file in the insertion directory if there is one), and table files in `.tbl.json` format will be converted into `.tbl` files. The created archive uses the name given, or defaults to `data02.arc`. All insertion operations are printed to the console by default, but can be disabled with a flag.
\n

_Required properties:_ `GameFolder, CustomInsertionFolder, InsertionCacheFolder`

_Optional properties:_ `OutputArcName, CustomInsertSuppressOutput`

## Build
Open LamuneExtract.sln in your IDE of choice.
Build either the Debug configuration for testing (includes debug symbols) or the Release configuration for an optimized build in one exe file.
\n

Debug build location: `build/sandbox/LamuneExtract.exe`

Release build location `build/release/LamuneExtract.exe`
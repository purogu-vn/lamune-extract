A set of tools for extracting and inserting the assets of Lamune.

Features:
 - Extraction of translatable text, images, and exe strings into an easy to translate format
 - Insertion of translated assets with ease and speed
 - Customizable parameters with a configuration file
 - Verbose errors when things aren't configured right
 - Custom extraction/insertion methods to take out or put in any files from/to a game archive

Depends on:
 - lamune.exe
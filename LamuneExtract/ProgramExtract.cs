﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using LamuneExtract.Human;
using LamuneExtract.Raw;
using LamuneExtract.Human.Transformer;
using Newtonsoft.Json.Linq;

namespace LamuneExtract {
    internal static class ProgramExtract {

        internal static readonly string[] Commands = { "tl", "script", "script-upgrade", "image", "exe", "custom" };
        
        private static readonly IList<DecryptedMemoryARCFile> loadedArcs = new List<DecryptedMemoryARCFile>();

        internal static void Extract(string action) {
            string data1Path = Config.GetGameFile("data01.arc", false);
            if (data1Path == null) {
                Config.ConfirmOrExit(
                    "No data01.arc location has been specified in the config so only Lamune version 1.0 files will be extracted for translation. Type 'yes' to proceed");
            }

            if (action.Equals("tl") || action.Equals("script")) {
                ExtractScript();
            }

            if (action.Equals("tl") || action.Equals("image")) {
                ExtractImage();
            }

            if (action.Equals("tl") || action.Equals("exe")) {
                ExtractExe();
            }

            if (action.Equals("script-upgrade")) {
                ExtractScriptUpgrade();
            }

            if (action.Equals("custom")) {
                ExtractCustom();
            }
        }

        private static void ExtractScript() {
            string scenarioPath = Config.GetGameFile("scenario.arc");
            string data1Path = Config.GetGameFile("data01.arc", false);
            string tlFolder = Config.GetAndPrepareRequiredDirectory(Config.TL_FOLDER, true);

            IList<DecryptedASBFile> extractedASBupdateFiles =
                data1Path == null ? new List<DecryptedASBFile>() : GetArc(data1Path).ASBFiles;
            IList<DecryptedASBFile> extractedASBoldFiles = GetArc(scenarioPath).ASBFiles;
            Console.WriteLine("Figuring out the latest versions of each .asb file");
            IList<DecryptedASBFile> asbFiles = ConsolidateFiles(extractedASBupdateFiles, extractedASBoldFiles,
                "data01.arc", "scenario.arc");
            string tablePath = data1Path ?? Config.GetGameFile("system.arc");

            Console.WriteLine("Loading scenario table data for load_script name output");
            DecryptedTBLFile scenarioTbl = GetTableFile(tablePath, "scenario.tbl");
            LoadScriptTransformer.LoadScriptMap(scenarioTbl.Table);
            Console.WriteLine("Loading track table data for music name output");
            DecryptedTBLFile trackTbl = GetTableFile(tablePath, "track.tbl");
            MusicTransformer.LoadTrackMap(trackTbl.Table);

            Console.WriteLine("Saving all output files");
            foreach (DecryptedASBFile asbFile in asbFiles) {
                ScriptFile script = ScriptFile.DecodeASB(asbFile);
                script.Save(tlFolder);
            }

            Console.WriteLine("Outputting extracted names");
            NameTransformer.SaveNames(tlFolder);
            Console.WriteLine("Extraction of scripts completed successfully");
        }

        private static void ExtractScriptUpgrade() {
            string tlFolder = Config.GetAndPrepareRequiredDirectory(Config.TL_FOLDER);
            IDictionary<string, JObject> rawTlScripts = new Dictionary<string, JObject>();
            foreach (string filePath in Directory.GetFiles(tlFolder)) {
                rawTlScripts[Path.GetFileName(filePath)] = filePath.ReadAndParseJson();
            }
            ExtractScript();
            foreach (string filePath in Directory.GetFiles(tlFolder)) {
                string fileName = Path.GetFileName(filePath);
                JObject upgradedScript = filePath.ReadAndParseJson();
                JArray upgradedLines = upgradedScript.GetArrayProperty("lines");
                JArray tlLines = rawTlScripts[fileName].GetArrayProperty("lines");
                if (upgradedLines.Count != tlLines.Count) {
                    Console.Error.AndExit($"Unable to upgrade scripts, unequal line counts in {fileName}");
                }

                for (int i = 0; i < upgradedLines.Count; i++) {
                    JObject upgradedLine = upgradedLines[i] as JObject;
                    JObject tlLine = tlLines[i] as JObject;
                    if (upgradedLine.ContainsKey("original") && upgradedLine.ContainsKey("tl")) {
                        upgradedLine["original"] = tlLine["original"];
                        upgradedLine["tl"] = tlLine["tl"];
                        if (tlLine.ContainsKey("finished")) {
                            upgradedLine["finished"] = tlLine["finished"];
                        }
                    }
                }
                filePath.SaveJson(upgradedScript);
            }
        }

        private static void ExtractImage() {
            string sysgraphPath = Config.GetGameFile("sysgraph.arc");
            string data1Path = Config.GetGameFile("data01.arc", false);
            string imageTlPath = Config.GetAndPrepareRequiredDirectory(Config.IMAGE_FOLDER, true);

            IList<DecryptedCPBFile> extractedCPBupdateFiles =
                data1Path == null ? new List<DecryptedCPBFile>() : GetArc(data1Path).CPBFiles;
            Console.WriteLine("Beginning extraction of sysgraph.arc");
            IList<DecryptedCPBFile> extractedCPBoldFiles = GetArc(sysgraphPath).CPBFiles;
            Console.WriteLine("Figuring out the latest versions of each .cpb file");
            IList<DecryptedCPBFile> cpbFiles = ConsolidateFiles(extractedCPBupdateFiles, extractedCPBoldFiles,
                "data01.arc", "sysgraph.arc");
            Console.WriteLine("Saving all output files");
            foreach (DecryptedCPBFile cpbFile in cpbFiles) {
                cpbFile.Save(imageTlPath);
            }

            Console.WriteLine("Extraction of images completed successfully");
        }

        private static void ExtractExe() {
            string exeFile = Config.GetRequiredFile(Config.LAMUNE_EXE);
            string tlFolder = Config.GetAndPrepareRequiredDirectory(Config.EXE_TL_FOLDER, true);
            ExeStrings.Extract(exeFile, tlFolder);
            Console.WriteLine("Extraction of exe possible text completed");
        }

        private static void ExtractCustom() {
            string customExtractFolder = Config.GetAndPrepareRequiredDirectory(Config.CUSTOM_EXTRACT_FOLDER);
            string gameFolder = Config.GetAndPrepareRequiredDirectory(Config.GAME_FOLDER);
            foreach (string arcName in Config.GetList(Config.CUSTOM_ARCS)) {
                string arcSubFolder = Path.Combine(customExtractFolder, arcName);
                Directory.CreateDirectory(arcSubFolder);
                DecryptedLazyARCFile arc = new DecryptedLazyARCFile(Path.Combine(gameFolder, arcName));
                foreach (LamuneFile file in arc.AllFiles) {
                    file.Save(arcSubFolder);
                }
            }
        }

        private static DecryptedTBLFile GetTableFile(string data1Path, string tblFile) {
            return GetArc(data1Path).TblFiles.First(file => file.FileName.Equals(tblFile));
        }

        private static DecryptedMemoryARCFile GetArc(string path) {
            string fileName = Path.GetFileName(path);
            DecryptedMemoryARCFile existingArc = loadedArcs.FirstOrDefault(arc => arc.FileName.Equals(fileName));
            if (existingArc == null) {
                Console.WriteLine($"Beginning {fileName} extraction of decrypted files");
                DecryptedMemoryARCFile newArc = new DecryptedMemoryARCFile(path);
                loadedArcs.Add(newArc);
                return newArc;
            }

            return existingArc;
        }
        
        private static IList<T> ConsolidateFiles<T>(IList<T> updatedFiles, IList<T> oldFiles, string updatedName,
            string oldName)
            where T : LamuneFile {
            IList<T> uniques = new List<T>();
            ISet<string> obtainedFileNames = new HashSet<string>();
            int updated = 0;
            int deleted = 0;
            int created = 0;
            foreach (T file in updatedFiles) {
                uniques.Add(file);
                obtainedFileNames.Add(file.FileName);
            }

            foreach (T file in oldFiles) {
                if (obtainedFileNames.Contains(file.FileName)) {
                    obtainedFileNames.Remove(file.FileName);
                    updated++;
                }
                else {
                    uniques.Add(file);
                    deleted++;
                }
            }

            created += obtainedFileNames.Count;

            if (Config.GetBoolean(Config.MERGE_SUMMARY)) {
                Console.WriteLine("Consolidation Summary:");
                Console.WriteLine($"{deleted + updated} {oldName} files");
                Console.WriteLine($"{created + updated} {updatedName} files");
                Console.WriteLine($"Using {deleted} files from {oldName}");
                Console.WriteLine($"Using {created + updated} files from {updatedName}");
                Console.WriteLine($"Using {created} unique files from {updatedName}");
                Console.WriteLine("Using " + (created + updated + deleted) + " files total");
            }

            return uniques;
        }
    }
}
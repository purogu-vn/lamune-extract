﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace LamuneExtract {
	internal static class ExeStrings {

		private const int DATA_BEGIN = 390806;
		private const int DATA_END = 429000;
		internal const string JSON_FILE = "_exe.json";

		private static bool ValidUnit(string unit, bool allAscii, bool allJis) {
			if(allAscii) {
				return false;
			}
			if(!allJis && unit.Length <= 2) {
				return false;
			}
			return true;
		}

		private static string AdvanceNextUnit(MemoryStream str, BinaryReader br, int length) {
			string unit = "";
			bool allAscii = true;
			bool allJis = true;
			while(str.Position < length) {
				byte next = br.ReadByte();
				if (HexHelp.IsJisByte1(next) && str.Position < length) {
					byte after = br.ReadByte();
					if (HexHelp.IsJisByte2(after)) {
						unit += Config.SHIFT_JIS.GetString(new[] { next, after });
						allAscii = false;
					}
					else {
						str.Position -= 1;
					}
				}
				else if (HexHelp.IsPrintableASCII(next) || next == 0x0a || next == 0x0D) {
					unit += Config.SHIFT_JIS.GetString(new[] { next });
					allJis = false;
				}
				else if(unit.Length > 0) {
					if(ValidUnit(unit, allAscii, allJis)) {
						return unit;
					}

					unit = "";
					allAscii = true;
					allJis = true;
				}
			}
			return null;
		}

		internal static void Extract(string exeFile, string outPath) {
			byte[] data = File.ReadAllBytes(exeFile);
			byte[] sliced = new byte[DATA_END - DATA_BEGIN];
			Array.Copy(data, DATA_BEGIN, sliced, 0, DATA_END - DATA_BEGIN);
			JObject units = new JObject();
			using (MemoryStream str = new MemoryStream(sliced)) {
				using (BinaryReader br = new BinaryReader(str)) {
					while(str.Position < sliced.Length) {
						string unit = AdvanceNextUnit(str, br, sliced.Length);
						if(unit != null) {
							Console.WriteLine(unit);
							units[unit] = "";
						}
					}
				}
			}
			Console.WriteLine($"Found {units.Count} possible units to translate");
			Path.Combine(outPath, JSON_FILE).SaveJson(units);
		}

		private static IDictionary<string, string> GetTranslations(string tlPath) {
			string tlFile = Path.Combine(tlPath, JSON_FILE);
			JObject jUnits = tlFile.ReadAndParseJson();
			IDictionary<string, string> translations = new Dictionary<string, string>();
			foreach (JProperty unit in jUnits.Properties()) {
				string tlStr = unit.Value.ToString();
				if (tlStr.Length > 0) {
					string originalStr = unit.Name;
					int originalBytes = Config.SHIFT_JIS.GetByteCount(originalStr);
					int tlBytes = Config.SHIFT_JIS.GetByteCount(tlStr);
					if (tlBytes > originalBytes) {
						Console.Error.WriteLine(
							$"Error: {originalStr} translation of {tlStr} is too long (max {originalBytes} characters, used {tlBytes})");
						Console.Error.WriteLine("Will not be used");
					}
					else {
						translations.Add(originalStr, tlStr);
					}
				}
			}
			return translations;
		}

		internal static void Insert(string exeFile, string tlPath, string outFile) {
			IDictionary<string, string> translations = GetTranslations(tlPath);
			byte[] data = File.ReadAllBytes(exeFile);
			using (MemoryStream str = new MemoryStream(data)) {
				using (BinaryReader br = new BinaryReader(str)) {
					using(BinaryWriter bw = new BinaryWriter(str)) {
						while (str.Position < data.Length) {
							string unit = AdvanceNextUnit(str, br, data.Length);
							if (unit != null && translations.ContainsKey(unit)) {
								int originalBytes = Config.SHIFT_JIS.GetByteCount(unit);
								byte[] tlBytes = Config.SHIFT_JIS.GetBytes(translations[unit]);
								// back to data start
								str.Position -= originalBytes + 1;
								bw.Write(tlBytes);
								int diff = originalBytes - tlBytes.Length;
								for(int i = 0; i < diff; i++) {
									bw.Write((byte)0);
								}
							}
						}
					}
				}
			}
			File.WriteAllBytes(outFile, data);
		}
	}
}

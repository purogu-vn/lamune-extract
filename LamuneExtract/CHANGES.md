## In progress
 - Rename CustomInsertionArcName to be OutputArcName, and allow it to be specified for any arc-building insertion operation
 - Remove PutInLoadScriptNames config option to always put in script names
## v1.0.1
 - Changes to support textareas in the suite by treating newlines as force breaks
## v1.0.0
 - Initial release
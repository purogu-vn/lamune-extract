﻿using System.Collections.Generic;

namespace LamuneExtract {
	internal class Config : ExtractCommon.Config {
		internal const string TL_FOLDER = "ScriptFolder";
		internal const string IMAGE_FOLDER = "ImageExtractFolder";
		internal const string IMAGE_TL_FOLDER = "ImageInsertFolder";
		internal const string EXE_TL_FOLDER = "ExeFolder";
		internal const string CACHE_FOLDER = "InsertionCacheFolder";
		internal const string LAMUNE_EXE = "LamuneExe";

		internal const string CUSTOM_EXTRACT_FOLDER = "CustomExtractionFolder";
		internal const string CUSTOM_INSERT_FOLDER = "CustomInsertionFolder";
		internal const string CUSTOM_ARCS = "CustomExtractionArcs";
		internal const string CUSTOM_ARC_NAME = "OutputArcName";

		internal const string MERGE_SUMMARY = "OutputConsolidationSummary";
		internal const string SKIP_IMAGE_PREFIX = "ImageSkipStartsWith";
		internal const string SHOW_TIME = "ShowTime";
		internal const string WORD_WRAP_LENGTH = "WordWrapLineLength";
		internal const string CUSTOM_SUPRESS_OUTPUT = "CustomInsertSupressOutput";

		private static readonly IDictionary<string, string> DEFAULTS = new Dictionary<string, string> {
			[TL_FOLDER] = "[Required (script). The folder containing the .asb.json files for each script]",
			[IMAGE_FOLDER] = "[Required (image extraction). The folder to extract image bitmaps of the game to]",
			[IMAGE_TL_FOLDER] = "[Required (image insertion). The folder containing modified bitmaps to insert into the game]",
			[EXE_TL_FOLDER] = "[Required (exe). The folder containing the assets to translate the exe]",
			[CACHE_FOLDER] = "[Required (insertion quick custom). A place to store previously encrypted scripts/images to speed up insertion]",
			[LAMUNE_EXE] = "[Required (exe). The base exe to use, should be the hacked version that supports exe translation]",

			[CUSTOM_EXTRACT_FOLDER] = "[Required (extract custom). Where specific arcs are extracted to]",
			[CUSTOM_INSERT_FOLDER] = "[Required for (insert custom). Extra files to include in the final arc]",
			[CUSTOM_ARCS] = "[Required (extract custom). Which arc files to extract]",
			[CUSTOM_ARC_NAME] = "[Optional (insert). The name of the produced arc file, default is data02.arc]",

			[MERGE_SUMMARY] = "[Optional. Outputs which files were used from which archive when consolidating .asb files from scenario.arc and data01.arc]",
			[SKIP_IMAGE_PREFIX] = "[Optional. When extracting images, which starts of file names to not extract]",
			[SHOW_TIME] = "[Optional. Shows the time taken during extraction/insertion in milliseconds]",
			[WORD_WRAP_LENGTH] = "[Optional. How long each line should be until it becomes auto wrapped to the next line. The game will split words in half if this number is too long (over 52 characters).]",
			[CUSTOM_SUPRESS_OUTPUT] = "[Optional. If custom insertion shouldn't print for every file that gets inserted"
		};

		private Config(string path) : base(path, DEFAULTS) { }

		internal static void InitConfig(string path) {
			InitConfig(new Config(path)); 
		}
	}
}

﻿using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using LamuneExtract.Human.Transformer;
using LamuneExtract.Raw;

namespace LamuneExtract.Human {
	internal class ScriptFile {

		internal string FileName { get; }
		private IList<ScriptCommand> Commands { get; }
		
		private ScriptFile(string fileName, IList<ScriptCommand> commands) {
			FileName = fileName;
			Commands = commands;
		}

		internal static ScriptFile FromFile(string path) {
			string fileName = Path.GetFileNameWithoutExtension(path);
			IList<ScriptCommand> commands = new List<ScriptCommand>();
			JObject jScript = path.ReadAndParseJson();
			JArray jLines = jScript.GetArrayProperty("lines");
			foreach(JObject jLine in jLines) {
				// possible split
				string command = jLine.GetStringProperty("command");
				if(command.Equals("text") && jLine.GetStringProperty("tl").Length > 0) {
					foreach(string screen in new WordWrapper(jLine.GetStringProperty("tl"))) {
						jLine["tl"] = screen;
						commands.Add(new ScriptCommand(jLine));
					}
				}
				else {
					if (jLine.ContainsKey("jump")) {
						int jumpDistance = jLine.GetIntProperty("jump");
						BranchTransformer.StoreBranchJump(commands.Count, jumpDistance);
					}
					commands.Add(new ScriptCommand(jLine));
				}
			}
			return new ScriptFile(fileName, commands);
		}

		internal static ScriptFile DecodeASB(DecryptedASBFile decFile) {
			string fileName = decFile.FileName;
			byte[] fileData = decFile.DataCopy();
			IList<ScriptCommand> commands = new List<ScriptCommand>();
			using (MemoryStream str = new MemoryStream(fileData)) {
				using (BinaryReader bRead = new BinaryReader(str)) {
					while (bRead.PeekChar() != -1) {
						int command = bRead.ReadInt32();
						int totalLength = bRead.ReadInt32();
						int[] arguments = new int[4];
						for (int i = 0; i < arguments.Length; i++) {
							arguments[i] = bRead.ReadInt32();
						}
						byte[] data = { };
						//exit command
						if(totalLength >= 0) {
							// subtract the length of the command, length field, and arguments
							int leftToRead = totalLength - 0x18;
							data = bRead.ReadBytes(leftToRead);
						}
						ScriptCommand sc = new ScriptCommand((int)str.Position, command, arguments, data);
						commands.Add(sc);
					}
				}
			}
			return new ScriptFile(fileName, commands);
		}
		
		internal void Save(string folder) {
			string path = Path.Combine(folder, FileName + ".json");
			JObject jScript = new JObject {
				new JProperty("lines", JsonLines())
			};
			path.SaveJson(jScript);
		}

		private JArray JsonLines() {
			JArray lines = new JArray();
			foreach(ScriptCommand line in Commands) {
				lines.Add(line.ToJson());
				BranchTransformer.ProcessBranchReferences(line);
			}
			BranchTransformer.EnsureBranchesProcessed();
			return lines;
		}

		internal DecryptedASBFile PackASB() {
			using (MemoryStream str = new MemoryStream()) {
				using (BinaryWriter bWrite = new BinaryWriter(str)) {
					foreach (ScriptCommand sc in Commands) {
						sc.EstablishPosition(str);
						bWrite.Write(sc.Command);
						byte[] data = sc.Data;
						int totalLength = 0x18 + data.Length;
						// exit command has FFFF length
						bWrite.Write(sc.Command == 0 ? -1 : totalLength);
						foreach(int arg in sc.Arguments) {
							bWrite.Write(arg);
						}
						bWrite.Write(data);
					}
					foreach (var patch in BranchTransformer.CommandBranchJumps) {
						ScriptCommand branchCommand = Commands[patch.Key];
						ScriptCommand jumpCommand = Commands[patch.Key + patch.Value];
						// skip to argument 0 to override absolute jump
						str.Position = branchCommand.Position + 8;
						bWrite.Write(jumpCommand.Position);
					}
					BranchTransformer.BranchJumpsReset();
				}
				return new DecryptedASBFile(FileName, str.ToArray());
			}	
		}
	}
}

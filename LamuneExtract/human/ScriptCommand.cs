﻿using LamuneExtract.Human.Transformer;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace LamuneExtract.Human {
	internal class ScriptCommand {

		private static readonly IDictionary<int, CommandTransformer> COMMAND_MAP = new Dictionary<int, CommandTransformer> {
			[0x00] = new CommandTransformer("exit"),
			[0x04] = new SimpleTextTransformer("window_title"),
			[0x05] = new LoadScriptTransformer("load_script"),
			[0x0A] = new BranchTransformer("branchflag"),
			[0x0B] = new BranchTransformer("branch"),
			[0x0C] = new BranchTransformer("jump"),
			[0x0D] = new ChoiceTransformer("choice"),
			[0x12] = new GotoLabelTransformer("gotolabel"),
			[0x26] = new NameTransformer("name"),
			[0x27] = new TextTransformer("text"),
			[0x32] = new CommandTransformer("figure"),
			[0x41] = new CommandTransformer("bg"),
			[0x5F] = new MusicTransformer("music"),
			[0x66] = new CommandTransformer("wav"),
			[0x6A] = new CommandTransformer("ogg"),
		};
		
		internal int Position { get; private set; }
		internal int Command { get; }
		internal int[] Arguments { get; }
		internal byte[] Data { get; }
		private CommandTransformer Transformer { get; }

		internal ScriptCommand(int position, int command, int[] arguments, byte[] data) {
			Position = position;
			Command = command;
			Arguments = arguments;
			Transformer = LookupTransformer(command);
			Data = data;
		}

		internal ScriptCommand(JObject json) {
			Command = ParseCommand(json.GetStringProperty("command"));
			JArray jArguments = json.GetArrayProperty("arguments");
			Arguments = new int[jArguments.Count];
			for (int i = 0; i < Arguments.Length; i++) {
				Arguments[i] = int.Parse(jArguments[i].ToString());
			}
			Transformer = LookupTransformer(Command);
			Data = Transformer.GetData(json);
			Transformer.AdjustArguments(json, Arguments);
		}

		internal void EstablishPosition(Stream str) {
			Position = (int)str.Position;
		}

		private CommandTransformer LookupTransformer(int command) {
			return COMMAND_MAP.ContainsKey(command) ? COMMAND_MAP[command] : new CommandTransformer(command);
		}

		public override string ToString() {
			return Transformer.Label + ": " + Transformer.DataString(Data);
		}

		internal JObject ToJson() {
			JObject json = new JObject {
				new JProperty("command", Transformer.Label),
				new JProperty("arguments", new JArray(Arguments))
			};
			foreach(JProperty prop in Transformer.GetJson(this)) {
				json.Add(prop);
			}
			return json;
		}

		private int ParseCommand(string command) {
			foreach (KeyValuePair<int, CommandTransformer> pair in COMMAND_MAP) {
				if (pair.Value.Label.Equals(command)) {
					return pair.Key;
				}
			}
			return Convert.ToInt32(command, 16);
		}
	}
}

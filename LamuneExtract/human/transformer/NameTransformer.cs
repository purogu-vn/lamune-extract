﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;

namespace LamuneExtract.Human.Transformer {
	internal class NameTransformer : CommandTransformer {
		internal const string OUTPUT_FILE = "_names.asb.json";
		internal static string CurrentName { get; private set; }
		private static IDictionary<string, string> tlNames = new SortedDictionary<string, string>();

		internal NameTransformer(string label) : base(label) {
		}

		internal static void SaveNames(string folder) {
			JArray jsonLines = new JArray();
			foreach(string name in tlNames.Keys) {
				jsonLines.Add(new JObject {
					new JProperty("command", "name-def"),
					new JProperty("original", name),
					new JProperty("tl", ""),
					new JProperty("finished", false)
				});
			}
			JObject jScript = new JObject {
				new JProperty("lines", jsonLines)
			};
			Path.Combine(folder, OUTPUT_FILE).SaveJson(jScript);
		}

		internal static bool LoadNames(string folder) {
			string path = Path.Combine(folder, OUTPUT_FILE);
			if (File.Exists(path)) {
				JObject jScript = path.ReadAndParseJson();
				JArray jLines = jScript.GetArrayProperty("lines");
				tlNames = new SortedDictionary<string, string>();
				foreach (JObject line in jLines) {
					string name = line.GetStringProperty("original");
					tlNames[name] = line.GetStringProperty("tl");
				}
				return true;
			}

			return false;
		}

		internal override byte[] GetData(JObject json) {
			string original = json.GetStringProperty("original");
			string tl = tlNames.ContainsKey(original) ? tlNames[original] : "";
			string inUse = tl.Length > 0 ? tl : original;
			return Config.SHIFT_JIS.GetBytes(original.Length > 0 ? inUse + '\0' : "");
		}

		internal override IList<JProperty> GetJson(ScriptCommand command) {
			string name = Config.SHIFT_JIS.GetString(command.Data).TrimEnd('\0');
			if(name.Length > 0) {
				if(!tlNames.ContainsKey(name)) {
					tlNames[name] = "";
				}
				CurrentName = name;
			}
			else {
				CurrentName = null;
			}
			return new List<JProperty> {
				new JProperty("original", name)
			};
		}
	}
}

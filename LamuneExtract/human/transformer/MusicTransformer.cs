﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace LamuneExtract.Human.Transformer {
	internal class MusicTransformer : CommandTransformer {
		private static IDictionary<int, string> trackMap = new Dictionary<int, string>();
		internal MusicTransformer(string label) : base(label) {
		}

		internal static void LoadTrackMap(IDictionary<int, string> map) {
			trackMap = map;
		}

		private string LookupScript(int key) {
			if(trackMap.ContainsKey(key)) {
				return trackMap[key];
			}
			return null;
		}

		internal override IList<JProperty> GetJson(ScriptCommand command) {
			IList<JProperty> properties = base.GetJson(command);
			string musicName = LookupScript(command.Arguments[2]);
			if(musicName != null) {
				properties.Add(new JProperty("music_name", musicName));
			}
			return properties;
		}
	}
}

﻿namespace LamuneExtract.Human.Transformer {
    internal class GotoLabelTransformer : LoadScriptTransformer {
        internal GotoLabelTransformer(string label) : base(label) {
        }

        protected override string LookupScript(ScriptCommand command) {
            // if flag set to goto a label in a different file
            if (command.Arguments[2] == 1) {
                return base.LookupScript(command);
            }
            return null;
        }
    }
}
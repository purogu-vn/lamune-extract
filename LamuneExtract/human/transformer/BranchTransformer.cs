﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace LamuneExtract.Human.Transformer {
    internal class BranchTransformer : CommandTransformer {
        
        private static readonly IDictionary<int, JProperty> BackReferences = new Dictionary<int, JProperty>();
        private static readonly IDictionary<int, int> BackReferenceCommandCount = new Dictionary<int, int>();
        internal static readonly IDictionary<int, int> CommandBranchJumps = new Dictionary<int, int>();
        internal BranchTransformer(string label) : base(label) {
        }

        internal override IList<JProperty> GetJson(ScriptCommand command) {
            int jumpTo = command.Arguments[0];
            IList<JProperty> json = base.GetJson(command);
            JProperty jumpProperty = new JProperty("jump", 1);
            json.Add(jumpProperty);
            BackReferences[jumpTo] = jumpProperty;
            BackReferenceCommandCount[jumpTo] = 0;
            return json;
        }

        internal static void ProcessBranchReferences(ScriptCommand command) {
            foreach (int key in BackReferences.Keys) {
                BackReferenceCommandCount[key] += 1;
            }
            if (BackReferences.ContainsKey(command.Position)) {
                BackReferences[command.Position].Value = BackReferenceCommandCount[command.Position];
                BackReferences.Remove(command.Position);
                BackReferenceCommandCount.Remove(command.Position);
            }
        }

        internal static void EnsureBranchesProcessed() {
            if (BackReferences.Count > 0) {
                throw new Exception("Branch jumped out of bounds!");
            }
        }

        internal static void StoreBranchJump(int commandIdx, int jump) {
            CommandBranchJumps[commandIdx] = jump;
        }

        internal static void BranchJumpsReset() {
            CommandBranchJumps.Clear();
        }
    }
}
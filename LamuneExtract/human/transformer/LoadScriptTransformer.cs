﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace LamuneExtract.Human.Transformer {
	internal class LoadScriptTransformer : CommandTransformer {
		private static IDictionary<int, string> scriptMap = new Dictionary<int, string>();
		internal LoadScriptTransformer(string label) : base(label) {
		}

		internal static void LoadScriptMap(IDictionary<int, string> map) {
			scriptMap = map;
		}

		protected virtual string LookupScript(ScriptCommand command) {
			int key = command.Arguments[1];
			if(scriptMap.ContainsKey(key)) {
				return scriptMap[key];
			}
			return null;
		}

		internal override IList<JProperty> GetJson(ScriptCommand command) {
			IList<JProperty> properties = base.GetJson(command);
			string scriptName = LookupScript(command);
			if(scriptName != null) {
				properties.Add(new JProperty("script_name", scriptName));
			}
			return properties;
		}
	}
}

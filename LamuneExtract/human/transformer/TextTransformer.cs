﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace LamuneExtract.Human.Transformer {
	internal class TextTransformer : SimpleTextTransformer {
		internal TextTransformer(string label) : base(label) {
		}

		internal override string FormatTranslation(string tl, JObject json) {
			if(json["speaker"] == null || !json.GetBoolProperty("autoQuote")) {
				return tl;
			}

			return "“" + tl + "”";
		}

		internal override IList<JProperty> GetJson(ScriptCommand command) {
			var json = base.GetJson(command);
			if(NameTransformer.CurrentName != null) {
				json.Add(new JProperty("speaker", NameTransformer.CurrentName));
				json.Add(new JProperty("autoQuote", true));
			}
			return json;
		}

		internal override void AdjustArguments(JObject json, int[] arguments) {
			// If ruby text flag is set, unset it
			if (ShouldTranslate(json) && arguments[1] == 4096) {
				arguments[1] = 0;
			}
		}
	}
}

﻿using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace LamuneExtract.Human.Transformer {
	internal class ChoiceTransformer : CommandTransformer {
		internal ChoiceTransformer(string label) : base(label) {
		}

		internal override byte[] GetData(JObject json) {
			JArray originalArr = json.GetArrayProperty("original");
			JArray tlArr = json.GetArrayProperty("tl");
			string[] choices = new string[originalArr.Count];
			for(int i = 0; i < choices.Length; i++) {
				string original = originalArr[i].ToString();
				string tl = tlArr[i].ToString();
				choices[i] = tl.Length > 0 ? tl : original;
			}
			string rawLine = string.Join("\0", choices) + '\0';
			return Config.SHIFT_JIS.GetBytes(rawLine);
		}

		internal override IList<JProperty> GetJson(ScriptCommand command) {
			string rawLine = Config.SHIFT_JIS.GetString(command.Data).TrimEnd('\0');
			string[] choices = rawLine.Split('\0');
			string[] tlChoices = new string[choices.Length];
			for(int i = 0; i < tlChoices.Length; i++) {
				tlChoices[i] = "";
			}
			return new List<JProperty> {
				new JProperty("original", new JArray(choices)),
				new JProperty("tl", new JArray(tlChoices)),
				new JProperty("finished", false)
			};
		}
	}
}

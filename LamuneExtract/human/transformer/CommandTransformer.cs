﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace LamuneExtract.Human.Transformer {
	internal class CommandTransformer {
		internal string Label { get; }

		internal CommandTransformer(int command) : this("0x" + command.ToString("X2")) {

		}

		internal CommandTransformer(string label) {
			Label = label;
		}

		internal virtual string DataString(byte[] data) {
			return $"<DATA> ({data.Length})";
		}

		internal virtual IList<JProperty> GetJson(ScriptCommand command) {
			return new List<JProperty> {
				new JProperty("data", Convert.ToBase64String(command.Data))
			};
		}

		internal virtual byte[] GetData(JObject json) {
			string b64Data = json.GetStringProperty("data");
			return Convert.FromBase64String(b64Data);
		}

		internal virtual void AdjustArguments(JObject json, int[] arguments) {
			
		}
	}
}

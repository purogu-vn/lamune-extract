﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace LamuneExtract.Human.Transformer {
	internal class SimpleTextTransformer : CommandTransformer {
		internal SimpleTextTransformer(string label) : base(label) {
		}

		internal override string DataString(byte[] data) {
			return Label + ": " + Config.SHIFT_JIS.GetString(data);
		}

		internal virtual string FormatTranslation(string tl, JObject json) {
			return tl;
		}

		internal bool ShouldTranslate(JObject json) {
			return json.GetStringProperty("tl").Length > 0 || json.GetBoolProperty("finished");
		}

		internal override byte[] GetData(JObject json) {
			string original = json.GetStringProperty("original");
			string tl = json.GetStringProperty("tl");
			string inUse = ShouldTranslate(json) ? FormatTranslation(tl, json) : original;
			// only pad with 0 if there was data
			return Config.SHIFT_JIS.GetBytes(original.Length > 0 ? inUse + '\0' : "");
		}

		internal override IList<JProperty> GetJson(ScriptCommand command) {
			return new List<JProperty> {
				new JProperty("original", Config.SHIFT_JIS.GetString(command.Data).TrimEnd('\0')),
				new JProperty("tl", ""),
				new JProperty("finished", false)
			};
		}
	}
}

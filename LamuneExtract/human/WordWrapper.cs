﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LamuneExtract.Human {
	internal class WordWrapper : IEnumerable<string> {
		private const string FORCE_BREAK = "\\n";
		private static readonly string[] EXPAND_STRINGS = { "<FN>", "<NN>" };
		private const int EXPAND_LENGTH = 8;
		private static readonly int LINE_LENGTH = Config.GetOptionalNumber(Config.WORD_WRAP_LENGTH, 52);
		
		private IList<string> Screens { get; }
		private StringBuilder screen;
		private StringBuilder line;
		private int lineNum;
		private int position;
		private int calculatedLineLength;

		internal WordWrapper(string text) {
			screen = new StringBuilder();
			line = new StringBuilder();
			Screens = new List<string>();
			lineNum = 1;
			position = 0;
			calculatedLineLength = 0;
			Wrap(text);
		}

		private void BreakLine() {
			if (lineNum >= 3) {
				screen.Append(line);
				Screens.Add(screen.ToString());
				screen.Clear();
				lineNum = 1;
			}
			else {
				screen.Append(line + "\\n");
				lineNum++;
			}
			line.Clear();
			calculatedLineLength = 0;
		}

		private string ReadNextWord(string text) {
			StringBuilder word = new StringBuilder();
			while (position < text.Length) {
				if (text[position] == ' ') {
					position++;
					if (word.Length > 0) {
						return word.ToString();
					}
				}
				else if (position + 1 < text.Length && text.Substring(position, 2).Equals(FORCE_BREAK)) {
					if (word.Length > 0) {
						return word.ToString();
					}
					else {
						position += 2;
						return FORCE_BREAK;
					}
				}
				else {
					word.Append(text[position]);
					position++;
				}
			}
			return word.Length > 0 ? word.ToString() : null;
		}

		private void Wrap(string text) {
			// Treat any newlines as force breaks
			text = text.Trim().Replace("\n", "\\n");
			string word = ReadNextWord(text);
			while (word != null) {
				if (word.Equals(FORCE_BREAK)) {
					BreakLine();
				}
				else {
					bool shouldExpand = EXPAND_STRINGS.Contains(word);
					bool notFirstWord = calculatedLineLength > 0;
					string wordWithSpace = notFirstWord ? " " + word : word;
					int wordWithSpaceLength = shouldExpand ? EXPAND_LENGTH : wordWithSpace.Length;
					if (calculatedLineLength + wordWithSpaceLength <= LINE_LENGTH) {
						line.Append(wordWithSpace);
						calculatedLineLength += wordWithSpaceLength;
					}
					else {
						BreakLine();
						line.Append(word);
						calculatedLineLength += word.Length;
					}
				}
				word = ReadNextWord(text);
			}
			if (line.Length > 0) {
				screen.Append(line);
			}
			if (screen.Length > 0) {
				Screens.Add(screen.ToString());
			}
		}

		public IEnumerator<string> GetEnumerator() {
			return Screens.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return Screens.GetEnumerator();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;

namespace LamuneExtract {
	internal class DirectoryWatcher {
		private static readonly IList<DirectoryWatcher> watchers = new List<DirectoryWatcher>();
		private DateTime lastUpdate;
		private FileSystemWatcher watcher;
		private Action action;

		internal DirectoryWatcher(string path, Action action) {
			this.action = action;
			lastUpdate = DateTime.MinValue;
			watcher = new FileSystemWatcher(path);
			watcher.Changed += Watcher_Changed;
			watcher.NotifyFilter = NotifyFilters.LastWrite;
			watcher.EnableRaisingEvents = true;
		}

		internal void Watcher_Changed(object sender, FileSystemEventArgs e) {
			DateTime writeTime = File.GetLastWriteTime(e.FullPath);
			if(writeTime.Subtract(lastUpdate).TotalMilliseconds > 500) {
				Console.WriteLine("Change detected");
				lastUpdate = writeTime;
				action();
			}
		}

		internal static void RegisterWatcher(string path, Action action) {
			watchers.Add(new DirectoryWatcher(path, action));
		}
	}
}

﻿using Ionic.Crc;
using Ionic.Zlib;
using System;
using System.IO;

namespace LamuneExtract {
	internal static class ZLib {

		internal static byte[] Compress(byte[] data) {
			byte[] compData = ZlibStream.CompressBuffer(data);
			CRC32 crc = new CRC32();
			crc.SlurpBlock(compData, 0, compData.Length);
			byte[] checksumBytes = BitConverter.GetBytes(crc.Crc32Result);
			using (MemoryStream checksumDataStream = new MemoryStream()) {
				checksumDataStream.Write(checksumBytes, 0, checksumBytes.Length);
				checksumDataStream.Write(compData, 0, compData.Length);
				return checksumDataStream.ToArray();
			}
		}

		internal static byte[] Decompress(byte[] checksumAndData, uint uncompressedLength) {
			// read unencrypted checksum
			uint storedChecksum = BitConverter.ToUInt32(checksumAndData, 0);
			// zlib format: 4 byte header (0x78DA), data, 4 byte checksum
			// start reading compressed data starting after stored checksum (starting with 0x78DA)
			using (MemoryStream ms = new MemoryStream(checksumAndData, 4, (int)checksumAndData.Length - 4)) {
				//calculate checksum from zlib stream
				uint calculatedChecksum = (uint)new CRC32().GetCrc32(ms);
				if (calculatedChecksum != storedChecksum) {
					throw new InvalidDataException("Checksums unequal");
				}
				// checksum calculation moved stream position, reset it
				ms.Position = 0;
				using (ZlibStream decompStream = new ZlibStream(ms, CompressionMode.Decompress, true)) {
					using (BinaryReader decompReader = new BinaryReader(decompStream)) {
						//decompress the bytes
						byte[] decompBytes = decompReader.ReadBytes((int)uncompressedLength);
						// overwrite the data
						return decompBytes;
					}

				}
			}
		}
	}
}

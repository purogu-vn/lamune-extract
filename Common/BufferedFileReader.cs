﻿using System;
using System.IO;

namespace ExtractCommon {
	public class BufferedFileReader : IDisposable {

		public Stream Stream { get; }
		public BinaryReader Reader { get; }

		private readonly FileStream fileStream;

		public BufferedFileReader(string path) {
			fileStream = File.OpenRead(path);
			Stream = new BufferedStream(fileStream);
			Reader = new BinaryReader(Stream);
		}
		public void Dispose() {
			fileStream.Dispose();
			Stream.Dispose();
			Reader.Dispose();
		}
	}
}

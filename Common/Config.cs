﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExtractCommon {
	public abstract class Config {
		public static readonly Encoding SHIFT_JIS = Encoding.GetEncoding("shift_jis");
		
		public const string GAME_FOLDER = "GameFolder";

		private IDictionary<string, string> Settings { get; }

		private static Config Instance;

		private static IDictionary<string, string> InstanceSettings {
			get {
				return Instance.Settings;
			}
		}
		
		private static readonly IDictionary<string, string> DEFAULTS = new Dictionary<string, string> {
			[GAME_FOLDER] = "[Required. The folder containing the original game exe and .arc files]"
		};

		protected Config(string path, IDictionary<string, string> additionalDefaults) {
			foreach (var pair in additionalDefaults) {
				DEFAULTS[pair.Key] = pair.Value;
			}
			Settings = new Dictionary<string, string>();
			if (!File.Exists(path)) {
				StringBuilder defaultConfig = new StringBuilder();
				foreach (var pair in DEFAULTS) {
					defaultConfig.AppendLine(pair.Key + "=" + pair.Value);
				}
				File.WriteAllText(path, defaultConfig.ToString());
				Console.Error.AndExit("No config file found. A blank one has been created in the same folder as the exe, please fill it out before continuing");
			}
			foreach (string line in File.ReadAllLines(path)) {
				if (line.StartsWith(";") || line.Length == 0) {
					continue;
				}
				string[] pair = line.Split(new[] { '=' }, 2);
				if (pair.Length != 2) {
					Console.Error.AndExit($"Misconfigured config file, unable to parse key=value for line: {line}");
				}
				string val = pair[1].Replace("\\", "\\\\");
				if (DEFAULTS.ContainsKey(pair[0])) {
					Settings.Add(pair[0], val);
				}
			}
		}

		public static void InitConfig(Config config) {
			Instance = config;
		}

		private static string NormalizePath(string path) {
			return Path.GetFullPath(path);
		}

		public static string GetGameFile(string file, bool required=true) {
			string gameFolder = GetAndPrepareRequiredDirectory(GAME_FOLDER);
			string filePath = Path.Combine(gameFolder, file);
			bool exists = File.Exists(filePath);
			if (required && !exists) {
				Console.Error.AndExit($"Missing required file for extraction: {file}");
			}
			return exists ? NormalizePath(filePath) : null;
		}

		public static string GetRequiredFile(string key) {
			string file = GetRequiredSetting(key);
			FileInfo info = new FileInfo(file);
			if (!info.Exists) {
				Console.Error.AndExit("The config value " + key + " must exist");
			}
			return NormalizePath(file);
		}

		public static string GetAndPrepareRequiredDirectory(string key, bool warnEmpty=false) {
			string dir = GetRequiredSetting(key);
			try {
				if (Directory.Exists(dir)) {
					if(Directory.GetFiles(dir).Length > 0 && warnEmpty) {
						ConfirmOrExit($"Are you sure you would like to extract? The {key} is non empty, which could mean losing progress. Continue? (yes/no)");
					}
				}
				else {
					Directory.CreateDirectory(dir);
				}
				return NormalizePath(dir);
			}
			catch (Exception e) {
				Console.Error.AndExit($"The config value {key} must be a valid path: {e.Message}");
				return null;
			}
		}

		public static string GetAndPrepareRequiredSubDirectory(string key, string subfolder, bool warnEmpty=false) {
			string dir = GetAndPrepareRequiredDirectory(key);
			try {
				string subdir = Path.Combine(dir, subfolder);
				if (Directory.Exists(subdir)) {
					if (Directory.GetFiles(subdir).Length > 0 && warnEmpty) {
						ConfirmOrExit($"Are you sure you would like to extract? The {key} subfolder {subfolder} is non empty, which could mean losing progress. Continue? (yes/no)");
					}
				}
				else {
					Directory.CreateDirectory(subdir);
				}
				return NormalizePath(subdir);
			}
			catch (Exception e) {
				Console.Error.AndExit($"The config value {key} subfolder {subfolder} must be a valid path: {e.Message}");
				return null;
			}
		}

		public static string GetAndPrepareOptionalDirectory(string key) {
			string dir = GetOptionalSetting(key);
			if(dir == null) {
				return null;
			}

			return GetAndPrepareRequiredDirectory(key);
		}

		public static string GetRequiredString(string key) {
			return GetRequiredSetting(key);
		}

		public static string GetOptionalSetting(string key) {
			return InstanceSettings.ContainsKey(key) ? InstanceSettings[key] : null;
		}

		private static string GetRequiredSetting(string key) {
			if(!InstanceSettings.ContainsKey(key)) {
				Console.Error.AndExit($"The config value {key} is required for this operation");
			}
			return InstanceSettings[key];
		}

		public static bool GetBoolean(string key) {
			return InstanceSettings.ContainsKey(key) && InstanceSettings[key].Equals("true");
		}

		public static int GetOptionalNumber(string key, int fallback) {
			string strNumber = GetOptionalSetting(key);
			if (strNumber == null) {
				return fallback;
			}
			if (!int.TryParse(strNumber, out int parsed)) {
				Console.Error.AndExit($"The config value {key} should be a number, got {strNumber}");
			}
			return parsed;
		}

		public static string[] GetList(string key) {
			string setting = InstanceSettings.ContainsKey(key) ? InstanceSettings[key] : "";
			return setting.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
		}

		public static void ConfirmOrExit(string question) {
			Console.WriteLine(question);
			string resp = Console.ReadLine();
			if (resp != null && !resp.Contains("yes")) {
				Console.WriteLine("Quitting...");
				Environment.Exit(0);
			}
		}
	}
}

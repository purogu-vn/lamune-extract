﻿using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;

namespace System {
	public static class Extensions {
		public static void AndExit(this TextWriter writer, string message) {
			writer.WriteLine(message);
			writer.WriteLine();
			Console.ReadLine();
			Environment.Exit(1);
		}

		public static void CopyTo(this Stream inStr, Stream outStr, int length, int bufferSize) {
			byte[] buffer = new byte[bufferSize * 1024];
			int read = 1;
			for(int leftToRead = length; leftToRead > 0 && read > 0; leftToRead -= read) {
				read = inStr.Read(buffer, 0, Math.Min(buffer.Length, leftToRead));
				outStr.Write(buffer, 0, read);
			}
		}

		public static JArray GetArrayProperty(this JObject jObj, string key) {
			return jObj.Property(key).Value as JArray;
		}

		public static string GetStringProperty(this JObject jObj, string key) {
			return jObj.Property(key).Value.ToString();
		}

		public static int GetIntProperty(this JObject jObj, string key) {
			return int.Parse(jObj.GetStringProperty(key));
		}

		public static bool GetBoolProperty(this JObject jObj, string key) {
			return bool.Parse(jObj.GetStringProperty(key));
		}

		public static JObject ReadAndParseJson(this string path) {
			return JObject.Parse(File.ReadAllText(path, Encoding.UTF8));
		}

		public static void SaveJson(this string path, JObject jObj) {
			File.WriteAllText(path, jObj.ToString());
		}
	}
}

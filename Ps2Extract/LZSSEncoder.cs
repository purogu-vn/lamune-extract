﻿using System;
using System.IO;

namespace Ps2Extract {
	internal static class LZSSEncoder {
		private const int LZSS_RING_BUFF = 4096;
		private const int LZSS_LONGEST_MATCH = 18;

		//Credit: https://github.com/RikuKH3/mf_pack/blob/master/LZSS.pas
		internal static void Decode(BinaryReader srcReader, int dstLength, BinaryWriter dstWriter) {
			int r = LZSS_RING_BUFF - LZSS_LONGEST_MATCH;
			byte[] mText = new byte[LZSS_LONGEST_MATCH + LZSS_RING_BUFF - 1];
			uint aFlag = 0;
			while (true) {
				aFlag >>= 1;
				if ((aFlag & 256) == 0) {
					aFlag = (uint) (srcReader.ReadByte() | 0xFF00);
				}
				if ((aFlag & 1) == 1) {
					byte c = srcReader.ReadByte();
					dstWriter.Write(c);
					dstLength--;
					if (dstLength <= 0) {
						return;
					}
					mText[r] = c;
					r++;
					r &= LZSS_RING_BUFF - 1;
				}
				else {
					int i = srcReader.ReadByte();
					int j = srcReader.ReadByte();
					i |= (j & 0xF0) << 4;
					j = (j & 0x0F) + 2;
					for (int k = 0; k <= j; k++) {
						byte c = mText[(i + k) & (LZSS_RING_BUFF - 1)];
						dstWriter.Write(c);
						dstLength--;
						if (dstLength <= 0) {
							return;
						}
						mText[r] = c;
						r++;
						r &= LZSS_RING_BUFF - 1;
					}
				}
			}
		}
	}
}

﻿
using Ps2Extract.Images;
using System;
using System.Collections.Generic;
using System.IO;

namespace Ps2Extract.Steps {
	internal class ThumbsStep : ExtractionStep {
		private const string MF_EXTRACT_SUBSTEP = "mf-extract";
		private const string ENCODE_SUBSTEP = "encode";

		internal ThumbsStep() : base("thumbs", true) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(MF_EXTRACT_SUBSTEP, MfExtractSubstep),
				new ExtractionSubstep(ENCODE_SUBSTEP, EncodeSubstep),
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ISODataStep.NAME);
		}

		private void MfExtractSubstep() {
			string thumbsMfPath = GetFileFromAssetFolder(ISODataStep.NAME, "thumbs.mf");
			string mfExtractPath = GetCleanWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			MFEncoder.Extract(thumbsMfPath, mfExtractPath);
		}

		private void EncodeSubstep() {
			string mfExtractPath = GetWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			string finalPath = GetCleanAssetSubFolder();
			string[] thumbFiles = Directory.GetFiles(mfExtractPath);
			for (int i = 0; i < thumbFiles.Length; i++) {
				string thumbFile = thumbFiles[i];
				// some files extracted have the text "dmy" only
				if (new FileInfo(thumbFile).Length < 8) {
					Console.WriteLine($"{Path.GetFileName(thumbFile)} is empty, skipping");
					continue;
				}
				Console.WriteLine($"Encoding {Path.GetFileNameWithoutExtension(thumbFile)} as image ({i + 1} / {thumbFiles.Length})");
				ImageEncoder.EncodeRawImage(thumbFile, finalPath);
			}
		}
	}
}

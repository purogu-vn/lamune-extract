﻿
using System.Collections.Generic;

namespace Ps2Extract.Steps {
	internal class BackgroundsStep : ExtractionStep {
		internal const string NAME = "backgrounds";

		private const string NAME_SUBSTEP = "name";

		internal BackgroundsStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(NAME_SUBSTEP, NameSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(Ps2ScriptMappingsStep.NAME);
			dependencies.Add(ImagesStep.NAME);
		}

		private void NameSubstep() {
			string backgroundMappingPath = GetFileFromAssetFolder(Ps2ScriptMappingsStep.NAME, "backgrounds.raw.name.json");
			string imagesPath = GetAssetSubFolder(ImagesStep.NAME);
			string finalPath = GetCleanAssetSubFolder();
			ApplyNameMappingsFromFileBasedOnMappings(backgroundMappingPath, imagesPath, finalPath);
		}
	}
}

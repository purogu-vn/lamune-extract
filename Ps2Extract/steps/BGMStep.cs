﻿
using System.Collections.Generic;

namespace Ps2Extract.Steps {
	internal class BGMStep : AudioStep {
		internal const string NAME = "bgm";

		private const string SPLIT_SUBSTEP = "split";
		private const string WAV_SUBSTEP = "wav";
		private const string OGG_SUBSTEP = "ogg";
		private const string NAME_SUBSTEP = "name";
		internal BGMStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(SPLIT_SUBSTEP, SplitSubstep),
				new ExtractionSubstep(WAV_SUBSTEP, WavSubstep),
				new ExtractionSubstep(OGG_SUBSTEP, OggSubstep),
				new ExtractionSubstep(NAME_SUBSTEP, NameSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ISODataStep.NAME);
		}

		private void SplitSubstep() {
			string bgmTablePath = GetFileFromAssetFolder(ISODataStep.NAME, "bgm.tbl");
			string bgmDatPath = GetFileFromAssetFolder(ISODataStep.NAME, "bgm.dat");
			string splitPath = GetCleanWorkingSubFolder(SPLIT_SUBSTEP);
			SplitRawADPCM(bgmTablePath, bgmDatPath, splitPath);
		}

		private void WavSubstep() {
			string splitPath = GetWorkingSubFolder(SPLIT_SUBSTEP);
			string wavPath = GetCleanWorkingSubFolder(WAV_SUBSTEP);
			ConvertADPCMToWavUsingMFAudio(splitPath, wavPath, 44100, 2, 0x8000);
		}

		private void OggSubstep() {
			string wavPath = GetWorkingSubFolder(WAV_SUBSTEP);
			string oggPath = GetCleanWorkingSubFolder(OGG_SUBSTEP);
			ConvertWavToOgg(wavPath, oggPath);
		}

		private void NameSubstep() {
			string oggPath = GetWorkingSubFolder(OGG_SUBSTEP);
			string finalPath = GetCleanAssetSubFolder();
			ApplyNameMappingsFromDataBasedOnFolder(Properties.Resources.track_raw_name, oggPath, finalPath);
		}
	}
}

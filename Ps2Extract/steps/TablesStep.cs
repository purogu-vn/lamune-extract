﻿using System.IO;

namespace Ps2Extract.Steps {
	internal class TablesStep : ExtractionStep {
		internal const string NAME = "tables";

		private const string COPY_SUBSTEP = "copy";

		internal TablesStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(COPY_SUBSTEP, CopySubstep)
			};
		}

		private void CopySubstep() {
			string finalPath = GetCleanAssetSubFolder();
			File.WriteAllText(Path.Combine(finalPath, "cg.tbl.json"), Properties.Resources.cg_tbl);
			File.WriteAllText(Path.Combine(finalPath, "track.tbl.json"), Properties.Resources.track_tbl);
			File.WriteAllText(Path.Combine(finalPath, "scenario.tbl.json"), Properties.Resources.scenario_tbl);
		}
	}
}

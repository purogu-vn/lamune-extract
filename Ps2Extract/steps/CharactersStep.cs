﻿
using System.Collections.Generic;

namespace Ps2Extract.Steps {
	internal class CharactersStep : ExtractionStep {
		internal const string NAME = "characters";

		private const string NAME_SUBSTEP = "name";

		internal CharactersStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(NAME_SUBSTEP, NameSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(Ps2ScriptMappingsStep.NAME);
			dependencies.Add(ImagesStep.NAME);
		}

		private void NameSubstep() {
			string characterMappingPath = GetFileFromAssetFolder(Ps2ScriptMappingsStep.NAME, "characters.raw.name.json");
			string imagesPath = GetAssetSubFolder(ImagesStep.NAME);
			string finalPath = GetCleanAssetSubFolder();
			ApplyNameMappingsFromFileBasedOnMappings(characterMappingPath, imagesPath, finalPath);
		}
	}
}

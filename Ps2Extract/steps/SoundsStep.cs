﻿
using System.Collections.Generic;

namespace Ps2Extract.Steps {
	internal class SoundsStep : AudioStep {
		internal const string NAME = "sounds";

		private const string SPLIT_SUBSTEP = "split";
		private const string WAV_SUBSTEP = "wav";
		private const string NAME_SUBSTEP = "name";
		internal SoundsStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(SPLIT_SUBSTEP, SplitSubstep),
				new ExtractionSubstep(WAV_SUBSTEP, WavSubstep),
				new ExtractionSubstep(NAME_SUBSTEP, NameSubstep),
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ISODataStep.NAME);
			dependencies.Add(Ps2ScriptMappingsStep.NAME);
		}

		private void SplitSubstep() {
			string soundsTablePath = GetFileFromAssetFolder(ISODataStep.NAME, "sounds.tbl");
			string soundsDatPath = GetFileFromAssetFolder(ISODataStep.NAME, "sounds.dat");
			string splitPath = GetCleanWorkingSubFolder(SPLIT_SUBSTEP);
			// vgmstream requires this specific extension for 22050Hz mono adpcm
			SplitRawADPCM(soundsTablePath, soundsDatPath, splitPath, ".vb");
		}

		private void WavSubstep() {
			string splitPath = GetWorkingSubFolder(SPLIT_SUBSTEP);
			string wavPath = GetCleanWorkingSubFolder(WAV_SUBSTEP);
			ConvertADPCMToWavUsingVGMStream(splitPath, wavPath);
		}

		private void NameSubstep() {
			string soundsMappingPath = GetFileFromAssetFolder(Ps2ScriptMappingsStep.NAME, "sounds.raw.name.json");
			string wavPath = GetWorkingSubFolder(WAV_SUBSTEP);
			string finalPath = GetCleanAssetSubFolder();
			ApplyNameMappingsFromFileBasedOnFolder(soundsMappingPath, wavPath, finalPath);
		}
	}
}

﻿
using System.Collections.Generic;

namespace Ps2Extract.Steps {
	internal class Ps2ScriptsStep : ExtractionStep {
		internal const string NAME = "ps2-scripts";

		private const string MF_EXTRACT_SUBSTEP = "mf-extract";
		private const string NAME_SUBSTEP = "name";

		internal Ps2ScriptsStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] { 
				new ExtractionSubstep(MF_EXTRACT_SUBSTEP, MFExtractSubstep),
				new ExtractionSubstep(NAME_SUBSTEP, NameSubstep),
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ISODataStep.NAME);
		}

		private void MFExtractSubstep() {
			string scriptsMfPath = GetFileFromAssetFolder(ISODataStep.NAME, "scripts.mf");
			string mfExtractPath = GetCleanWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			MFEncoder.Extract(scriptsMfPath, mfExtractPath);
		}

		private void NameSubstep() {
			string mfExtractPath = GetWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			string finalPath = GetCleanAssetSubFolder();
			ApplyNameMappingsFromDataBasedOnFolder(Properties.Resources.scenario_raw_name, mfExtractPath, finalPath, ".ps2.txt");
		}
	}
}

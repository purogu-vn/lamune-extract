﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Ps2Extract.Steps {
	internal abstract class ExtractionStep {
		
		private readonly ExtractionSubstep[] substeps;

		internal string Name { get; }
		internal bool Optional { get; }

		internal ExtractionStep(string name, bool optional=false) {
			Name = name;
			Optional = optional;
			this.substeps = InitializeSubsteps();
		}

		internal abstract ExtractionSubstep[] InitializeSubsteps();

		protected virtual void AddDependencies(List<string> dependencies) {
			
		}

		protected string GetWorkingSubFolder(string substep) {
			return Config.GetAndPrepareRequiredSubDirectory(Config.WORKING_FOLDER, Path.Combine(Name, substep));
		}

		protected string GetAssetSubFolder(string name) {
			return Config.GetAndPrepareRequiredSubDirectory(Config.ASSETS_FOLDER, name);
		}

		protected string GetFileFromWorkingFolder(string substep, string name) {
			string workingPath = GetWorkingSubFolder(substep);
			string filePath = Path.Combine(workingPath, name);
			if (!File.Exists(filePath)) {
				Console.Error.AndExit($"Could not read file {name} from {substep} working folder");
			}
			return filePath;
		}

		protected string GetFileFromAssetFolder(string assetName, string name) {
			string assetPath = GetAssetSubFolder(assetName);
			string filePath = Path.Combine(assetPath, name);
			if (!File.Exists(filePath)) {
				Console.Error.AndExit($"Could not read file {name} from {assetName} assets folder");
			}
			return filePath;
		}

		protected string GetCleanWorkingSubFolder(string substep) {
			string workingFolder = GetWorkingSubFolder(substep);
			ClearFolder(workingFolder);
			return workingFolder;
		}

		protected string GetCleanAssetSubFolder() {
			string assetFolder = GetAssetSubFolder(Name);
			ClearFolder(assetFolder);
			return assetFolder;
		}

		protected string GetCleanArcAssetSubFolder(string arcName) {
			string arcFolder = Config.GetAndPrepareRequiredSubDirectory(Config.ARC_FOLDER, arcName);
			string arcAssetFolder = Path.Combine(arcFolder, "assets");
			ClearFolder(arcAssetFolder);
			return arcAssetFolder;
		}

		internal void Run(string startingSubstepName=null) {
			Console.WriteLine($"Beginning extraction step {Name} starting from { startingSubstepName ?? "the beginning" }");
			if (startingSubstepName == null) {
				startingSubstepName = substeps[0].Name;
			}
			bool started = false;
			foreach(var substep in substeps) {
				if(substep.Name.Equals(startingSubstepName)) {
					started = true;
				}
				if(!started) {
					continue;
				}
				try {
					Console.WriteLine("\n\n=========================================================");
					Console.WriteLine($"Running extraction step {Name}[{substep.Name}]");
					Console.WriteLine("=========================================================");
					substep.Run();
				}
				catch (Exception e) {
					Console.Error.AndExit($"Error during execution of step {Name}[{substep.Name}]: {e}");
				}
			}
			if(!started) {
				Console.Error.AndExit($"Unknown starting substep {startingSubstepName} for step {Name}");
			}
			Console.WriteLine($"Finished extraction step {Name}");
		}

		public override string ToString() {
			var dependencies = new List<string>();
			AddDependencies(dependencies);
			string strDependencies = dependencies.Count > 0 ? $"    [depends on: {string.Join(", ", dependencies)}]" : "";
			return $"{Name}:    {string.Join<ExtractionSubstep>(" -> ", substeps)}{strDependencies}";
		}

		private static void ClearFolder(string path) {
			Directory.CreateDirectory(path);
			// CreateDirectory isn't synchronous, wait a little to ensure the folder is there
			while (!Directory.Exists(path)) {
				Thread.Sleep(100);
			}
			foreach (string subpath in Directory.GetFileSystemEntries(path)) {
				if (File.Exists(subpath)) {
					File.Delete(subpath);
				}
				else {
					Directory.Delete(subpath, true);
				}
			}
		}

		private static void HandleConsoleOutput(object sender, DataReceivedEventArgs args) {
			Console.WriteLine(args.Data);
		}

		protected static void RunCmd(string command) {
			Process cmd = new Process();
			cmd.StartInfo.FileName = "cmd.exe";
			cmd.StartInfo.RedirectStandardInput = true;
			cmd.StartInfo.RedirectStandardOutput = true;
			cmd.StartInfo.RedirectStandardError = true;
			cmd.StartInfo.CreateNoWindow = true;
			cmd.StartInfo.UseShellExecute = false;
			cmd.OutputDataReceived += HandleConsoleOutput;
			cmd.ErrorDataReceived += HandleConsoleOutput;
			cmd.Start();

			cmd.StandardInput.WriteLine(command);
			cmd.StandardInput.Flush();
			cmd.StandardInput.Close();

			cmd.BeginOutputReadLine();
			cmd.BeginErrorReadLine();
			cmd.WaitForExit();
		}

		protected static void BatchConvertFiles(string inPath, string outPath, string commandFormat, string ext) {
			string[] files = Directory.GetFiles(inPath);
			for (int i = 0; i < files.Length; i++) {
				string inFile = files[i];
				string outFile = Path.Combine(outPath, Path.ChangeExtension(Path.GetFileName(inFile), ext));
				Console.WriteLine($"Converting {Path.GetFileName(inFile)} to {Path.GetFileName(outFile)} ({i + 1} / {files.Length})");
				RunCmd(string.Format(commandFormat, inFile, outFile));
			}
		}

		protected void ApplyNameMappingsFromFileBasedOnFolder(string mappingPath, string inPath, string outPath, string ext = null) {
			ApplyNameMappingsFromDataBasedOnFolder(File.ReadAllText(mappingPath), inPath, outPath, ext);
		}
		
		// Iterates over all files in inPath, attempting to lookup a mapping for each
		protected void ApplyNameMappingsFromDataBasedOnFolder(string mappingData, string inPath, string outPath, string ext=null) {
			var jMapping = JObject.Parse(mappingData);
			string[] files = Directory.GetFiles(inPath);
			for(int i = 0; i < files.Length; i++) {
				string inFile = files[i];
				if (i == 0) {
					ext = ext ?? Path.GetExtension(inFile);
				}
				string rawName = Path.GetFileNameWithoutExtension(inFile).ToLower();
				if(jMapping.Property(rawName) == null) {
					Console.Error.WriteLine($"No valid mapping for {rawName}. It is not referenced in the game and will not be copied.");
					continue;
				}
				string outName = jMapping.GetStringProperty(rawName) + ext;
				Console.WriteLine($"Applying name mapping: {Path.GetFileName(inFile)} -> {outName} ({i + 1} / {files.Length})");
				File.Copy(inFile, Path.Combine(outPath, outName));
			}
		}

		// Iterates over all mapping values ensuring each has a valid source file
		protected void ApplyNameMappingsFromFileBasedOnMappings(string mappingPath, string inPath, string outPath, string ext = null) {
			ext = ext ?? Path.GetExtension(Directory.GetFiles(inPath)[0]);
			var jMapping = JObject.Parse(File.ReadAllText(mappingPath));
			int i = 0;
			foreach(var jProperty in jMapping.Properties()) {
				string rawName = jProperty.Name;
				string inFile = Path.Combine(inPath, rawName + ext);
				if(!File.Exists(inFile)) {
					Console.Error.AndExit($"No unnamed source file found for {rawName} which maps to {jMapping.GetStringProperty(rawName)}");
				}
				string outName = jMapping.GetStringProperty(rawName) + ext;
				Console.WriteLine($"Applying name mapping: {Path.GetFileName(inFile)} -> {outName} ({i + 1} / {jMapping.Count})");
				File.Copy(inFile, Path.Combine(outPath, outName));
				i++;
			}
		}
	}
}

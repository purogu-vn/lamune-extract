﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ps2Extract.Steps {
	internal class Ps2ScriptMappingsStep : ExtractionStep {
		internal const string NAME = "ps2-script-mappings";
		private const string EXTRACT_MAPPINGS_SUBSTEP = "extract-mappings";

		internal Ps2ScriptMappingsStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(EXTRACT_MAPPINGS_SUBSTEP, ExtractMappingsSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(Ps2ScriptsStep.NAME);
		}

		private void ExtractMappingsSubstep() {
			string ps2ScriptsPath = GetAssetSubFolder(Ps2ScriptsStep.NAME);
			string finalPath = GetCleanAssetSubFolder();
			ExtractNameMapping("voice", ps2ScriptsPath, finalPath, BeginsWithCmd("@play_voice"), ParseSecondArg);
			ExtractNameMapping("backgrounds", ps2ScriptsPath, finalPath, BeginsWithCmd("@show_bg"), ParseBetweenParens);
			ExtractNameMapping("characters", ps2ScriptsPath, finalPath, BeginsWithCmd("@show_chara"), ParseBetweenParens);
			ExtractNameMapping("sounds", ps2ScriptsPath, finalPath, BeginsWithCmd("@play_sound"), ParseSecondArg);
		}

		private static void ExtractNameMapping(string nameType, string ps2ScriptsPath, string mappingsPath, Predicate<string> linePred, Func<string, string> nameParser) {
			Console.WriteLine($"Beginning name mapping extraction for {nameType}");
			IDictionary<string, string> nameMap = new SortedDictionary<string, string>();
			foreach (string scriptFile in Directory.GetFiles(ps2ScriptsPath)) {
				foreach (string line in File.ReadAllLines(scriptFile)) {
					if (linePred(line)) {
						string name = nameParser(line);
						string sNumber = line.Substring(line.LastIndexOf(',') + 1);
						int numLength = 0;
						while (numLength < sNumber.Length && char.IsDigit(sNumber[numLength])) {
							numLength++;
						}
						sNumber = sNumber.Substring(0, numLength);
						if (sNumber.Length <= 0) {
							continue;
						}
						int number = int.Parse(sNumber);
						string raw = number.ToString("x8");
						if (nameMap.ContainsKey(raw) && nameMap[raw] != name) {
							Console.Error.AndExit($"Two files map to the name name for id {IdForInt(number)}: First={nameMap[raw]} Second={name} in {Path.GetFileName(scriptFile)}");
						}
						else {
							nameMap[raw] = name;
						}
					}
				}
			}
			File.WriteAllText(Path.Combine(mappingsPath, nameType + ".raw.name.json"), JsonConvert.SerializeObject(nameMap));
		}

		private static string IdForInt(int i) {
			return $"{i}({i:X8})";
		}

		private static Predicate<string> BeginsWithCmd(params string[] cmds) {
			return line => cmds.Any(cmd => line.StartsWith(cmd));
		}

		private static string ParseBetweenParens(string line) {
			string name = line.Split('(', ')')[1];
			if (name.Contains('"')) {
				name = name.Split('"')[1];
			}
			name = name.Trim().ToLower();
			return name;
		}

		private static string ParseSecondArg(string line) {
			return ParseBetweenParens(line).Split(',')[1];
		}
	}
}

﻿
using System.Collections.Generic;
using System.IO;

namespace Ps2Extract.Steps {
	internal class VoiceStep : AudioStep {
		internal const string NAME = "voice";

		private const string SPLIT_SUBSTEP = "split";
		private const string WAV_SUBSTEP = "wav";
		private const string OGG_SUBSTEP = "ogg";
		private const string NAME_SUBSTEP = "name";
		internal VoiceStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(SPLIT_SUBSTEP, SplitSubstep),
				new ExtractionSubstep(WAV_SUBSTEP, WavSubstep),
				new ExtractionSubstep(OGG_SUBSTEP, OggSubstep),
				new ExtractionSubstep(NAME_SUBSTEP, NameSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ISODataStep.NAME);
			dependencies.Add(Ps2ScriptMappingsStep.NAME);
		}

		private void SplitSubstep() {
			string voiceTablePath = GetFileFromAssetFolder(ISODataStep.NAME, "voice.tbl");
			string voiceDatPath = GetFileFromAssetFolder(ISODataStep.NAME, "voice.dat");
			string splitPath = GetCleanWorkingSubFolder(SPLIT_SUBSTEP);
			// vgmstream (with my patch) requires this specific extension for 44100Hz mono adpcm
			SplitRawADPCM(voiceTablePath, voiceDatPath, splitPath, ".xag");
		}

		private void WavSubstep() {
			string splitPath = GetWorkingSubFolder(SPLIT_SUBSTEP);
			string wavPath = GetCleanWorkingSubFolder(WAV_SUBSTEP);
			ConvertADPCMToWavUsingVGMStream(splitPath, wavPath);
		}

		private void OggSubstep() {
			string wavPath = GetWorkingSubFolder(WAV_SUBSTEP);
			string oggPath = GetCleanWorkingSubFolder(OGG_SUBSTEP);
			ConvertWavToOgg(wavPath, oggPath);
		}

		private void NameSubstep() {
			string voiceMappingPath = GetFileFromAssetFolder(Ps2ScriptMappingsStep.NAME, "voice.raw.name.json");
			string oggPath = GetWorkingSubFolder(OGG_SUBSTEP);
			string finalPath = GetCleanAssetSubFolder();
			ApplyNameMappingsFromFileBasedOnFolder(voiceMappingPath, oggPath, finalPath);
		}
	}
}

﻿using Ps2Extract.Images;
using System.Collections.Generic;

namespace Ps2Extract.Steps {
	internal class SysgraphStep : ExtractionStep {
		private const string MF_EXTRACT_SUBSTEP = "mf-extract";
		private const string ENCODE_SUBSTEP = "encode";

		internal SysgraphStep() : base("sysgraph", true) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(MF_EXTRACT_SUBSTEP, MfExtractSubstep),
				new ExtractionSubstep(ENCODE_SUBSTEP, EncodeSubstep),
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ISODataStep.NAME);
		}

		private void MfExtractSubstep() {
			string sysgraphMfPath = GetFileFromAssetFolder(ISODataStep.NAME, "sysgraph.mf");
			string mfExtractPath = GetCleanWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			MFEncoder.RecursiveExtract(sysgraphMfPath, mfExtractPath);
		}

		private void EncodeSubstep() {
			string mfExtractPath = GetWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			string encodePath = GetCleanWorkingSubFolder(ENCODE_SUBSTEP);
			ImageEncoder.RecursiveEncode(mfExtractPath, encodePath);
		}
	}
}

﻿
using System.Collections.Generic;
using System.IO;

namespace Ps2Extract.Steps {
	internal class VideoStep : ExtractionStep {
		private const string EXTRACT_PSS_STEP = "extract-pss";
		private const string COMBINE_STEP = "combine";

		private const string PSS_PLEX_COMMAND_FORMAT = @"""{0}"" D /N ""{1}"" ""{2}.mv2"" ""{2}.wav""";
		// Without 12M, looks fine out of game, but the game struggles to keep up with a bitrate so high
		private const string FFMPEG_COMMAND_FORMAT = @"""{0}"" -i ""{1}"" -i ""{2}"" -c:v mpeg1video -qscale:v 0 -r 29.97 -maxrate 12M -c:a mp2 ""{3}""";
		internal VideoStep() : base("video") { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(EXTRACT_PSS_STEP, ExtractPSSSubstep),
				new ExtractionSubstep(COMBINE_STEP, CombineSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ISODataStep.NAME);
		}

		private void ExtractPSSSubstep() {
			string pssPlexExe = Config.GetRequiredFile(Config.PSS_PLEX_PATH);
			string isoAssetPath = GetAssetSubFolder(ISODataStep.NAME);
			string splitStreamsPath = GetCleanWorkingSubFolder(EXTRACT_PSS_STEP);
			foreach (string file in Directory.GetFiles(isoAssetPath, "*.pss")) {
				RunCmd(string.Format(PSS_PLEX_COMMAND_FORMAT, pssPlexExe, file, Path.Combine(splitStreamsPath, Path.GetFileNameWithoutExtension(file))));
			}
		}

		private void CombineSubstep() {
			string ffmpegExe = Config.GetRequiredFile(Config.FFMPEG_PATH);
			string splitStreamsPath = GetWorkingSubFolder(EXTRACT_PSS_STEP);
			string finalPath = GetCleanAssetSubFolder();
			foreach (string file in Directory.GetFiles(splitStreamsPath, "*.mv2")) {
				string outFilePath = Path.Combine(finalPath, Path.GetFileNameWithoutExtension(file) + ".mpg");
				RunCmd(string.Format(FFMPEG_COMMAND_FORMAT, ffmpegExe, file, Path.ChangeExtension(file, ".wav"), outFilePath));
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ps2Extract.Steps {
	internal class ScriptsStep : ExtractionStep {
		internal const string NAME = "scripts";

		private const string EXTRACT_SCRIPT_SUBSTEP = "extract-script";
		private const string CONVERT_SUBSTEP = "convert";

		private const string CONVERT_SCRIPT_NAME = "ps2_to_asb.py";
		private const string CONVERT_COMMAND_FORMAT = @"""{0}"" ""{1}"" ""{2}"" ""{3}"" ""{4}"" ""{4}""";

		internal ScriptsStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(EXTRACT_SCRIPT_SUBSTEP, ExtractScriptSubstep),
				new ExtractionSubstep(CONVERT_SUBSTEP, ConvertSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(Ps2ScriptsStep.NAME);
			dependencies.Add(TablesStep.NAME);
		}

		private void ExtractScriptSubstep() {
			string extractScriptPath = GetCleanWorkingSubFolder(EXTRACT_SCRIPT_SUBSTEP);
			File.WriteAllText(Path.Combine(extractScriptPath, CONVERT_SCRIPT_NAME), Properties.Resources.ps2_to_asb);
		}

		private void ConvertSubstep() {
			string pyExe = Config.GetRequiredString(Config.PYTHON3_PATH);
			string convertScriptPath = GetFileFromWorkingFolder(EXTRACT_SCRIPT_SUBSTEP, CONVERT_SCRIPT_NAME);
			string ps2ScriptsPath = GetAssetSubFolder(Ps2ScriptsStep.NAME);
			string tablesPath = GetAssetSubFolder(TablesStep.NAME);
			string finalPath = GetCleanAssetSubFolder();
			RunCmd(string.Format(CONVERT_COMMAND_FORMAT, pyExe, convertScriptPath, ps2ScriptsPath, tablesPath, finalPath));
		}
	}
}

﻿using ExtractCommon;
using System;
using System.Collections.Generic;
using System.IO;

namespace Ps2Extract.Steps {
	internal class RawTablesStep : ExtractionStep {
		private const string EXE_EXTRACT_SUBSTEP = "exe-extract";
		internal RawTablesStep() : base("raw-tables", true) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] { 
				new ExtractionSubstep(EXE_EXTRACT_SUBSTEP, ExeExtractSubstep)
			};
		}

		private void ExeExtractSubstep() {
			string isoExePath = Config.GetRequiredFile(Config.ISO_EXE_PATH);
			string finalPath = GetCleanAssetSubFolder();
			using(var src = new BufferedFileReader(isoExePath)) {
				ExtractTableMapping(src, 0x12D1C8, "track", finalPath);
				ExtractTableMapping(src, 0x12BF50, "scenario", finalPath);
				ExtractTableMapping(src, 0x12D3F0, "cg", finalPath);
			}
		}

		private static void ExtractTableMapping(BufferedFileReader src, int start, string tableName, string outPath) {
			src.Stream.Position = start;
			IList<string> decodedOutLines = new List<string>();
			bool done = false;
			while (!done) {
				int id = src.Reader.ReadInt32();
				int rawId = src.Reader.ReadInt32();
				if (id == -1 && rawId == -1) {
					done = true;
				}
				else {
					decodedOutLines.Add($"{rawId:X8}={id}");
				}
			}
			Console.WriteLine($"Extracted {tableName} raw table to file");
			File.WriteAllLines(Path.Combine(outPath, tableName + ".raw.id.map"), decodedOutLines);
		}
	}
}

﻿
using Ps2Extract.Images;
using System;
using System.Collections.Generic;
using System.IO;

namespace Ps2Extract.Steps {
	internal class ImagesStep : ExtractionStep {
		internal const string NAME = "images";

		private const string MF_EXTRACT_SUBSTEP = "mf-extract";
		private const string MF_SPLIT_SUBSTEP = "mf-split";
		private const string ENCODE_SUBSTEP = "encode";

		internal ImagesStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(MF_EXTRACT_SUBSTEP, MfExtractSubstep),
				new ExtractionSubstep(MF_SPLIT_SUBSTEP, MfSplitSubstep),
				new ExtractionSubstep(ENCODE_SUBSTEP, EncodeSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ISODataStep.NAME);
		}

		private void MfExtractSubstep() {
			string imagesMfPath = GetFileFromAssetFolder(ISODataStep.NAME, "images.mf");
			string mfExtractPath = GetCleanWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			MFEncoder.Extract(imagesMfPath, mfExtractPath);
		}

		private void MfSplitSubstep() {
			string mfExtractPath = GetWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			string mfSplitPath = GetCleanWorkingSubFolder(MF_SPLIT_SUBSTEP);
			foreach(string mfImage in Directory.GetFiles(mfExtractPath)) {
				if(new FileInfo(mfImage).Length == 0) {
					Console.WriteLine($"{Path.GetFileName(mfImage)} is empty, skipping");
					continue;
				}
				string imageName = Path.GetFileNameWithoutExtension(mfImage);
				string splitImageFolder = Path.Combine(mfSplitPath, imageName);
				Directory.CreateDirectory(splitImageFolder);
				MFEncoder.Extract(mfImage, splitImageFolder);
			}
		}
		private void EncodeSubstep() {
			string mfSplitPath = GetWorkingSubFolder(MF_SPLIT_SUBSTEP);
			string finalPath = GetCleanAssetSubFolder();
			string[] imageFolders = Directory.GetDirectories(mfSplitPath);
			for(int i = 0; i < imageFolders.Length; i++) {
				string imageFolder = imageFolders[i];
				Console.WriteLine($"Encoding {Path.GetFileNameWithoutExtension(imageFolder)} as image ({i + 1} / {imageFolders.Length})");
				ImageEncoder.EncodeImageFromImageFolder(imageFolder, finalPath);
			}
		}
	}
}

﻿using ExtractCommon;
using System;
using System.Collections.Generic;
using System.IO;

namespace Ps2Extract.Steps {
	internal abstract class AudioStep : ExtractionStep {
		private const string MFAUDIO_COMMAND_FORMAT_BEGIN = @"""{0}"" /IF{1} /IC{2}";
		private const string MFAUDIO_COMMAND_FORMAT_END = @" /OTWAVU /OF44100 /OC1 ""{0}"" ""{1}""";

		private const string OGGENC2_COMMAND_FORMAT_BEGIN = @"""{0}"" -q 10 ";
		private const string OGGENC2_COMMAND_FORMAT_END = @"""{0}"" -o ""{1}""";

		private const string VGMSTREAM_COMMAND_FORMAT_BEGIN = @"""{0}""";
		private const string VGMSTREAM_COMMAND_FORMAT_END = @" -o {1} ""{0}""";

		internal AudioStep(string name) : base(name) { }

		protected void SplitRawADPCM(string tablePath, string rawPath, string outPath, string ext = ".dat") {
			IList<ADPCMHeader> headers = new List<ADPCMHeader>();
			using (var src = new BufferedFileReader(tablePath)) {
				while (src.Stream.Position != src.Stream.Length) {
					headers.Add(new ADPCMHeader(src.Reader));
				}
				Console.WriteLine($"Read {headers.Count} ADPCM headers from {Name} table");
			}

			using (var src = new BufferedFileReader(rawPath)) {
				for (int i = 0; i < headers.Count; i++) {
					src.Stream.Position = headers[i].Offset;
					byte[] raw = src.Reader.ReadBytes(headers[i].Length);
					File.WriteAllBytes(Path.Combine(outPath, i.ToString("X8") + ext), raw);

					Console.WriteLine($"Finished spliting {Name} track from raw ADPCM ({i + 1} out of {headers.Count})");
				}
			}
		}

		protected static void ConvertWavToOgg(string wavPath, string oggPath) {
			string oggenc2Path = Config.GetRequiredFile(Config.OGGENC2_PATH);
			string formatString = string.Format(OGGENC2_COMMAND_FORMAT_BEGIN, oggenc2Path) + OGGENC2_COMMAND_FORMAT_END;
			BatchConvertFiles(wavPath, oggPath, formatString, ".ogg");
		}
		protected static void ConvertADPCMToWavUsingMFAudio(string rawPath, string wavPath, int sampleRate, int channels, int interleave = 0) {
			string mfAudioExe = Config.GetRequiredFile(Config.MFAUDIO_PATH);
			string inputFlags = string.Format(MFAUDIO_COMMAND_FORMAT_BEGIN, mfAudioExe, sampleRate, channels);
			if (channels > 1) {
				inputFlags += " /II" + interleave.ToString("X");
			}
			string commandFormat = inputFlags + MFAUDIO_COMMAND_FORMAT_END;
			ConvertADPCMToWav(rawPath, wavPath, commandFormat);
		}

		protected static void ConvertADPCMToWavUsingVGMStream(string rawPath, string wavPath) {
			string vgmstreamExe = Config.GetRequiredFile(Config.VGMSTREAM_PATCHED_PATH);
			string commandFormat = string.Format(VGMSTREAM_COMMAND_FORMAT_BEGIN, vgmstreamExe) + VGMSTREAM_COMMAND_FORMAT_END;
			ConvertADPCMToWav(rawPath, wavPath, commandFormat);
		}

		private static void ConvertADPCMToWav(string rawPath, string wavPath, string commandFormat) {
			BatchConvertFiles(rawPath, wavPath, commandFormat, ".wav");
		}
	}
}

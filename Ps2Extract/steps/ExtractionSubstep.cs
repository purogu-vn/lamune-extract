﻿using System;

namespace Ps2Extract.Steps {
	internal class ExtractionSubstep {
		internal string Name { get; }
		internal Action Run { get; }

		internal ExtractionSubstep(string name, Action run) {
			Name = name;
			Run = run;
		}

		public override string ToString() {
			return Name;
		}
	}
}

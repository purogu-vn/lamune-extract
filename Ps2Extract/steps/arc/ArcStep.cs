﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace Ps2Extract.Steps.Arc {
	internal abstract class ArcStep : ExtractionStep {
		protected const string DEPENDENCIES_FILE = "_dependencies.json";
		private const string PREPARE_SUBSTEP = "prepare";

		private readonly string arcName;
		private readonly string[] assetTypes;

		internal ArcStep(string arcName, string[] assetTypes) : base(arcName + "-arc") {
			this.arcName = arcName;
			this.assetTypes = assetTypes;
		}

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] {
				new ExtractionSubstep(PREPARE_SUBSTEP, PrepareSubstep)
			};
		}

		protected override void AddDependencies(List<string> dependencies) {
			dependencies.Add(ScriptsStep.NAME);
			dependencies.AddRange(assetTypes);
		}

		protected virtual bool CopyAsset(string assetType, string assetName, string assetFolder, string finalPath) {
			string assetPath = Path.Combine(assetFolder, assetName);
			if (!File.Exists(assetPath)) {
				Console.Error.AndExit($"Could not located required asset {assetName} of type {assetType}");
			}
			File.Copy(assetPath, Path.Combine(finalPath, assetName), true);
			return true;
		}

		private void PrepareSubstep() {
			string dependenciesPath = GetFileFromAssetFolder(ScriptsStep.NAME, DEPENDENCIES_FILE);
			string finalPath = GetCleanArcAssetSubFolder(arcName);
			Console.WriteLine($"Preparing {arcName} ARC...");
			
			JObject jDependencies = dependenciesPath.ReadAndParseJson();
			foreach (string assetType in assetTypes) {
				string assetFolder = GetAssetSubFolder(assetType);
				JArray jAssets = jDependencies.GetArrayProperty(assetType);
				Console.WriteLine($"Copying {jAssets.Count} {assetType} assets...");
				int validAssetsCount = 0;
				foreach (var jAsset in jAssets) {
					string assetName = jAsset.ToString();
					CopyAsset(assetType, assetName, assetFolder, finalPath);
					validAssetsCount++;
				}
				Console.WriteLine($"Finished copying ({jAssets.Count} / {validAssetsCount}) {assetType} assets...");
			}
		}
	}
}

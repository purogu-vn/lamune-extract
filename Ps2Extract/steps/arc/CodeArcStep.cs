﻿using System;

namespace Ps2Extract.Steps.Arc {
	internal class CodeArcStep : ArcStep {

		private static readonly string[] CODE_ASSETS = { ScriptsStep.NAME, TablesStep.NAME };
		internal CodeArcStep() : base("code", CODE_ASSETS) { }

		protected override bool CopyAsset(string assetType, string assetName, string assetFolder, string finalPath) {
			if (assetName.Equals(DEPENDENCIES_FILE)) {
				Console.WriteLine($"Skipping {assetName}");
				return false;
			}
			return base.CopyAsset(assetType, assetName, assetFolder, finalPath);
		}
	}
}

﻿namespace Ps2Extract.Steps.Arc {
	internal class AudioArcStep : ArcStep {

		private static readonly string[] AUDIO_ASSETS = { VoiceStep.NAME, SoundsStep.NAME, BGMStep.NAME };
		internal AudioArcStep() : base("audio", AUDIO_ASSETS) { }
	}
}

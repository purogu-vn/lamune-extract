﻿namespace Ps2Extract.Steps.Arc {
	internal class VisualArcStep : ArcStep {

		private static readonly string[] VISUAL_ASSETS = { CharactersStep.NAME, BackgroundsStep.NAME };
		internal VisualArcStep() : base("visual", VISUAL_ASSETS) { }
	}
}

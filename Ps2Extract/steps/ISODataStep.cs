﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Ps2Extract.Steps {
	internal class ISODataStep : ExtractionStep {
		internal const string NAME = "iso-data";

		private const string MF_EXTRACT_SUBSTEP = "mf-extract";
		private const string NAME_SUBSTEP = "name";

		private static readonly IDictionary<string, string> NUMBER_TO_NAME = new Dictionary<string, string> {
			["00000000"] = "UNK1.dat",
			["00000001"] = "click.dat",
			["00000002"] = "sounds.tbl",
			["00000003"] = "bgm.tbl",
			["00000004"] = "voice.tbl",
			["00000005"] = "GLAY01.pss",
			["00000006"] = "MITU-TR.pss",
			["00000007"] = "TYTLE01_2005.pss",
			["00000008"] = "UFO.pss",
			["00000009"] = "LAMU_OP.pss",
			["0000000A"] = "voice.dat",
			["0000000B"] = "UNK2.mf",
			["0000000C"] = "scripts.mf",
			["0000000D"] = "thumbs.mf",
			["0000000E"] = "sysgraph.mf",
			["0000000F"] = "bgm.dat",
			["00000010"] = "sounds.dat",
			["00000011"] = "images.mf",
		};

		internal ISODataStep() : base(NAME) { }

		internal override ExtractionSubstep[] InitializeSubsteps() {
			return new ExtractionSubstep[] { 
				new ExtractionSubstep(MF_EXTRACT_SUBSTEP, MFExtractSubstep),
				new ExtractionSubstep(NAME_SUBSTEP, NameSubstep)
			};
		}

		private void MFExtractSubstep() {
			string isoDataPath = Config.GetRequiredFile(Config.ISO_DATA_PATH);
			string mfSplitPath = GetCleanWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			MFEncoder.Extract(isoDataPath, mfSplitPath);
		}

		private void NameSubstep() {
			string mfSplitPath = GetWorkingSubFolder(MF_EXTRACT_SUBSTEP);
			string finalPath = GetCleanAssetSubFolder();
			foreach (string datFile in Directory.EnumerateFiles(mfSplitPath)) {
				string num = Path.GetFileNameWithoutExtension(datFile);
				string name = NUMBER_TO_NAME[num];
				Console.WriteLine($"Naming {num} as {name}");
				File.Copy(datFile, Path.Combine(finalPath, name), true);
			}
		}
	}
}

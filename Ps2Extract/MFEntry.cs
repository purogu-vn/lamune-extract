﻿using System;
using System.IO;
using ExtractCommon;

namespace Ps2Extract {
	internal class MFEntry {

		private const int COPY_BUFFER_MB = 128;
		internal string FileName { get; }

		internal int CompressedLength { get;  }
		internal int Offset { get; }
		internal bool Compressed { get; }
		internal int Length { get; }

		internal MFEntry(string fileName, BinaryReader br) {
			FileName = fileName;
			CompressedLength = br.ReadInt32();
			Offset = br.ReadInt32();
			Compressed = br.ReadInt32() != 0;
			Length = br.ReadInt32();
		}

		internal void ExtractToFile(BufferedFileReader src, string outDir) {
			using (FileStream fileOut = File.Create(Path.Combine(outDir, FileName + ".dat"))) {
				src.Stream.Position = Offset;
				if (Compressed) {
					// Skip over UFFA header
					src.Stream.Position += 0x10;
					Console.WriteLine($"Decompressing MF Entry: {FileName}");
					var decodedEntryStr = new MemoryStream();
					using (var bw = new BinaryWriter(decodedEntryStr)) {
						LZSSEncoder.Decode(src.Reader, Length, bw);
						decodedEntryStr.Position = 0;
						decodedEntryStr.CopyTo(fileOut);
					}
				}
				else {
					src.Stream.CopyTo(fileOut, Length, COPY_BUFFER_MB);
				}
			}
		}
	}
}

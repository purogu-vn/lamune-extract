﻿

using System;

namespace Ps2Extract.Images {
	internal class ImageChunkType14 : ImageChunk {
		// Max desired alpha 255 / max seen alpha 128
		private const double ALPHA_CONVERSION_RATIO = 255.0 / 128.0;
		internal ImageChunkType14(int width, int height) : base(width, height) { }

		protected override int PaletteSize => 16;
		protected override int DataSize => Width * Height / 2;

		protected override byte[] GetBitmapData() {
			byte[] bitmapData = new byte[8 * Data.Length];
			for (int i = 0; i < Data.Length; i++) {
				byte twoColorIds = Data[i];
				byte colorId1 = (byte)(twoColorIds & 0x0F);
				byte[] rgba1 = Palette[colorId1];
				bitmapData[8 * i] = rgba1[2];
				bitmapData[8 * i + 1] = rgba1[1];
				bitmapData[8 * i + 2] = rgba1[0];
				bitmapData[8 * i + 3] = GetAlpha(rgba1[3]);

				byte colorId2 = (byte)(twoColorIds >> 4);
				byte[] rgba2 = Palette[colorId2];
				bitmapData[8 * i + 4] = rgba2[2];
				bitmapData[8 * i + 5] = rgba2[1];
				bitmapData[8 * i + 6] = rgba2[0];
				bitmapData[8 * i + 7] = GetAlpha(rgba2[3]);
			}
			return bitmapData;
		}

		private byte GetAlpha(byte rawAlpha) {
			return (byte)Math.Ceiling(rawAlpha * ALPHA_CONVERSION_RATIO);
		}
	}
}

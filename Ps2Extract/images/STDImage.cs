﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Ps2Extract.Images {
	internal class STDImage : Image {

		private readonly Size size;
		private readonly Size contentSize;
		private readonly Point drawStart;
		internal STDImage(string name, string dataPath, Size size, Size contentSize, Point drawStart) : base(name, dataPath) {
			this.size = size;
			this.contentSize = contentSize;
			this.drawStart = drawStart;
		}
		internal override void Decode(string outPath) {
			string imageFile = Name + ".png";
			Console.WriteLine($"Encoding STD image {Name} ({size.Width}x{size.Height}) ({contentSize.Width}x{contentSize.Height}) ({drawStart.X},{drawStart.Y})");
			ImageChunk[] chunks = ReadMultiChunkFile();

			using (Bitmap data = new Bitmap(contentSize.Width, contentSize.Height, PixelFormat.Format32bppArgb)) {
				using (Graphics gData = Graphics.FromImage(data)) {
					using (Bitmap mask = chunks[0].ToBitmap()) {
						int x = 0, y = 0;
						for (int i = 1; i < chunks.Length; i++) {
							using (Bitmap bit = chunks[i].ToBitmap()) {
								if (x >= contentSize.Width) {
									x = 0;
									y += bit.Height;
								}
								gData.DrawImage(bit, x, y);
								x += bit.Width;
							}
						}
						using (Bitmap combined = AlphaMaskImage(data, mask)) {
							using (Bitmap final = new Bitmap(size.Width, size.Height, PixelFormat.Format32bppArgb)) {
								using (Graphics gFinal = Graphics.FromImage(final)) {
									gFinal.DrawImage(combined, drawStart.X, drawStart.Y);
								}
								ScaleAndSave(final, Path.Combine(outPath, imageFile), ImageFormat.Png);
							}
						}
					}
				}
			}
		}

		private static Bitmap AlphaMaskImage(Bitmap data, Bitmap mask) {
			Bitmap combined = new Bitmap(data.Width, data.Height, PixelFormat.Format32bppArgb);
			Rectangle rect = new Rectangle(0, 0, data.Width, data.Height);
			BitmapData bitsMask = mask.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
			BitmapData bitsData = data.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
			BitmapData bitsCombined = combined.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
			unsafe {
				for (int y = 0; y < data.Height; y++) {
					byte* ptrMask = (byte*)bitsMask.Scan0 + y * bitsMask.Stride;
					byte* ptrData = (byte*)bitsData.Scan0 + y * bitsData.Stride;
					byte* ptrCombined = (byte*)bitsCombined.Scan0 + y * bitsCombined.Stride;
					for (int x = 0; x < data.Width; x++) {
						ptrCombined[4 * x] = ptrData[4 * x];            // blue
						ptrCombined[4 * x + 1] = ptrData[4 * x + 1];    // green
						ptrCombined[4 * x + 2] = ptrData[4 * x + 2];    // red
						ptrCombined[4 * x + 3] = ptrMask[4 * x + 3];    // alpha
					}
				}
			}
			mask.UnlockBits(bitsMask);
			data.UnlockBits(bitsData);
			combined.UnlockBits(bitsCombined);
			return combined;
		}
	}
}

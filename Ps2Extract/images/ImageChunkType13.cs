﻿
using System.IO;

namespace Ps2Extract.Images {
	internal class ImageChunkType13 : ImageChunk {
		internal ImageChunkType13(int width, int height) : base(width, height) { }

		protected override int PaletteSize => 256;
		protected override int DataSize => Width * Height;

		internal override void ReadPalette(BinaryReader br) {
			base.ReadPalette(br);
			//swizzle: SortClut https://github.com/Xeeynamo/OpenKh/blob/master/OpenKh.Imaging/Tm2.cs
			for (int i = 0; i < Palette.Length; i += 32) {
				for (int j = 0; j < 8; j++) {
					int pos1 = i + 8 + j;
					int pos2 = i + 16 + j;
					byte[] temp = Palette[pos1];
					Palette[pos1] = Palette[pos2];
					Palette[pos2] = temp;
				}
			}
		}

		protected override byte[] GetBitmapData() {
			byte[] bitmapData = new byte[4 * Data.Length];
			for (int i = 0; i < Data.Length; i++) {
				byte colorId = Data[i];
				byte[] rgba = Palette[colorId];
				bitmapData[4 * i] = rgba[2];
				bitmapData[4 * i + 1] = rgba[1];
				bitmapData[4 * i + 2] = rgba[0];
				bitmapData[4 * i + 3] = GetAlpha(rgba[3]);
			}
			return bitmapData;
		}

		/// Alpha is always 0x80 which is no transparency or 0
		private byte GetAlpha(byte rawAlpha) {
			return 255;
		}
	}
}

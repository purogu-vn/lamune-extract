﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace Ps2Extract.Images {
	internal abstract class ImageChunk {
		internal int Width { get; }
		internal int Height { get; }
		// lookup from colorId to rgba
		protected byte[][] Palette { get; private set; }
		protected byte[] Data { get; private set; }

		protected abstract int PaletteSize { get; }
		protected abstract int DataSize { get; }

		internal ImageChunk(int width, int height) {
			Width = width;
			Height = height;
		}

		protected abstract byte[] GetBitmapData();

		internal virtual void ReadPalette(BinaryReader br) {
			Palette = new byte[PaletteSize][];
			for (int i = 0; i < Palette.Length; i++) {
				Palette[i] = br.ReadBytes(4);
			}
		}

		internal void ReadData(BinaryReader br) {
			Data = br.ReadBytes(DataSize);
		}

		internal Bitmap ToBitmap() {
			byte[] bitmapData = GetBitmapData();
			Bitmap bit = new Bitmap(Width, Height, PixelFormat.Format32bppArgb);
			Rectangle rect = new Rectangle(0, 0, Width, Height);
			BitmapData bmpData = bit.LockBits(rect, ImageLockMode.ReadWrite, bit.PixelFormat);
			Marshal.Copy(bitmapData, 0, bmpData.Scan0, bitmapData.Length);
			bit.UnlockBits(bmpData);
			return bit;
		}
	}
}

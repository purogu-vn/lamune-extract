﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Ps2Extract.Images {
	internal class RawImage : Image {
		internal RawImage(string name, string dataPath) : base(name, dataPath) { }
		internal override void Decode(string outPath) {
			string imageFile = Name + ".png";
			ImageChunk chunk = ReadChunkHeader();
			Console.WriteLine($"Encoding RAW image {Name} ({chunk.Width}x{chunk.Height})");
			chunk.ReadPalette(Src.Reader);
			chunk.ReadData(Src.Reader);
			using (Bitmap bit = chunk.ToBitmap()) {
				bit.Save(Path.Combine(outPath, imageFile), ImageFormat.Png);
			}
		}
	}
}

﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Ps2Extract.Images {
	internal static class ImageEncoder {
		private const string MF_IMAGE_HEADER_FILE_NAME = "00000000";
		private const string MF_IMAGE_DATA_FILE_NAME = "00000001";
		private const string IMG_IMAGE_HEADER = "IMG";
		private const string STD_IMAGE_HEADER = "STD";

		internal static void EncodeImageFromImageFolder(string imageFolder, string outPath) {
			string error = TryEncodeImageFromImageFolder(imageFolder, outPath);
			if(error != null) {
				Console.Error.AndExit(error);
			}
		}

		internal static void EncodeRawImage(string inPath, string outPath) {
			string error = TryEncodeRawImage(inPath, outPath);
			if (error != null) {
				Console.Error.AndExit(error);
			}
		}

		internal static void EncodeRawImageArray(string inPath, string outPath) {
			string error = TryEncodeRawImageArray(inPath, outPath);
			if (error != null) {
				Console.Error.AndExit(error);
			}
		}

		internal static void RecursiveEncode(string inPath, string outPath) {
			foreach(string file in Directory.GetFiles(inPath)) {
				if(TryEncodeRawImageArray(file, outPath) != null && TryEncodeRawImage(file, outPath) != null) {
					Console.WriteLine($"Skipping non image file {Path.GetFileNameWithoutExtension(file)}");
				}
			}
			foreach (string subDir in Directory.GetDirectories(inPath)) {
				if (Directory.GetFiles(subDir).Length != 2 || TryEncodeImageFromImageFolder(subDir, outPath) != null) {
					string outSubDir = Path.Combine(outPath, Path.GetFileNameWithoutExtension(subDir));
					Directory.CreateDirectory(outSubDir);
					RecursiveEncode(subDir, outSubDir);
				}
			}
		}

		private static string TryEncodeImageFromImageFolder(string imageFolder, string outPath) {
			string imageName = Path.GetFileNameWithoutExtension(imageFolder);
			string header = GetHeaderFromImageFolder(imageFolder);
			string dataPath = GetDataPathFromImageFolder(imageFolder);
			string[] headerPieces = header.Split(new[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

			if (headerPieces.Length == 0) {
				return $"Image {imageName} has an improperly formatted header";
			}

			if (headerPieces[0].Equals(IMG_IMAGE_HEADER)) {
				if (headerPieces.Length != 2) {
					return $"IMG image {imageName} has an improperly formatted header";
				}
				Size size = ParseSize(headerPieces[1]);
				using (var imgImage = new IMGImage(imageName, dataPath, size)) {
					imgImage.Decode(outPath);
				}
			}
			else if (headerPieces[0].Equals(STD_IMAGE_HEADER)) {
				if (headerPieces.Length != 4) {
					return $"STD image {imageName} has an improperly formatted header";
				}
				Size size = ParseSize(headerPieces[1]);
				Size contentSize = ParseSize(headerPieces[2]);
				Point drawStart = new Point(ParseSize(headerPieces[3]));
				using (var stdImage = new STDImage(imageName, dataPath, size, contentSize, drawStart)) {
					stdImage.Decode(outPath);
				}
			}
			else {
				return $"Unknown type {headerPieces[0]} for image {imageName}";
			}
			return null;
		}

		private static string TryEncodeRawImage(string inPath, string outPath) {
			try {
				string imageName = Path.GetFileNameWithoutExtension(inPath);
				using (var rawImage = new RawImage(imageName, inPath)) {
					rawImage.Decode(outPath);
				}
				return null;
			}
			catch (Exception e) {
				return $"Error in decoding RAW image: {e.Message}";
			}
		}

		private static string TryEncodeRawImageArray(string inPath, string outPath) {
			try {
				string imageFolderName = Path.GetFileNameWithoutExtension(inPath);
				using (var rawImageArray = new RawImageArray(imageFolderName, inPath)) {
					rawImageArray.Decode(outPath);
				}
				return null;
			}
			catch(Exception e) {
				return $"Error in decoding RAW image array: {e.Message}";
			}
		}

		private static Size ParseSize(string sSize) {
			string[] splitSize = sSize.Split(',');
			if (splitSize.Length != 2 || splitSize.Any((size) => size.Any((c) => !char.IsDigit(c)))) {
				Console.Error.AndExit($"Size {sSize} could not be parsed");
			}
			return new Size(int.Parse(splitSize[0]), int.Parse(splitSize[1]));
		}

		private static string GetHeaderFromImageFolder(string folder) {
			foreach (string file in Directory.GetFiles(folder)) {
				if (Path.GetFileNameWithoutExtension(file) == MF_IMAGE_HEADER_FILE_NAME) {
					return Encoding.ASCII.GetString(File.ReadAllBytes(file));
				}
			}
			Console.Error.AndExit($"Image {Path.GetDirectoryName(folder)} does not have a header definition file with name {MF_IMAGE_HEADER_FILE_NAME}");
			return null;
		}

		private static string GetDataPathFromImageFolder(string folder) {
			foreach (string file in Directory.GetFiles(folder)) {
				if (Path.GetFileNameWithoutExtension(file) == MF_IMAGE_DATA_FILE_NAME) {
					return file;
				}
			}
			Console.Error.AndExit($"Image {Path.GetDirectoryName(folder)} does not have a data file with name {MF_IMAGE_DATA_FILE_NAME}");
			return null;
		}
	}
}

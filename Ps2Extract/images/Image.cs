﻿using ExtractCommon;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Ps2Extract.Images {
	abstract class Image : IDisposable {
		private const int PALETTE_256_COLORS_1_COLOR_PER_BYTE = 0x13;
		private const int PALETTE_16_COLORS_2_COLORS_PER_BYTE = 0x14;

		protected string Name { get; }
		protected BufferedFileReader Src { get; }
		internal Image(string name, string dataPath) {
			Name = name;
			Src = new BufferedFileReader(dataPath);
		}

		internal abstract void Decode(string outPath);

		public void Dispose() {
			Src.Dispose();
		}

		protected int ReadHeader() {
			Src.Reader.ReadInt32();
			int headerEntries = Src.Reader.ReadInt32();
			Src.Reader.ReadInt32();
			Src.Reader.ReadInt32(); // fileSize
			return headerEntries;
		}

		protected ImageChunk ReadChunkHeader() {
			Src.Reader.ReadInt32();
			Src.Reader.ReadInt32();
			Src.Reader.ReadInt32(); // bitsPerPixel
			int chunkType = Src.Reader.ReadInt32();
			int width = Src.Reader.ReadInt32();
			int height = Src.Reader.ReadInt32();
			Src.Reader.ReadInt32(); // paletteStart
			Src.Reader.ReadInt32(); // dataStart
			if(chunkType == PALETTE_256_COLORS_1_COLOR_PER_BYTE) {
				return new ImageChunkType13(width, height);
			}
			else if(chunkType == PALETTE_16_COLORS_2_COLORS_PER_BYTE) {
				return new ImageChunkType14(width, height);
			}
			else {
				throw new BadImageFormatException($"Unsupported chunk type 0x{chunkType:X} for image {Name}");
			}
		}

		protected void ReadChunks(params ImageChunk[] chunks) {
			foreach (var chunk in chunks) {
				chunk.ReadPalette(Src.Reader);
			}
			foreach (var chunk in chunks) {
				chunk.ReadData(Src.Reader);
			}
		}

		protected ImageChunk[] ReadMultiChunkFile() {
			int headerEntries = ReadHeader();
			ImageChunk[] chunks = new ImageChunk[headerEntries];
			for (int i = 0; i < chunks.Length; i++) {
				chunks[i] = ReadChunkHeader();
			}
			ReadChunks(chunks);
			return chunks;
		}

		protected static void ScaleAndSave(Bitmap bitmap, string outPath, ImageFormat format) {
			using (Bitmap scaled = new Bitmap(bitmap, new Size((int)(bitmap.Width * 1.25), (int)(bitmap.Height * 1.25)))) {
				scaled.SetResolution(bitmap.HorizontalResolution, bitmap.VerticalResolution);
				using (Graphics graphics = Graphics.FromImage(scaled)) {
					graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
					graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
					graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
					//draw the image into the target bitmap
					graphics.DrawImage(bitmap, 0, 0, scaled.Width, scaled.Height);
				}
				scaled.Save(outPath, format);
			}
		}
	}
}

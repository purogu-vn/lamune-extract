﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Ps2Extract.Images {
	internal class IMGImage : Image {

		private readonly Size size;
		internal IMGImage(string name, string dataPath, Size size) : base(name, dataPath) {
			this.size = size;
		}
		internal override void Decode(string outPath) {
			string imageFile = Name + ".png";
			Console.WriteLine($"Encoding IMG image {Name} ({size.Width}x{size.Height})");
			ImageChunk[] chunks = ReadMultiChunkFile();

			using (Bitmap final = new Bitmap(size.Width, size.Height, PixelFormat.Format32bppArgb)) {
				using (Graphics g = Graphics.FromImage(final)) {
					int x = 0, y = 0;
					foreach (ImageChunk chunk in chunks) {
						using (Bitmap bit = chunk.ToBitmap()) {
							if (x >= size.Width) {
								x = 0;
								y += bit.Height;
							}
							g.DrawImage(bit, x, y);
							x += bit.Width;
						}
					}
				}
				ScaleAndSave(final, Path.Combine(outPath, imageFile), ImageFormat.Png);
			}
		}
	}
}

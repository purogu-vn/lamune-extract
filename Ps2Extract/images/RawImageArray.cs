﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Ps2Extract.Images {
	internal class RawImageArray : Image {
		internal RawImageArray(string name, string dataPath) : base(name, dataPath) { }
		internal override void Decode(string outPath) {
			ImageChunk[] chunks = ReadMultiChunkFile();
			Console.WriteLine($"Encoding RAW image array {Name} of {chunks.Length} images");
			string arrayOutPath = Path.Combine(outPath, Name);
			Directory.CreateDirectory(arrayOutPath);
			for (int i = 0; i < chunks.Length; i++) {
				string imageName = i.ToString("X8") + ".png";
				Console.WriteLine($"Encoding RAW image {imageName} ({i + 1} / {chunks.Length})");
				using (Bitmap bit = chunks[i].ToBitmap()) {
					ScaleAndSave(bit, Path.Combine(arrayOutPath, imageName), ImageFormat.Png);
				}
			}
		}
	}
}

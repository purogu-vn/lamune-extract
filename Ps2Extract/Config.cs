﻿using System.Collections.Generic;

namespace Ps2Extract {
	internal class Config : ExtractCommon.Config {
		internal const string WORKING_FOLDER = "Ps2WorkingFolder";
		internal const string ASSETS_FOLDER = "Ps2AssetsFolder";
		internal const string ARC_FOLDER = "Ps2ArcFolder";
		internal const string ISO_DATA_PATH = "Ps2ISOData";
		internal const string ISO_EXE_PATH = "Ps2ISOExe";

		internal const string PSS_PLEX_PATH = "Ps2PSSPlex";
		internal const string FFMPEG_PATH = "Ps2FFMPEG";
		internal const string MFAUDIO_PATH = "Ps2MFAudio";
		internal const string OGGENC2_PATH = "Ps2Oggenc2";
		internal const string VGMSTREAM_PATCHED_PATH = "Ps2vgmstreamPatched";
		internal const string PYTHON3_PATH = "Ps2Python3";

		private static readonly IDictionary<string, string> DEFAULTS = new Dictionary<string, string> {
			[WORKING_FOLDER] = "[The folder to be used to store all intermediate files created during extraction]",
			[ASSETS_FOLDER] = "[The folder used to store the final extracted assets once all processing is complete]",
			[ARC_FOLDER] = "[The folder used to store files ready to be packed into ARC files]",
			[ISO_DATA_PATH] = "[Path to the original PS2 Lamune ISO data file]",
			[ISO_EXE_PATH] = "[Path to the executable code found in the original PS2 Lamune ISO]",
			[PSS_PLEX_PATH] = "[Required (video). Path to the PSS Plex program (https://www.zophar.net/utilities/ps2util/pss-plex.html)]",
			[FFMPEG_PATH] = "[Required (video). Path to ffmpeg.exe (https://www.ffmpeg.org/)]",
			[MFAUDIO_PATH] = "[Required (bgm). Path to the MFAudio program (https://www.zophar.net/utilities/ps2util/mfaudio-1-1.html)]",
			[OGGENC2_PATH] = "[Required (bgm, voice). Path to the oggenc2 program (https://www.rarewares.org/ogg-oggenc.php)",
			[VGMSTREAM_PATCHED_PATH] = "[Required (voice, sounds). Path to vgmstream with the xag patch (https://bitbucket.org/purogu-vn/vgmstream-xag-patch/src/master/)]",
			[PYTHON3_PATH] = "[Required (scripts). Path to Python3 (https://www.python.org/downloads/)]"
		};

		private Config(string path) : base(path, DEFAULTS) { }

		internal static void InitConfig(string path) {
			InitConfig(new Config(path));
		}
	}
}

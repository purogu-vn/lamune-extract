﻿using CommandLine;
using System.Collections.Generic;

namespace Ps2Extract {
	internal class CommandLineOptions {

		[Option('c', "config", Default = "lamune.cfg", Required = false)]
		public string ConfigPath { get; set; }

		[Option("steps", Required = false)]
		public IList<string> Steps { get; set; }

		[Value(0, MetaName = nameof(Action), Required = true)]
		public string Action { get; set; }

		[Value(1, MetaName = nameof(Step), Required = false)]
		public string Step { get; set; }

		[Value(2, MetaName = nameof(Substep), Required = false)]
		public string Substep { get; set; }
	}
}

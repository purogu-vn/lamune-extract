from pathlib import Path
import json

ps2_path = Path("../scenario.ps2.tbl.json")
pc_path = Path("../scenario.pc.tbl.json")
out_path = Path("../scenario.tbl.json")

def write_json(file, data):
	with file.open("w", encoding="utf8") as f:
		json.dump(data, f, indent=2)


def read_json(file):
	with file.open("r", encoding="utf8") as f:
		return json.load(f)

ps2 = read_json(ps2_path)
pc = read_json(pc_path)
merged = { int(k):v for k,v in ps2.items() }
for k,v in pc.items():
	ik = int(k)
	if ik in merged and merged[ik] != v:
		raise ValueError(f"Duplicate key: {ik} with ps2={merged[ik]} and pc={v}")
	merged[ik] = v
merged = dict(sorted(merged.items()))
write_json(out_path, merged)
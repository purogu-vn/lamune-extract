from pathlib import Path
import json
from shutil import copyfile

base_path = Path(r"A:\code\hack\out\scripts")
guess_path = base_path.joinpath("guess")
scripts_path = base_path.joinpath("in")
pc_path = base_path.joinpath("pc")
out_path = base_path.joinpath("out")

pc_branch_path = guess_path.joinpath("pcIdToBranch.json")
pc_name_path = guess_path.joinpath("pcIdToName.json")
pc_line_path = guess_path.joinpath("pcIdToLine.json")

raw_branch_path = guess_path.joinpath("rawToBranch.json")
raw_id_path = guess_path.joinpath("rawToId.json")
raw_hints_path = guess_path.joinpath("rawHints.json")
raw_name_path = guess_path.joinpath("rawToName.json")
raw_line_path = guess_path.joinpath("rawToLine.json")

scenario_path = guess_path.joinpath("scenario.tbl.json")

named_path = guess_path.joinpath("named")

auto_named_path = out_path.joinpath("named")
auto_raw_table_path = out_path.joinpath("scenario.raw.txt")
auto_raw_name_path = out_path.joinpath("rawToName.json")
auto_raw_id_path = out_path.joinpath("rawToId.json")
auto_scenario_path = out_path.joinpath("scenario.tbl.json")

def write_json(file, data):
	with file.open("w", encoding="utf8") as f:
		json.dump(data, f, sort_keys=True)


def read_json(file):
	with file.open("r", encoding="utf8") as f:
		return json.load(f)


def read_lines(file, encoding="utf8"):
	with file.open("r", encoding=encoding) as f:
		for line in f:
			yield line

def read_pc_table():
	name_to_id = {}
	id_to_name = {}
	with pc_path.joinpath("scenario.tbl").open("rb") as table:
		i = 0
		name = table.read(32)
		while name != b"":
			name = name.decode('ascii').rstrip('\0')
			if name:
				name_to_id[name] = i
				id_to_name[i] = name
			name = table.read(32)
			i += 1
	return name_to_id, id_to_name

def prepare_pc():
	name_to_id, id_to_name = read_pc_table()
	id_to_first_line = {}
	
	id_to_branches = {}
	for script in pc_path.joinpath("shortened").iterdir():
		name = script.stem[:script.stem.find(".")]
		if name not in name_to_id:
			print("Not in scenario table: {}".format(name))
			continue
		i = name_to_id[name]
		id_to_branches[i] = []
		for line in read_lines(script):
			if line.startswith("text") and i not in id_to_first_line:
				id_to_first_line[i] = line[(line.find("[") + 1):line.rfind("]")]
			elif line.startswith("load_script"):
				jump_to = line[(line.find("[") + 1):line.rfind("]")]
				jump_to_id = name_to_id[jump_to]
				id_to_branches[i].append(jump_to_id)
	for name in name_to_id:
		if not pc_path.joinpath("shortened", name + ".asb.txt").exists():
			print("No file exists: {}({})".format(name, name_to_id[name]))
			del id_to_name[name_to_id[name]]
	write_json(pc_branch_path, id_to_branches)
	write_json(pc_name_path, id_to_name)
	write_json(pc_line_path, id_to_first_line)

def ps2_name():
	pc_id_to_branches = read_json(pc_branch_path)
	pc_branches_to_ids = {}
	for i, branches in pc_id_to_branches.items():
		set_branches = frozenset(branches)
		if set_branches not in pc_branches_to_ids:
			pc_branches_to_ids[set_branches] = []
		pc_branches_to_ids[set_branches].append(i)
	pc_id_to_name = read_json(pc_name_path)
	pc_id_to_first_line = read_json(pc_line_path)
	pc_first_line_to_id = {}
	for i, line in pc_id_to_first_line.items():
		if line is not None:
			pc_first_line_to_id[line] = i

	raw_to_name = read_json(raw_name_path) if raw_name_path.exists() else {}
	raw_to_id = read_json(raw_id_path) if raw_id_path.exists() else {}
	raw_to_branches = {}
	raw_to_first_line = {}
	raw_to_first_line_id = {}
	hints = {}
	stats = {name: 0 for name in ["matched", "id", "unknown"]}

	for script in scripts_path.iterdir():
		if script.stem not in raw_to_id or not raw_to_id[script.stem]:
			raw_to_branches[script.stem] = []
			hints[script.stem] = {}
			raw_to_id[script.stem] = ""
			for line in read_lines(script, encoding="shift-jis"):
				if "//" in line:
					if '.txt' in line:
						hints[script.stem]["comment_file"] = line
					elif "シナリオナンバー" in line:
						hints[script.stem]["comment_scenario_num"] = line
					elif "(" in line and "(" in line and ("から" in line):
						hints[script.stem]["comment_from"] = line
				elif "@" in line:
					if line.startswith("@note"):
						hints[script.stem]["note"] = line
					elif "@next_scenario" in line:
						num = line[line.find('(') + 1:line.find(')')]
						num = int(num.strip())
						raw_to_branches[script.stem].append(num)
				elif script.stem not in raw_to_first_line_id:
					cleaned = line.strip().replace("\\", "")
					if cleaned and len(cleaned) > 4:
						if cleaned in pc_first_line_to_id:
							raw_to_first_line[script.stem] = cleaned
							raw_to_first_line_id[script.stem] = pc_first_line_to_id[cleaned]
							hints[script.stem]["first_line_match_id"] = pc_first_line_to_id[cleaned]
							hints[script.stem]["first_line_match_name"] = pc_id_to_name[pc_first_line_to_id[cleaned]]
						else:
							raw_to_first_line[script.stem] = None


			set_branches = frozenset(raw_to_branches[script.stem])
			if not len(set_branches) or set_branches not in pc_branches_to_ids:
				print("No links for {}".format(script.stem))
				stats["unknown"] += 1
			else:
				id_possible = pc_branches_to_ids[set_branches]
				if len(id_possible) == 1:
					i = id_possible[0]
					if script.stem in raw_to_first_line_id and raw_to_first_line_id[script.stem] == i:
						print("Exact match: {} = {}".format(script.stem, pc_id_to_name[i]))
						del hints[script.stem]
						raw_to_id[script.stem] = i
						raw_to_name[script.stem] = pc_id_to_name[i]
						stats["matched"] += 1
					else:
						print("Mismatch on guessing algorithms: {}".format(script.stem))
						hints[script.stem]["guess_ids"] = i
						hints[script.stem]["guess_names"] = pc_id_to_name[i]
						stats["unknown"] += 1
				else:
					if script.stem in raw_to_first_line_id and raw_to_first_line_id[script.stem] in id_possible:
						i = raw_to_first_line_id[script.stem]
						print("Overlapping match between algorithms: {} = {}".format(script.stem, i))
						del hints[script.stem]
						raw_to_id[script.stem] = i
						raw_to_name[script.stem] = pc_id_to_name[i]
						stats["matched"] += 1
					else:
						print("{} has {} matches".format(script.stem, len(id_possible)))
						hints[script.stem]["guess_ids"] = id_possible
						hints[script.stem]["guess_names"] = [pc_id_to_name[i] for i in id_possible]
						stats["unknown"] += 1
		else:
			i = raw_to_id[script.stem]
			if script.stem not in raw_to_name or not raw_to_name[script.stem]:
				if i in pc_id_to_name:
					raw_to_name[script.stem] = pc_id_to_name[i]
					print("Using id {} as name {}".format(i, pc_id_to_name[i]))
					stats["matched"] += 1
				else:
					raw_to_name[script.stem] = ""
					stats["id"] += 1
			elif i in pc_id_to_name and raw_to_name[script.stem] != pc_id_to_name[i]:
				print("Updating name with id {} to be {} (was {})".format(i, pc_id_to_name[i], raw_to_name[script.stem]))
				raw_to_name[script.stem] = pc_id_to_name[i]
				stats["matched"] += 1
			else:
				stats["matched"] += 1

	write_json(raw_name_path, raw_to_name)
	write_json(raw_id_path, raw_to_id)
	write_json(raw_branch_path, raw_to_branches)
	write_json(raw_hints_path, hints)
	write_json(raw_line_path, raw_to_first_line)
	print("\nSummary:")
	for name, count in stats.items():
		print("{}: {}".format(name, count))

def copy(src, dest):
	copyfile(str(src), str(dest))

def apply_names():
	raw_to_name = read_json(raw_name_path)
	raw_to_id = read_json(raw_id_path)
	id_to_name_ps2 = {}
	for raw in raw_to_name:
		name = raw_to_name[raw]
		if not name:
			continue
		i = raw_to_id[raw]
		if i in id_to_name_ps2:
			print(i, id_to_name_ps2, name)
			raise
		id_to_name_ps2[i.zfill(4)] = name
		src = scripts_path.joinpath(raw + ".txt")
		dest = named_path.joinpath(name + ".ps2.txt")
		copy(src, dest)
	write_json(scenario_path, id_to_name_ps2)


def auto_extract():
	
	table = {}
	for line in read_lines(auto_raw_table_path, encoding="shift-jis"):
		i, raw = line.strip().split("=")
		table[raw.lower()] = int(i)
	if len(table) != 572:
		raise
	pc_id_to_name = read_pc_table()[1]
	print(pc_id_to_name)
	raw_to_name = {}
	raw_to_id = {}
	for script in scripts_path.iterdir():
		if script.stem not in table:
			print(script.stem)
			raise
		i = table[script.stem]
		raw_to_id[script.stem] = i
		raw_to_name[script.stem] = pc_id_to_name[i] if i in pc_id_to_name else ""
	write_json(auto_raw_id_path, raw_to_id)
	write_json(auto_raw_name_path, raw_to_name)

def auto_build():
	pc_id_to_name = read_pc_table()[1]
	raw_to_id = read_json(auto_raw_id_path)
	raw_to_name = read_json(auto_raw_name_path)
	table = {}
	for script in scripts_path.iterdir():
		i = raw_to_id[script.stem]
		name = raw_to_name[script.stem]
		if not name:
			print("No name for {}({})".format(script.stem, i))
		else:
			table[i] = name
			dest = auto_named_path.joinpath(name + ".ps2.txt")
			copy(script, dest)
	for i, name in pc_id_to_name.items():
		if i not in table:
			print("Scenario {}({}) not in ps2".format(name, i))
	write_json(auto_scenario_path, table)

#prepare_pc()
#ps2_name()
#apply_names()

#auto_extract()
auto_build()

from pathlib import Path
import json
from shutil import copyfile

base_path = Path(r"A:\code\hack\out\bgm")
out_path = base_path.joinpath("out")
pc_track_path = base_path.joinpath("track.tbl")
bgm_path = base_path.joinpath("ogg")

named_path = out_path.joinpath("named")
raw_table_path = out_path.joinpath("track.raw.txt")
raw_name_path = out_path.joinpath("rawToName.json")
raw_id_path = out_path.joinpath("rawToId.json")
track_path = out_path.joinpath("track.tbl.json")


def write_json(file, data):
	with file.open("w", encoding="utf8") as f:
		json.dump(data, f, sort_keys=True)


def read_json(file):
	with file.open("r", encoding="utf8") as f:
		return json.load(f)

def read_lines(file, encoding="utf8"):
	with file.open("r", encoding=encoding) as f:
		for line in f:
			yield line

def copy(src, dest):
	copyfile(str(src), str(dest))


def read_pc_table():
	id_to_name = {}
	with pc_track_path.open("rb") as table:
		i = 0
		name = table.read(32)
		while name != b"":
			name = name.decode('shift-jis').rstrip('\0')
			if name:
				id_to_name[i] = name
			name = table.read(32)
			i += 1
	return id_to_name

def extract():
	table = {}
	for line in read_lines(raw_table_path):
		i, raw = line.strip().split("=")
		table[raw] = int(i)
	pc_id_to_name = read_pc_table()
	raw_to_name = {}
	raw_to_id = {}
	for bgm in bgm_path.iterdir():
		raw = bgm.stem
		if raw not in table:
			print("Not found in table in-game: {}".format(raw))
			continue
		i = table[raw]
		raw_to_id[raw] = i
		raw_to_name[raw] = pc_id_to_name[i] if i in pc_id_to_name else ""
	write_json(raw_id_path, raw_to_id)
	write_json(raw_name_path, raw_to_name)

def build():
	raw_to_id = read_json(raw_id_path)
	raw_to_name = read_json(raw_name_path)
	table = {}
	for bgm in bgm_path.iterdir():
		raw = bgm.stem
		if raw not in raw_to_id:
			print("Not found in table in-game: {}".format(raw))
			continue
		i = raw_to_id[raw]
		name = raw_to_name[raw]
		if not name:
			print("No name for {}({})".format(raw, i))
		else:
			table[i] = name
			dest = named_path.joinpath(name + ".ogg")
			copy(bgm, dest)
	write_json(track_path, table)


#extract()
build()
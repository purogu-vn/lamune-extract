from pathlib import Path
import json
from shutil import copyfile

base_path = Path(r"A:\code\hack\out\images\cg")

raw_table_path = base_path.joinpath("cg.raw.txt")
raw_name_path = base_path.joinpath("nameMap.txt")
pc_cg_path = base_path.joinpath("cg.tbl")

cg_path = base_path.joinpath("cg.tbl.json")

def write_json(file, data):
	with file.open("w", encoding="utf8") as f:
		json.dump(data, f)

def read_lines(file, encoding="utf8"):
	with file.open("r", encoding=encoding) as f:
		for line in f:
			yield line

def read_text_map(file):
	m = {}
	for line in read_lines(file):
		k, v = line.strip().split("=")
		m[k] = v
	return m

def read_pc_table():
	id_to_name = {}
	with pc_cg_path.open("rb") as table:
		i = 0
		name = table.read(32)
		while name != b"":
			name = name.decode('shift-jis').rstrip('\0')
			if name:
				id_to_name[str(i)] = name
			name = table.read(32)
			i += 1
	return id_to_name


pc_id_to_name = read_pc_table()
id_to_raw = read_text_map(raw_table_path)
raw_to_name = read_text_map(raw_name_path)
table = {}
print(pc_id_to_name)

for i, raw in id_to_raw.items():
	if raw not in raw_to_name:
		print("No name for {}({})".format(raw, i))
	else:
		table[i] = raw_to_name[raw]
		del raw_to_name[raw]

for i, name in pc_id_to_name.items():
	if i in table:
		if table[i] != name:
			print("{} has changed name: (pc={}, ps2={})".format(i, name, table[i]))
	else:
		print("{} only in pc: {}".format(i, name))

write_json(cg_path, table)
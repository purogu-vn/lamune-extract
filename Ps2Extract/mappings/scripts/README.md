These scripts were used recursively to assign names and ids to the raw table entries.
The PC version of the tables were used as a starting point as they share common assignments.
The scripts have not since been updated or made generic at the moment since the mappings have been completed.
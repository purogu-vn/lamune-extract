﻿using System.IO;

namespace Ps2Extract {
	internal class ADPCMHeader {
		internal int SampleRate { get; set; }
		internal int Offset { get; set; }
		internal int Length { get; set; }

		internal ADPCMHeader(BinaryReader br) {
			SampleRate = br.ReadInt32();
			Offset = br.ReadInt32();
			Length = br.ReadInt32();
			br.ReadBytes(20);
		}
	}
}

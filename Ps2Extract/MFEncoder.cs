﻿using ExtractCommon;
using System;
using System.IO;
using System.Text;

namespace Ps2Extract {
	internal static class MFEncoder {

		internal static void Extract(string mfPath, string outDir) {
			if (!TryExtract(mfPath, outDir)) {
				Console.Error.AndExit($"{Path.GetFileName(mfPath)} is not a MF file");
			}
		}

		internal static void RecursiveExtract(string mfPath, string outDir) {
			TryExtract(mfPath, outDir);
			RecursiveExtractHelper(outDir);
		}

		private static void RecursiveExtractHelper(string workingDir) {
			foreach (string file in Directory.GetFiles(workingDir)) {
				string extractPath = Path.Combine(workingDir, Path.GetFileNameWithoutExtension(file));
				Directory.CreateDirectory(extractPath);
				if(TryExtract(file, extractPath)) {
					File.Delete(file);
					Console.WriteLine($"Recursively extracting contents of {Path.GetFileNameWithoutExtension(file)}");
					RecursiveExtractHelper(extractPath);
				}
				else {
					Directory.Delete(extractPath);
				}
			}
		}

		private static bool TryExtract(string mfPath, string outDir) {
			using (var src = new BufferedFileReader(mfPath)) {
				string sig = Encoding.ASCII.GetString(src.Reader.ReadBytes(4)).TrimEnd('\0', '\n', '\r');
				if (sig.Equals("MF")) {
					Console.WriteLine($"Extracting MF: {Path.GetFileName(mfPath)}");
					int numFiles = src.Reader.ReadInt32();
					src.Reader.ReadInt32(); // dataStart
					src.Reader.ReadBytes(4);
					MFEntry[] entries = new MFEntry[numFiles];
					for (int i = 0; i < numFiles; i++) {
						entries[i] = new MFEntry(i.ToString("X8"), src.Reader);
					}
					foreach (var entry in entries) {
						Console.WriteLine($"Extracting MF entry: {entry.FileName}");
						entry.ExtractToFile(src, outDir);
					}
					return true;
				}
				else {
					return false;
				}
			}
		}
	}
}

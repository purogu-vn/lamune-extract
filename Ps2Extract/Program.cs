﻿using CommandLine;
using CommandLine.Text;
using Ps2Extract.Steps;
using Ps2Extract.Steps.Arc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ps2Extract {
	internal class Program {
		private const int MAX_HELP_TEXT_WIDTH = 160;

		private static readonly ExtractionStep[] STEPS = new ExtractionStep[] {
			new ISODataStep(),
			new Ps2ScriptsStep(),
			new Ps2ScriptMappingsStep(),
			new BGMStep(),
			new SoundsStep(),
			new VoiceStep(),
			new VideoStep(),
			new TablesStep(),
			new ImagesStep(),
			new CharactersStep(),
			new BackgroundsStep(),
			new ScriptsStep(),
			new AudioArcStep(),
			new VisualArcStep(),
			new CodeArcStep(),
			// Optional
			new RawTablesStep(),
			new SysgraphStep(),
			new ThumbsStep()
		};

		public static void Main(string[] args) {
			var parser = new Parser(with => with.HelpWriter = null);
			var result = parser.ParseArguments<CommandLineOptions>(args);
			result.WithParsed(HandleParsed)
				.WithNotParsed((errors) => HandleNotParsed(result, errors));
		}

		private static void HandleParsed(CommandLineOptions options) {
			Config.InitConfig(options.ConfigPath);

			if(!options.Action.Equals("extract")) {
				Console.Error.AndExit($"Action {options.Action} is not supported");
			}

			if (options.Step != null && options.Steps.Count > 0) {
				Console.Error.AndExit($"Cannot specify a single step to run {options.Step} in addition to provided list of steps {string.Join(",", options.Steps)}. Use one or the other.");
			}
			else if(options.Step != null) {
				var step = FindStep(options.Step);
				step.Run(options.Substep);
			}
			else {
				IEnumerable<ExtractionStep> steps = options.Steps.Select(FindStep);
				foreach(var step in steps) {
					step.Run();
				}
			}

			Console.WriteLine("Done");
			Console.ReadLine();
		}

		private static void HandleNotParsed(ParserResult<CommandLineOptions> result, IEnumerable<Error> errors) {
			Console.WriteLine(GenerateHelpText(result, errors));
			Console.ReadLine();
			Environment.Exit(10);
		}

		private static HelpText GenerateHelpText(ParserResult<CommandLineOptions> result, IEnumerable<Error> errors) {
			if (errors.IsVersion()) {
				return HelpText.AutoBuild(result, MAX_HELP_TEXT_WIDTH);
			}
			else {
				return HelpText.AutoBuild(result, helpText => {
					ModifyHelpText(helpText);
					return helpText;
				}, MAX_HELP_TEXT_WIDTH);
			}
		}

		private static void ModifyHelpText(HelpText helpText) {
			helpText.AddPostOptionsLine("Supported extraction steps:");
			bool switchedToOptional = false;
			foreach (var step in STEPS) {
				if(!switchedToOptional && step.Optional) {
					helpText.AddPostOptionsLine("Optional extraction steps:");
					switchedToOptional = true;
				}
				helpText.AddPostOptionsLine($"    {step}");
			}
			
		}

		private static ExtractionStep FindStep(string name) {
			foreach(var step in STEPS) {
				if(step.Name.Equals(name)) {
					return step;
				}
			}
			Console.Error.AndExit($"Could not locate step with name: {name}");
			return null;
		}
	}
}

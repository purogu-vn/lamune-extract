from pathlib import Path
from shutil import copyfile
import subprocess
import sys

GAME_FOLDER = Path(r"A:\code\vn\lamune\sandbox\ps2\game")
ARC_PREP_FOLDER = Path(r"A:\code\vn\lamune\sandbox\ps2\arc")
VIDEO_FOLDER = Path(r"A:\code\vn\lamune\sandbox\ps2\assets\video")
EXTRACT_EXE = Path(r"A:\code\vn\lamune\lamune-extract\build\release\LamuneExtract.exe")

CONFIG_TEMPLATE = {
	"GameFolder": str(GAME_FOLDER),
	"CustomInsertionFolder": "assets",
	"InsertionCacheFolder": "cache",
	"ShowTime": "true",
	"CustomInsertSupressOutput": "true"
}

ARC_NAME_TO_NUM = {
	"audio": 2,
	"visual": 3,
	"code": 4
}

def write_config(path, config):
	with path.open("w", encoding="utf8") as f:
		for key, value in config.items():
			f.write("{}={}\n".format(key, value))

def copy(src, dest):
	copyfile(str(src), str(dest))

def locate_build_config_file():
	user_config = None
	config = None
	for file in Path(".").iterdir():
		if file.name.endswith(".user.cfg"):
			user_config = file
		elif file.name.endswith(".cfg"):
			config = file
	if user_config:
		return user_config
	if config:
		return config
	raise ValueError("Could not locate config file")

class Config(dict):
	pass

def load_build_config():
	config = Config()
	config_file = locate_build_config_file()
	with config_file.open("r") as f:
		for line in f:
			if line:
				key, value = line.split("=")
				setattr(config, key.strip(), Path(value.strip()))
	return config


def build_configs():
	print("Building config files")
	for arc in CONFIG.ArcPrepFolder.iterdir():
		config = { **CONFIG_TEMPLATE }
		config["OutputArcName"] = "data0{}.arc".format(ARC_NAME_TO_NUM[arc.name])
		config_path = arc.joinpath("lamune.cfg")
		write_config(config_path, config)

def build_arc(arc):
	print("Building ARC: {}\n==============================".format(arc))
	subprocess.run([str(CONFIG.ExtractExe), "insert", "custom"], check=True, cwd=CONFIG.ArcPrepFolder.joinpath(arc))

def copy_videos():
	print("Copying videos")
	game_video_folder = CONFIG.GameFolder.joinpath("movie")
	game_video_folder.mkdir(exist_ok=True)
	for video in CONFIG.VideoFolder.iterdir():
		game_video = game_video_folder.joinpath(video.name)
		copy(video, game_video)

def run_game():
	subprocess.run([str(CONFIG.GameFolder.joinpath("lamune.exe"))], check=True)

CONFIG = load_build_config()

cmds = sys.argv[1:]

for cmd in cmds:
	if cmd == "video":
		copy_videos()
	elif cmd == "configs":
		build_configs()
	elif cmd == "run":
		run_game()
	else:
		build_arc(cmd)

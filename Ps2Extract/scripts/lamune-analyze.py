import argparse
from pathlib import Path
import json

ENCODING = "cp932"
TRANSLATABLE = ("text", "window_title", "choice")
STAT_CATS = ("hiragana", "katakana", "kanji", "lines", "characters")
PERCENTS_FILE = "_percents.json"

def json_load(file):
	with file.open('r', encoding="utf8") as f:
		return json.load(f)

def hiragana(b):
	return b[0] == 0x82 and b[1] <= 0xF1 and b[1] >= 0x9F

def katakana(b):
	return b[0] == 0x83 and b[1] <= 0x96 and b[1] >= 0x40

def kanji(b):
	return b[0] <= 0xFC and b[0] >= 0x87

def identify(b):
	if hiragana(b):
		return "hiragana"
	elif katakana(b):
		return "katakana"
	elif kanji(b):
		return "kanji"

def add_stat_data(stats, text, tl):
	stats["total_lines"] += 1
	stats["total_characters"] += len(text)
	if tl:
		stats["tl_lines"] += 1
		stats["tl_characters"] += len(text)
	for char in text:
		id = identify(char.encode(ENCODING))
		if id:
			stats["total_" + id] += 1
			if tl:
				stats["tl_" + id] += 1

def create_base_stats():
	stats = {}
	for cat in STAT_CATS:
		stats["total_" + cat] = 0
		stats["tl_" + cat] = 0
	return stats

def handle_stat_one(file):
	stats = create_base_stats()
	j = json_load(file)
	if file.name == "_names.json":
		for original, tl in j.items():
			add_stat_data(stats, original, tl)
	else:
		for line in j["lines"]:
			if line["command"] in TRANSLATABLE:
				if line["command"] == "choice":
					for original, tl in zip(line["original"], line["tl"]):
						add_stat_data(stats, original, tl)
				else:
					add_stat_data(stats, line["original"], line["tl"])
	return stats

def formatted_stats(title, stats):
	return "{}: {}".format(title, ", ".join([ "{}={}".format(k, v) for k,v in stats.items() ]))

def handle_stat(args):
	stats = {}
	if args.file:
		stats[args.file] = handle_stat_one(Path(args.file))
	else:
		src = Path(args.directory or 'out/json-asb')
		for file in src.iterdir():
			if file.name != PERCENTS_FILE:
				stats[file.name] = handle_stat_one(file)
	total_stats = create_base_stats()
	for file_name, file_stats in stats.items():
		print(formatted_stats(file_name, file_stats))
		for stat_name, stat in file_stats.items():
			total_stats[stat_name] += stat
	print(formatted_stats("Total", total_stats))

def parse_stat(parser):
	parser.add_argument('--file', '-f')
	parser.add_argument('--directory', '-d')

def handle_ratio_one(file, total_stats):
	ratios = {}
	stats = handle_stat_one(file)
	for cat in STAT_CATS:
		total_cat = "total_" + cat
		tl_cat = "tl_" + cat
		total_stats[total_cat] += stats[total_cat]
		total_stats[tl_cat] += stats[tl_cat]
		if stats[total_cat]:
			ratios[cat] = stats[tl_cat] / stats[total_cat]
	return ratios

def formatted_ratios(title, ratios):
	return "{}: {}".format(title, (", ".join(["{}={}%".format(k, round(v * 100, 3)) for k,v in ratios.items()]) if len(ratios) else "No text") if not len(ratios) or any([v > 0 for v in ratios.values()]) else "Untouched")

def handle_ratio(args):
	ratios = {}
	total_stats = create_base_stats()
	if args.file:
		ratios[args.file] = handle_ratio_one(Path(args.file), total_stats)
	else:
		src = Path(args.directory or 'out/json-asb')
		for file in src.iterdir():
			if file.name != PERCENTS_FILE:
				ratios[file.name] = handle_ratio_one(file, total_stats)
	for file_name, file_ratios in ratios.items():
		print(formatted_ratios(file_name, file_ratios))
	total_ratios = {}
	for cat in STAT_CATS:
		total_cat = "total_" + cat
		tl_cat = "tl_" + cat
		if total_stats[total_cat]:
			total_ratios[cat] = total_stats[tl_cat] / total_stats[total_cat]
	print(formatted_ratios("Total", total_ratios))
	

def parse_ratio(parser):
	parser.add_argument('--file', '-f')
	parser.add_argument('--directory', '-d')

def handle_unneeded(args):
	src = Path(args.source_directory or './out/json-asb')
	for file in src.iterdir():
		if file.name.endswith('.asb.json'):
			j = json_load(file)
			noneToTranslate = True
			for line in j["lines"]:
				if line["command"] in TRANSLATABLE:
					noneToTranslate = False
			if noneToTranslate:
				print(file.name)

def parse_unneeded(parser):
	parser.add_argument('--source-directory', '-s')

HANDLERS = {
	"stat" : handle_stat,
	"ratio" : handle_ratio,
	"unneeded" : handle_unneeded
}

PARSERS = {
	"stat" : parse_stat,
	"ratio" : parse_ratio,
	"unneeded": parse_unneeded
}

parser = argparse.ArgumentParser()
parser.add_argument('action', choices=HANDLERS.keys(),
							help='The action for the program to take')
action = parser.parse_known_args()[0].action
PARSERS[action](parser)
HANDLERS[action](parser.parse_args())
from pathlib import Path
import json
import base64
import struct

def join_args(args):
	return ",".join(str(arg) for arg in args)

def bytes_to_name(bytes):
	return bytes.decode("ascii").strip("\x00")

def data_to_name(line):
	return bytes_to_name(decoded_data(line))

def decoded_data(line):
	return base64.b64decode(line["data"].encode("ascii"))

def decoded_ints(data, n):
	return struct.unpack("{}i".format(n), data)

def unmash_shorts_from_int(i):
	return i >> 16, i & 0xFFFF

def int_to_rgb(i):
	r = i >> 16
	g = (i >> 8) & 0xFF
	b = i & 0xFF
	return "rgb({},{},{})".format(r,g,b)

def number_to_person(num):
	if num == 1:
		return "nanami"
	elif num == 2:
		return "hikari"
	elif num == 3:
		return "suzuka"
	elif num == 4:
		return "tae"
	else:
		return "other"

comparison_op_to_name = {
	1: "!=",
	3: ">",
	4: "<"
}

transition_to_name = {
	1: "NORMAL",
	2: "CROSSFADE_RATIO",
	3: "CROSSFADE_SMOOTH",
	5: "QUAKE",
	6: "QUAKE_VERTICAL",
	7: "QUAKE_HORIZON",
	9: "WAVE_HORIZON",
	10: "SPLIT_L_TO_R",
	11: "SPLIT_R_TO_L",
	17: "MOSAIC",
	18: "FLASH"
}

position_to_name = {
	0: "LEFT",
	1: "CENTER",
	2: "RIGHT",
	5: "ALL"
}

scroll_type_to_name = {
	2: "U_TO_D"
}

def lookup(d, i):
	return d[i] if i in d else "unk{}".format(i)

def command_to_out(command, args, line):
	if command == "exit":
		return command
	elif command == "0x01":
		return "nop"
	elif command == "0x02":
		return "break_skip"
	elif command == "0x03":
		return "quickexit"
	elif command == "window_title":
		return "{} [{}]".format(command, line["original"])
	elif command == "load_script":
		return "{} [{}]".format(command, line["script_name"])
	elif command == "0x06":
		return "clear_script"
	elif command == "branchflag":
		enc_data = decoded_data(line)
		numFlags = args[1]
		flags = decoded_ints(enc_data, numFlags)
		str_flags = join_args(["flag={}".format(flag) for flag in flags])
		return "{} [skip {} if {} {} {}set]".format(command, line["jump"], str_flags, "is" if len(flags) == 1 else "are", "not " if args[1] == 1 else "")
	elif command == "branch":
		op = args[3]
		op = comparison_op_to_name[op] if op in comparison_op_to_name else "op" + str(op)
		return "{} [skip {} if var{} {} {}]".format(command, line["jump"], args[1], op, args[2])
	elif command == "jump":
		return "{} {}".format(command, line["jump"])
	elif command == "0x10":
		label = data_to_name(line)
		return "label {}".format(label)
	elif command == "gotolabel":
		label = data_to_name(line)
		if "script_name" in line:
			return "{} {} in {}".format(command, label, line["script_name"])
		else:
			return "{} {}".format(command, label)
	elif command == "0x13":
		seconds = args[1] / 1000
		return "wait {} seconds".format(seconds)
	elif command == "0x14":
		channel = args[1]
		return "wait_wav for channel {}".format(channel)
	elif command == "0x15":
		channel = args[1]
		seconds = args[2] / 1000
		return "wait_music for channel {} to reach {} seconds".format(channel, seconds)
	elif command == "0x16":
		showTriangle = args[1]
		return "wait_click [showTriangle={}]".format("true" if showTriangle else "false")
	elif command == "0x17":
		return "toggleinput {}".format("allowed" if args[1] == 1 else "disabled")
	elif command == "0x18":
		return "setflag [flag={}]".format(args[1])
	elif command == "0x19":
		return "clearflag [flag={}]".format(args[1])
	elif command == "0x1B":
		return "assign [var{}={}]".format(args[1], args[2])
	elif command == "0x1C":
		return "add [var{}+={}]".format(args[1], args[2])
	elif command == "name":
		if args[1] == 1:
			return "{} [{}]".format(command, line["original"])
		else:
			return "{} unset".format(command)
	elif command == "text":
		bitFlags = args[1]
		return "{}({}) [{}]".format(command, bitFlags, line["original"])
	elif command == "0x28":
		x, y = unmash_shorts_from_int(args[1])
		halfWidth, perLine = unmash_shorts_from_int(args[2])
		fontSize, lineHeight = unmash_shorts_from_int(args[3])
		return "text_window[x={}, y={}, halfWidth={}, perLine={}, fontSize={}, lineHeight={}]".format(x,y,halfWidth,perLine,fontSize,lineHeight)
	elif command == "0x29":
		return "reset_text_window"
	elif command == "0x2A":
		foreground = int_to_rgb(args[1])
		background = int_to_rgb(args[2])
		return "text_color [fore={}, back={}]".format(foreground, background)
	elif command == "0x2B":
		return "reset_text_color"
	elif command == "0x2E":
		speed = args[1]
		return "text_speed [{}]".format(speed if speed else "reset")
	elif command == "0x2F":
		enc_data = decoded_data(line)
		transition_name = lookup(transition_to_name, args[1])
		num_args = args[2]
		transition_args = join_args(decoded_ints(enc_data, num_args))
		return "update({}{})".format(transition_name, ":" + transition_args if transition_args else "")
	elif command == "0x30":
		enc_data = decoded_data(line)
		num_vals = args[1]
		vals = join_args(decoded_ints(enc_data, num_vals))
		return "update_config({})".format(vals)
	elif command == "0x31":
		textSave = args[1]
		return "update_text_save [{}]".format("on" if textSave else "off")
	elif command == "figure":
		position = lookup(position_to_name, args[1])
		if args[3] == 1:
			dec_name = data_to_name(line)
			return "{} set {} [{}]".format(command, position, dec_name)
		else:
			return "{} unset {}".format(command, position)
	elif command == "0x34":
		return "hidefigure {}".format(lookup(position_to_name, args[1]))
	elif command == "0x37":
		return "placefigurex {} centered at x={}".format(lookup(position_to_name, args[1]), args[2])
	elif command == "0x38":
		return "resetfigurex {}".format(lookup(position_to_name, args[1]))
	elif command == "bg":
		enc_data = decoded_data(line)
		l = decoded_ints(enc_data[:4], 1)[0]
		if l > 0:
			name = bytes_to_name(enc_data[4:])
			return "{} [{}]".format(command, name)
		else:
			return "{} [bgId={}]".format(command, args[1])
	elif command == "0x42":
		return "bgclear"
	elif command == "0x43":
		scroll_type_num, updateSpeed = unmash_shorts_from_int(args[1])
		scroll_type = scroll_type_to_name[scroll_type_num]
		cgNum = args[2]
		pixelsPerUpdate = args[3]
		return "scrollbg [{}:cgNum={},updateSpeed={},pixelsPerUpdate={}".format(scroll_type,cgNum,updateSpeed,pixelsPerUpdate)
	elif command == "0x47":
		return "placeoverlay at ({},{})".format(args[1], args[2])
	elif command == "0x48":
		return "setoverlay [cgNum={}]".format(args[1])
	elif command == "0x49":
		return "clearoverlay"
	elif command == "0x52":
		cgNum = args[1]
		return "textbox_cg {}".format("[cgNum={}]".format(cgNum) if cgNum else "reset")
	elif command == "0x56":
		cgNum = args[1]
		return "namebox_cg {}".format("[cgNum={}".format(cgNum) if cgNum else "reset")
	elif command == "0x58":
		return "show_lower"
	elif command == "0x59":
		return "hide_lower"
	elif command == "0x5A":
		on = args[1]
		return "toggle_textbox [{}]".format("on" if on else "off")
	elif command == "0x5B":
		on = args[1]
		return "toggle_namebox [{}]".format("on" if on else "off")
	elif command == "0x5C":
		on = args[1]
		return "toggle_menu [{}]".format("on" if on else "off")
	elif command == "music":
		channel = args[1]
		loop = args[3]
		return "{} on channel {} {}[{}]".format(command, channel, "(looping) " if loop else "", line["music_name"])
	elif command == "0x60":
		channel = args[1]
		trackNum = args[2]
		loop = args[3]
		return "music_fade on channel {} {}[{}]".format(channel, "(looping) " if loop else "", trackNum)
	elif command == "0x61":
		enc_data = decoded_data(line)
		channel = args[1]
		trackNum = args[2]
		loop = args[3]
		speed = decoded_ints(enc_data, 1)[0]
		return "music_fade_async on channel {} {}[{}, speed={}]".format(channel, "(looping) " if loop else "", trackNum, speed)
	elif command == "0x63":
		channel = args[1]
		return "stopmusic on channel {}".format(channel)
	elif command == "0x64":
		channel = args[1]
		return "stopmusic_fade on channel {}".format(channel)
	elif command == "0x65":
		channel, speed = args[1], args[2]
		return "stopmusic_fade_async on channel {} [speed={}]".format(channel, speed)
	elif command == "wav":
		channel = args[1]
		loop = args[3]
		dec_name = data_to_name(line)
		return "{} play {}on channel {} [{}]".format(command, "looping " if loop else "", channel, dec_name)
	elif command == "0x67":
		channel = args[1]
		loop = args[3]
		dec_name = data_to_name(line)
		return "wav_fade play {}on channel {} [{}]".format("looping " if loop else "", channel, dec_name)
	elif command == "0x68":
		channel = args[1]
		return "stopwav on channel {}".format(channel)
	elif command == "0x69":
		channel = args[1]
		return "stopwav_fade on channel {}".format(channel)
	elif command == "ogg":
		voice_name = data_to_name(line)
		person = number_to_person(args[1])
		return "{} [{}] ({})".format(command, voice_name, person)
	elif command == "0x6F":
		dec_name = data_to_name(line)
		return "mpg [{}]".format(dec_name)
	else:
		args = join_args(args)
		data = line["data"] if "data" in line else line["original"]
		return "{}({}) [{}]".format(command, args, data)


def shorten(json, out):
	for line in json["lines"]:
		command = line["command"]
		args = line["arguments"]
		formatted = command_to_out(command, args, line)
		out.write(formatted + "\n")

in_dir = Path("out/scripts")
out_dir = Path("out/shortened")

for file in in_dir.iterdir():
	if file.name.endswith(".asb.json"):
		out_path = out_dir.joinpath(file.name).with_suffix(".txt")
		with file.open("r", encoding="utf8") as original, out_path.open("w", encoding="utf8") as out:
			shorten(json.load(original), out)
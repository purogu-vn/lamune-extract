from pathlib import Path
import json
import struct
import base64
import sys

ASSET_TYPES = ["bgm", "video", "voice", "sounds", "backgrounds", "characters", "tables", "scripts"]

def write_json(file, data):
	with file.open("w", encoding="utf8") as f:
		json.dump(data, f)

def read_json(file):
	with file.open("r", encoding="utf8") as f:
		return json.load(f)

def read_lines(file):
	with file.open("r", encoding="shift-jis") as f:
		for line in f:
			yield line.strip()

def b64encode(data):
	return base64.b64encode(data).decode("ascii")

def c_string(s):
	return s.encode("shift-jis") + b"\x00"

def n_ints(args):
	return struct.pack("{}i".format(len(args)), *[int(arg) for arg in args])

def mash_shorts_to_int(short1, short2):
	return (int(short1) << 16) | int(short2)

def int_bool(args, idx, default="FALSE"):
	text = args[idx].upper() if idx < len(args) else default
	return 1 if text == "TRUE" else 0

comparison_sym_to_reverse_num = {
	"==": 1,
	"!=": 2,
	"<=": 3,
	">=": 4,
	"<": 5,
	">": 6
}

figure_position_to_num = {
	"LEFT": 0,
	"CENTER": 1,
	"RIGHT": 2,
	"ALL": 5
}

transition_name_to_num = {
	"NORMAL": 1,
	"CROSSFADE_RATIO": 2,
	"CROSSFADE_SMOOTH": 3,
	"QUAKE": 5,
	"QUAKE_VERTICAL": 6,
	"QUAKE_HORIZON": 7,
	"WAVE_HORIZON": 9,
	"SPLIT_L_TO_R": 10,
	"SPLIT_R_TO_L": 11,
	"SPLIT_U_TO_D": 12,
	"SPLIT_D_TO_U": 13,
	"MOSAIC": 17,
	"FLASH": 18
}

scroll_type_to_num = {
	"U_TO_D": 2
}

def register_asset(env, cat, name):
	env["requires"][cat].add(name.lower())

def register_music(trackNum, env):
	# remove leading zeros
	trackNum = str(int(trackNum))
	music_name = env["tables"]["track"][trackNum]
	register_asset(env, "bgm", music_name + ".ogg")

def register_cg(cgNum, env):
	if cgNum in env["tables"]["cg"]:
		cg_name = env["tables"]["cg"][cgNum]
		register_background(cg_name, env)
	else:
		print("Unknown cgNum={}".format(cgNum))

def register_character(chara_name, env):
	register_asset(env, "characters", chara_name + ".png")

def register_background(bg_name, env):
	register_asset(env, "backgrounds", bg_name + ".png")

def register_sound(wav_name, env):
	register_asset(env, "sounds", wav_name + ".wav")

def register_voice(ogg_name, env):
	register_asset(env, "voice", ogg_name + ".ogg")

def register_video(mpg_name, env):
	register_asset(env, "video", mpg_name + ".mpg")

def adjust_choice_command(cmd, choices):
	cmd["arguments"][1] = len(choices)
	cmd["original"] = choices
	cmd["tl"] = len(choices) * [""]
	cmd["finished"] = False

def make_tlable_command(cmd, args, text):
	return {
		"command": cmd,
		"arguments": format_args(args),
		"original": text,
		"tl": "",
		"finished": False
	}

def make_command_raw(cmd, args, data):
	return {
		"command": cmd,
		"arguments": args,
		"data": data
	}

def format_args(args):
	formattedArgs = [0,0,0,0]
	if args:
		for i in range(len(args)):
			formattedArgs[i + 1] = int(args[i])
	return formattedArgs

def make_command(cmd, args=None, data=b""):
	return make_command_raw(cmd, format_args(args), b64encode(data))


#########################################################################
# Command makers
#########################################################################

#0x00
def make_exit():
	return make_command_raw("exit", [-1,0,0,0], "")
#0x01
def make_nop(args, env):
	return make_command("0x01")
#0x02
def make_break_skip(args, env):
	return make_command("0x02")
#0x03
def make_quickexit(args, env):
	return make_command("0x03")
#0x04
def make_window_title(args, env):
	return make_tlable_command("window_title", [], args[0])
#0x05
def make_load_script(args, env):
	scenarioId = args[0]
	return make_command("load_script", [scenarioId])
#0x06
def make_clear_script(args, env):
	return make_command("0x06")
#0x0A
def make_branchflag(args, env):
	num_flags = len(args)
	data = n_ints(args)
	return make_command("branchflag", [num_flags], data)
#0x0B
def make_branch(args, env):
	var, op_sym, val = args[0].split(" ")
	var_num = var[1:]
	op = comparison_sym_to_reverse_num[op_sym]
	return make_command("branch", [var_num, val, op])
#0x0C
def make_jump(args, env):
	return make_command("jump")
#0x0D
def make_choice(args, env):
	return make_command("choice")
#0x10
def make_label(label):
	data = c_string(label)
	return make_command("0x10", [], data)
#0x12
def make_gotolabel(args, env):
	location = args[0]
	if ":" in location:
		scenarioId, label = location.split(":")
		differentFile = 1
	else:
		scenarioId, label = 0, location
		differentFile = 0
	data = c_string(label)
	return make_command("gotolabel", [scenarioId, differentFile], data)
#0x13
def make_wait(args, env):
	millis = args[0]
	return make_command("0x13", [millis])
#0x14
def make_wait_wav(args, env):
	channel = args[0]
	return make_command("0x14", [channel])
#0x15
def make_wait_music(args, env):
	channel = args[0]
	time = args[1]
	return make_command("0x15", [channel, time])
#0x16
def make_wait_click(args, env):
	showTriangle = int_bool(args, 0, "TRUE")
	return make_command("0x16", [showTriangle])
#0x17
def make_toggleinput(args, env):
	allowed = int_bool(args, 0)
	return make_command("0x17", [allowed])
#0x18
def make_setflag(args, env):
	flagNum = args[0]
	return make_command("0x18", [flagNum])
#0x19
def make_clearflag(args, env):
	flagNum = args[0]
	return make_command("0x19", [flagNum])
#0x1B
def make_assign(args, env):
	var_num = args[0][1:]
	val = args[1]
	return make_command("0x1B", [var_num, val])
#0x1C
def make_add(args, env):
	var_num = args[0][1:]
	val = args[1]
	return make_command("0x1C", [var_num, val])
#0x26
def make_name(line):
	return make_tlable_command("name", [1 if line else 0, 512 if line == "<FN>" else 0], line)
#0x27
def make_text(line):
	line = line.rstrip("\\")
	bitFlags = 0
	if "<BR>" in line:
		line = line.replace("<BR>", "")
		bitFlags = bitFlags | 1
	if "<NB>" in line:
		line = line.replace("<NB>", "")
		bitFlags = bitFlags | 2
	if "<NC>" in line:
		line = line.replace("<NC>", "")
		bitFlags = bitFlags | 4
	if "<NC2>" in line:
		line = line.replace("<NC2>", "")
		bitFlags = bitFlags | 4
	if "<FN>" in line:
		bitFlags = bitFlags | 512
	if ":" in line:
		bitFlags = bitFlags | 4096
	if "<NN>" in line:
		bitFlags = bitFlags | 8192
	# signals a line with <> markers, but not necessary to include
	if ">>" in line:
		line = line.replace(">>", "")
	# signals a hard end to a screen of text
	if "<DV2>" in line:
		line = line.replace("<DV2>", "")
	# wave dash gets converted to ? due to bug in Encoding.GetEncoding("shift_jis") not converting it properly back into bytes
	# replace with more popular full width tilde
	if "〜" in line:
		line = line.replace("〜", "～")
	return make_tlable_command("text", [bitFlags], line)
#0x28
def make_text_window(args, env):
	first2 = mash_shorts_to_int(args[0], args[1])
	second2 = mash_shorts_to_int(args[2], args[3])
	third2 = mash_shorts_to_int(args[4], args[5])
	return make_command("0x28", [first2, second2, third2])
#0x29
def make_reset_text_window(args, env):
	return make_command("0x29")
#0x2A
def make_text_color(args, env):
	a = int(args[0], 16)
	b = int(args[1], 16)
	return make_command("0x2A", [a, b])
#0x2B
def make_reset_text_color(args, env):
	return make_command("0x2B")
#0x2E
def make_text_speed(args, env):
	if args:
		# set_text_speed (0) means instant which is -1
		speed = int(args[0]) or -1
	else:
		speed = 0
	
	return make_command("0x2E", [speed])
#0x2F
def make_update(args, env):
	trans_name = args[0].upper()
	if trans_name not in transition_name_to_num:
		raise ValueError("Unknown transition: {}".format(trans_name))
		
	trans_type = transition_name_to_num[trans_name]
	num_args = len(args) - 1
	data = n_ints(args[1:])
	return make_command("0x2F", [trans_type, num_args], data)
#0x30
def make_update_config(args, env):
	num_vals = len(args)
	data = n_ints(args)
	return make_command("0x30", [num_vals], data)
#0x31
def make_update_text_save(args, env):
	saveText = int_bool(args, 0)
	return make_command("0x31", [saveText])
#0x32
def make_figure(args, env):
	position = figure_position_to_num[args[0].upper()]
	image_name = args[1]
	hasImage = 0
	data = b''
	if image_name:
		hasImage = 1
		data = c_string(image_name)
		register_character(image_name, env)
	return make_command("figure", [position,0,hasImage], data)
#0x34
def make_hidefigure(args, env):
	position = figure_position_to_num[args[0].upper()]
	return make_command("0x34", [position])
#0x37
def make_placefigurex(args, env):
	position = figure_position_to_num[args[0]]
	midpointX = args[1]
	return make_command("0x37", [position, midpointX])
#0x38
def make_resetfigurex(args, env):
	position = figure_position_to_num[args[0]]
	return make_command("0x38", [position])
#0x41
def make_bg(args, env):
	bg_name = args[0].lower()
	cmd_args = []
	# Using named bg
	if bg_name != "1" and bg_name != "2":
		data = b'\x01\x00\x00\x00' + c_string(bg_name)
		register_background(bg_name, env)
	# Using bgId
	else:
		data = b'\x00\x00\x00\x00'
		cmd_args.append(bg_name)
	return make_command("bg", cmd_args, data)
#0x42
def make_clearbg(args, env):
	return make_command("0x42")
#0x43
def make_scrollbg(args, env):
	scroll_type = scroll_type_to_num[args[0]]
	cgNum = args[1]
	updateSpeed = args[2]
	pixelsPerUpdate = args[3]
	combo = mash_shorts_to_int(scroll_type, pixelsPerUpdate)
	register_cg(cgNum, env)
	return make_command("0x43", [combo, cgNum, updateSpeed])
#0x47
def make_placeoverlay(args, env):
	x, y = args
	return make_command("0x47", [x,y])
#0x48
def make_setoverlay(args, env):
	cgNum = args[0]
	# Don't register the cgNum, it will fail to load the overlays, but they don't do anything anymore in the ps2 version
	#register_cg(cgNum, env)
	return make_command("0x48", [cgNum])
#0x49
def make_clearoverlay(args, env):
	return make_command("0x49")
#0x52
def make_textbox_cg(args, env):
	cgNum = args[0] if len(args) > 0 else 0
	# The need for this to be working got patched out of the script like the overlays
	#if cgNum:
	#	register_cg(cgNum, env)
	return make_command("0x52", [cgNum])
#0x56
def make_namebox_cg(args, env):
	# always reset
	cgNum = 0
	return make_command("0x56", [cgNum])
#0x58
def make_show_lower(args, env):
	return make_command("0x58")
#0x59
def make_hide_lower(args, env):
	return make_command("0x59")
#0x5A
def make_toggle_textbox(args, env):
	on = int_bool(args, 0)
	return make_command("0x5A", [on])
#0x5B
def make_toggle_namebox(args, env):
	on = int_bool(args, 0)
	return make_command("0x5B", [on])
#0x5C
def make_toggle_menu(args, env):
	on = int_bool(args, 0)
	return make_command("0x5C", [on])
#0x5F
def make_music(args, env):
	channel = args[0]
	trackNum = args[1]
	loop = int_bool(args, 2, "TRUE")
	register_music(trackNum, env)
	return make_command("music", [channel,trackNum,loop])
#0x60
def make_music_fade(args, env):
	channel = args[0]
	trackNum = args[1]
	loop = int_bool(args, 2, "TRUE")
	register_music(trackNum, env)
	return make_command("0x60", [channel,trackNum,loop])
#0x61
def make_music_fade_async(args, env):
	channel = args[0]
	trackNum = args[1]
	speed = args[2]
	loop = int_bool(args, 3, "TRUE")
	data = n_ints([speed])
	register_music(trackNum, env)
	return make_command("0x61", [channel, trackNum, loop], data)
#0x63
def make_stopmusic(args, env):
	channel = args[0]
	return make_command("0x63", [channel])
#0x64
def make_stopmusic_fade(args, env):
	channel = args[0] if len(args) > 0 else 0
	return make_command("0x64", [channel])
#0x65
def make_stopmusic_fade_async(args, env):
	channel = args[0]
	speed = args[1]
	return make_command("0x65", [channel, speed])
#0x66
def make_wav(args, env):
	channel = args[0]
	wav_name = args[1]
	loop = int_bool(args, 2, "FALSE")
	register_sound(wav_name, env)
	return make_command("wav", [channel,0,loop], c_string(wav_name + ".wav"))
#0x67
def make_wav_fade(args, env):
	channel = args[0]
	wav_name = args[1]
	loop = int_bool(args, 2, "FALSE")
	register_sound(wav_name, env)
	return make_command("0x67", [channel,0,loop], c_string(wav_name + ".wav"))
#0x68
def make_stopwav(args, env):
	channel = args[0]
	return make_command("0x68", [channel])
#0x69
def make_stopwav_fade(args, env):
	channel = args[0]
	return make_command("0x69", [channel])
#0x6A
def make_ogg(args, env):
	speaker = args[0]
	ogg_name = args[1] 
	register_voice(ogg_name, env)
	return make_command("ogg", [speaker], c_string(ogg_name + ".ogg"))

#0x6F
def make_mpg(args, env):
	raw_mpg_name = args[0]
	mpg_name = raw_mpg_name[:raw_mpg_name.rfind(".")] if raw_mpg_name.endswith(".mpg") else raw_mpg_name
	mpg_name = mpg_name.upper()
	register_video(mpg_name, env)
	return make_command("0x6F", [], c_string(mpg_name + ".mpg"))

#########################################################################

COMMAND_CONVERTERS = {
	"nop": make_nop,
	"break": make_break_skip,
	"exit": make_quickexit, #DONE
	"note": make_window_title, #DONE
	"next_scenario": make_load_script, #DONE
	"end_scenario": make_clear_script,
	"if_flag": make_branchflag,
	"if_var": make_branch, #DONE
	"else": make_jump, #DONE
	"select": make_choice, #DONE
	"goto": make_gotolabel, #DONE
	"wait": make_wait, #DONE
	"wait_sound": make_wait_wav, #DONE
	"wait_music_time": make_wait_music, #DONE
	"click": make_wait_click, #DONE
	"enable_input": make_toggleinput, #DONE
	"flag_on": make_setflag, #DONE
	"flag_off": make_clearflag, #DONE
	"var_set": make_assign,
	"var_add": make_add, #DONE
	"set_text_window": make_text_window, #DONE
	"reset_text_window": make_reset_text_window,
	"set_text_color": make_text_color,
	"reset_text_color": make_reset_text_color,
	"set_text_speed": make_text_speed,
	"reset_text_speed": make_text_speed,
	"update": make_update, #DONE
	"set_update_param": make_update_config, #DONE
	"set_update_text_save": make_update_text_save, #DONE
	"show_chara": make_figure, #DONE
	"hide_chara": make_hidefigure, #DONE
	"set_chara_x": make_placefigurex, #DONE
	"reset_chara_x": make_resetfigurex, #DONE
	"show_bg": make_bg, #DONE
	"hide_bg": make_clearbg,
	"scroll_bg": make_scrollbg, #DONE
	"set_day_pos": make_placeoverlay, #DONE
	"show_day": make_setoverlay, #DONE
	"hide_day": make_clearoverlay, #DONE
	"set_msg_window_cg": make_textbox_cg,
	"reset_msg_window_cg": make_textbox_cg,
	"reset_name_window_cg": make_namebox_cg,
	"show_window": make_show_lower, #DONE
	"hide_window": make_hide_lower, #DONE
	"enable_msg_window": make_toggle_textbox, #DONE
	"enable_name_window": make_toggle_namebox,
	"enable_sys_button": make_toggle_menu, #DONE
	"play_music": make_music, #DONE
	"play_music_fade": make_music_fade, #DONE
	"play_music_fade_async": make_music_fade_async, #DONE
	"stop_music": make_stopmusic, #DONE
	"stop_music_fade": make_stopmusic_fade, #DONE
	"stop_music_fade_async": make_stopmusic_fade_async, #DONE
	"play_sound": make_wav, #DONE
	"play_sound_fade": make_wav_fade, #DONE
	"stop_sound": make_stopwav, #DONE
	"stop_sound_fade": make_stopwav_fade, #DONE
	"play_voice": make_ogg, #DONE
	"play_movie": make_mpg,
}

def parse_commands_from_text(buffer, forceFirstName):
	if forceFirstName:
		return [make_name(forceFirstName), make_text("\\n".join(buffer))]
	elif "「" in buffer[0] and not buffer[0].startswith("「"):
		# name and text are on the same line
		name, text = buffer[0].split("「")
		buffer[0] = "「" + text
		return [make_name(name), make_text("\\n".join(buffer))]
	elif len(buffer) == 1:
		return [make_name(""), make_text(buffer[0])]
	elif "「" in buffer[1]:
		name, lines = buffer[0], "\\n".join(buffer[1:])
		return [make_name(name), make_text(lines)]
	else:
		return [make_name(""), make_text("\\n".join(buffer))]

def parse_command(line):
	cmd = line[(line.find("@")+1):line.find("(")].strip()
	raw_args_str = line[(line.find("(") + 1):line.find(")")].strip()
	raw_args = raw_args_str.split(",") if raw_args_str else []
	args = [raw.strip()[1:-1] if '"' in raw else raw.strip() for raw in raw_args]
	return cmd, args

MULTILINE_COMMANDS = ("if_flag", "if_var", "else", "select")

def multiline_buffer_append(lines, multiline_buffer, next_line=None):
	head_cmd = multiline_buffer[0]
	if head_cmd["command"] in ("branchflag", "branch", "jump"):
		jump = len(multiline_buffer) + (1 if next_line and next_line.startswith("@else") else 0)
		head_cmd["jump"] = jump
		lines.extend(multiline_buffer)
	elif head_cmd["command"] == "choice":
		choices = [multiline[:multiline.find("/")] if "//" in multiline else multiline for multiline in multiline_buffer[1:]]
		adjust_choice_command(head_cmd, choices)
		lines.append(head_cmd)

BUFFER_ENDERS = ("<NB>", "<BR>", "<DV2>")

def convert_script(ps2Script, asb_scripts_path, env):
	name = ps2Script.stem[:ps2Script.stem.rfind(".")]
	lines = []
	text_buffer = []
	multiline_buffer = []
	readingBlock = False
	multilineBufferReady = False
	textBufferReady = False
	forceFirstName = None
	print("Converting {}".format(name), flush=True)
	for line in read_lines(ps2Script):
		if multilineBufferReady:
			multiline_buffer_append(lines, multiline_buffer, line)
			multiline_buffer = []
			multilineBufferReady = False
		if textBufferReady:
			lines.extend(parse_commands_from_text(text_buffer, forceFirstName))
			text_buffer = []
			forceFirstName = None
			textBufferReady = False
		if line:
			if not line.startswith("//"):
				if line.startswith("@"):
					cmd, args = parse_command(line)
					if cmd == "name":
						forceFirstName = args[0] if len(args) > 0 else None
					else:
						if cmd not in COMMAND_CONVERTERS:
							print("Unimplemented command {}({}) in {}".format(cmd, ",".join(args), name))
							cmd = "nop"
						converted = COMMAND_CONVERTERS[cmd](args, env)
						if multiline_buffer or cmd in MULTILINE_COMMANDS:
							multiline_buffer.append(converted)
						else:
							lines.append(converted)
				elif line.startswith("{"):
					readingBlock = True
				elif line.startswith("}"):
					readingBlock = False
				elif line.startswith("*"):
					lines.append(make_label(line[1:]))
				elif multiline_buffer:
					multiline_buffer.append(line)
				else:
					text_buffer.append(line)
					if any(line.endswith(ending) for ending in BUFFER_ENDERS):
						textBufferReady = True
		elif text_buffer:
			textBufferReady = True
		if len(multiline_buffer) >= 2 and not readingBlock:
			multilineBufferReady = True
	if multilineBufferReady:
		multiline_buffer_append(lines, multiline_buffer)
	lines.append(make_exit())
	asb = {
		"lines": lines
	}
	asbScriptName = name + ".asb.json"
	env["requires"]["scripts"].add(asbScriptName)
	write_json(asb_scripts_path.joinpath(asbScriptName), asb)


def sets_to_str(paths, key):
	if isinstance(paths[key], set):
		paths[key] = [str(d) for d in paths[key]]
	else:
		for subkey in paths[key]:
			sets_to_str(paths[key], subkey)

def init_requires():
	return { asset: set() for asset in ASSET_TYPES }

def init_tables(tables_path):
	return { table_file.name[:table_file.name.find('.')]: read_json(table_file) for table_file in tables_path.iterdir() if table_file.name.endswith(".tbl.json") }

def add_table_requires(env, tables_path):
	for table_file in tables_path.iterdir():
		env["requires"]["tables"].add(table_file.name)

def convert_scripts(ps2_scripts_path, tables_path, asb_scripts_path, dependencies_path):
	env = {
		"requires": init_requires(),
		"tables": init_tables(tables_path)
	}
	for ps2Script in ps2_scripts_path.iterdir():
		convert_script(ps2Script, asb_scripts_path, env)

	add_table_requires(env, tables_path)
	sets_to_str(env, "requires")

	write_json(dependencies_path.joinpath("_dependencies.json"), env["requires"])

if len(sys.argv) != 5:
	raise ValueError("Invalid number of arguments")

args_as_paths = [Path(arg) for arg in sys.argv[1:]]

convert_scripts(*args_as_paths)